<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_global extends CI_Controller {
	 function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
        }
		
	public function delete()
	{
		$idprod = $this->input->post('idprod');
		
		$this->m_general->delCart($idprod);
	}
	
	public function find() {
		$warna = $this->input->post('warna');
		$data['query'] = $this->db->query('select * from tbl_products where warna ="'.$warna.'" group by warna asc');
		$this->load->view('result',$data);
	}
}
