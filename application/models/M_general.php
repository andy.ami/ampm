<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_general extends CI_Model{	

	public function addCart() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = $this->db->query("select a.*, (select SUM(b.jumlah) from tbl_cart b where b.ip='$ip' and b.id_order='0') as hit_cart, b.id as idcart, b.id_order, b.produk as idp, b.size, b.jumlah,b.ip from tbl_products as a JOIN tbl_cart as b 
        where b.produk = a.id and b.ip='$ip' and b.id_order ='0' order by b.id asc");
		return $sql;	
	}
	
	function getAddress() {
		$id = $this->session->userdata('loc_id');
		$sql = $this->db->query('select a.id as id_alamat,a.user,a.nama,a.alamat as nama_alamat,a.kota,a.provinsi,a.negara,a.kodepos,a.telepon, b.id,b.email, b.nama as nama_user,b.telepon as tlp_user, c.idcity,c.name_city, d.idprovince,d.name_province, e.idkecamatan,e.name_kecamatan from tbl_alamat a 
		LEFT JOIN tbl_user b ON b.id=a.user 
		LEFT JOIN tbl_city c ON c.idcity=a.kota
		LEFT JOIN tbl_kecamatan e ON e.idkecamatan=a.kecamatan
		LEFT JOIN tbl_province d ON d.idprovince=a.provinsi where a.user="'.$id.'" order by a.id desc');
		return $sql;
	}
	
	public function delCart($idprod) {
	  $ip = $_SERVER['REMOTE_ADDR'];
      $this->db->where('id', $idprod);
	  $this->db->where('ip', $ip);
	  $this->db->where('id_order', '0');
      $this->db->delete('tbl_cart'); 
	}
	
	public function insert_data($tabel, $data){
		$this->db->set('tgl_input','NOW()',FALSE);
		$this->db->set('tgl_update','NOW()',FALSE);
		$this->db->set('user_input',$this->session->userdata('loc_id'));
		$this->db->set('user_update',$this->session->userdata('loc_id'));
		$this->db->insert($tabel, $data);
	}

	public function hapus_data($tabel, $field, $kode){
		$this->db->where($field, $kode);
		$this->db->delete($tabel);
	}

	public function ubah_data($tabel, $data, $field, $kode){
		$this->db->where($field, $kode);
		$this->db->set('user_update',$this->session->userdata('loc_id'));
		$this->db->update($tabel, $data);
	}

	public function Bank(){
		$query = $this->db->query('select * from tbl_bank order by id desc');
		return $query;
	}
	
}