<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Register extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
        }
		
    public function addReg() {
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('email', 'Nama Email', 'required');
		$this->form_validation->set_rules('telp', 'Telepon', 'required');
		$this->form_validation->set_rules('password1', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
		$nama 		= $this->input->post('nama_lengkap');
		$ema		= $this->input->post('email');
		$tlp		= $this->input->post('telp');
		$password1	= md5($this->input->post('password1'));
		$password	= $this->input->post('password1');
		$tgl		= date('Y-m-d');
		//$data = $this->m_register->get_by_email($this->input->post('email'))->row();
		$query = $this->db->query("select * from tbl_user where email = '$ema'");
		if($query->num_rows() > 0){
			echo "Data nama email $ema sudah terdaftar.";
		}else{
			echo 'OK';
		$data = array(
			'email'			=> $ema,
			'password'		=> $password1,
			'telepon'		=> $tlp,
			'nama'			=> $nama,
			'tanggal'		=> $tgl
			);
		
			$this->db->insert('tbl_user',$data);
			
			
			$to   = $ema;
		  	$from = "clothingampm@gmail.com";
			$this->load->library('email');
			
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
		  	$this->email->subject('AMPM Official | Registration');
			
		  	$message  = "<html><body>";
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=120></div></p>";
		  	$message .= "<p>Terima kasih telah bergabung di <b>AMPM Clothing.</b>";
			$message .= "Silahkan melakukan login dengan akun Anda menggunakan data di bawah ini : <br/>";
		  	$message .= "Username : <strong>".$nama."</strong><br/>";
		  	$message .= "Nama Email : <strong>".$ema."</strong><br/>";
			$message .= "Password   : <strong>".$password."</strong></p><br/>";
			$message .= "<p><u>AMPM WeAreEqual</u></p>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
			}
		} else {

		 }
	}
	
	public function RegMobile() {
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('email', 'Nama Email', 'required');
		$this->form_validation->set_rules('telp', 'Telepon', 'required');
		$this->form_validation->set_rules('password1', 'Password', 'required');
		if ($this->form_validation->run() == TRUE) {
		$nama 		= $this->input->post('nama_lengkap');
		$ema		= $this->input->post('email');
		$tlp		= $this->input->post('telp');
		$password1	= md5($this->input->post('password1'));
		$password	= $this->input->post('password1');
		$tgl		= date('Y-m-d');
		$query = $this->db->query("SELECT * FROM tbl_user WHERE email='$ema'");
		
		if($query->num_rows() > 0){
			echo "Data nama email $ema sudah terdaftar.";
			redirect('reg_apps');
		}else{
			echo 'OK';
			
		$data = array(
			'email'			=> $ema,
			'password'		=> $password1,
			'telepon'		=> $tlp,
			'nama'			=> $nama,
			'tanggal'		=> $tgl
			);
			
			$this->db->insert('tbl_user',$data);
			
			$to   = $ema;
		  	$from = "clothingampm@gmail.com";
			$this->load->library('email');
			
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
		  	$this->email->subject('AMPM Official | Registration');
			
		  	$message  = "<html><body>";
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=120></div></p>";
		  	$message .= "<p>Terima kasih telah bergabung di <b>AMPM Clothing.</b>";
			$message .= "Silahkan melakukan login dengan akun Anda menggunakan data di bawah ini : <br/>";
		  	$message .= "Username : <strong>".$nama."</strong><br/>";
		  	$message .= "Nama Email : <strong>".$ema."</strong><br/>";
			$message .= "Password   : <strong>".$password."</strong></p><br/>";
			$message .= "<p><u>AMPM WeAreEqual</u></p>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
			}
			redirect('login_apps');
		} else {
		    $this->session->set_flashdata('message', '&nbsp;&nbsp;User ID dan password tidak cocok');
			redirect('reg_apps');
		 }
	}
}

?>