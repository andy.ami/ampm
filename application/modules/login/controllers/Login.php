<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Login extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_login');
        }
		
    public function index() {
		$email = $this->input->post('nama_email');
		$password = md5($this->input->post('nama_password'));
		$query	= $this->m_login->getLoginData($email,$password);
		if($query->num_rows() > 0){
			$row	= $query->row();
			$data	= array(
				'loc_id'	=> $row->id,
				'loc_nama'  => $row->nama,
				'loc_email' => $row->email,
				'loc_telepon' => $row->telepon,
				'loc_login' => TRUE
			);
			$this->session->set_userdata($data);
		}else{
			$this->session->set_flashdata('pesan_error', 'Login Gagal, Periksa Kembali Email atau Password Anda ! Silahkan Ulangi Kembali...');
			redirect('home');
		}
	}
	
	function logout(){
	   $this->session->unset_userdata('loc_login');
	   session_destroy();
	   redirect('login', 'refresh');
	 }
	
}

?>