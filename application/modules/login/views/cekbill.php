<div class="page">

<script type="text/javascript">
    $(document).ready(function() {
        $('#checkout-login-button').on("click", function(event) {
            event.preventDefault();
            var data = {
                username: $('#checkout-login-email').val(),
                password: $('#checkout-login-password').val(),
                rememberMe: $('#checkout-login-remember-me').prop('checked')
            };
            $.ajax({
                url: "/Account/Login",
                type: "post",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                processData: false,
                success: function(result) {
                    if (result) {
                        trackLogin();
                        window.location.href = "../address/index26af.html?showLoginMessage=true";
                    } else {
                        warningBox("Wrong username or password");
                    }
                }
            });
        });
    });

    function trackGuest() {
        if (dataLayer) {
            dataLayer.push({
                "event": "checkoutOption",
                "ecommerce": {
                    "checkout_option": {
                        "actionField": { "step": 2, "option": "continueAsGuest" }
                    }
                }
            });
        }
    }

    function trackLogin() {
        if (dataLayer) {
            dataLayer.push({
                "event": "checkoutOption",
                "ecommerce": {
                    "checkout_option": {
                        "actionField": { "step": 2, "option": "login" }
                    }
                }
            });
        }
    }
</script>
<br/><br/><br/><br/>
<div class="heading">
    <h1>Checkout</h1>
</div>

<ul class="checkout-progress">
    <li class="
        is-completed ">
            <a href="<?php echo site_url('checkout/cart'); ?>">
        <div class="checkout-progress__step-icon">1</div>
        <p>Your shopping bag</p>
    </a>

    </li>
    <li class="is-current
         ">
    <a href="<?php echo site_url('checkout/billing'); ?>">
        <div class="checkout-progress__step-icon">2</div>
        <p>Your billing / shipping address</p>
    </a>
    </li>
    <li class=" ">
    <div class="checkout-progress__step-icon">3</div>
    <p>Payment</p>
    </li>
</ul>

<form class="checkout-container" action="<?php echo site_url('checkout/payment'); ?>">
    <div class="checkout-column">
	<?php 
	$a = $this->session->userdata('id');
    if($a != TRUE) {
	
	?>
	
        <div class="form form--box" id="checkout-login">
            <h2>Log in to your account</h2>
            <p>Log in for faster checkout</p>
            <label class="text-input-label" for="checkout-login-email">E-mail</label>
            <input type="text" class="text-input" id="checkout-login-email">
            <label class="text-input-label" for="checkout-login-password">Password</label>
            <input type="password" class="text-input" id="checkout-login-password">
            <a href="#" class="forgot-password-link">Forgot password?</a>
            <input class="checkbox" type="checkbox" id="checkout-login-remember-me">
            <label for="checkout-login-remember-me">Remember me</label>
            <button type="submit" id="checkout-login-button">Log in and continue</button>
        </div>
	<?php
	}else{
	?>
		<div class="form ng-scope" id="billing-address-form" ng-controller="MarketController" ng-init="init()">

            <h2>Billing address</h2>
            <p>Please fill in the required information below</p>
            <p>* Required fields</p>
            <div class="form__content">
                

                <label class="text-input-label" for="BillingAddress_FirstName">Nama Lengkap *</label>
<input class="text-input" id="BillingAddress_FirstName" maxlength="15" name="BillingAddress.FirstName" type="text" value="">
<span class="field-validation-valid" data-valmsg-for="BillingAddress.FirstName" data-valmsg-replace="true"></span>
<label class="text-input-label" for="BillingAddress_LastName">Nomor Telepon *</label>
<input class="text-input" id="BillingAddress_LastName" maxlength="20" name="BillingAddress.LastName" type="text" value="">
<span class="field-validation-valid" data-valmsg-for="BillingAddress.LastName" data-valmsg-replace="true"></span>
<label class="text-input-label" for="BillingAddress_Address1">Alamat *</label>
<input class="text-input" id="BillingAddress_Address1" maxlength="35" name="BillingAddress.Address1" type="text" value="">
<span class="field-validation-valid" data-valmsg-for="BillingAddress.Address1" data-valmsg-replace="true"></span>

<label for="billing-address-country" class="select-label">Propinsi *</label>
<select class="select ng-pristine ng-untouched ng-valid" data-id="billing-address" id="billing-address-country" name="BillingAddress.CountryCode" ng-change="setMarket(&#39;billing-address&#39;)" ng-model="selectedCountryCode" style="display: none;"><option value="AL">Jawa Tengah</option>
<option value="AT">Jawa Timur</option>
<option value="BE">DKI Jakarta</option>
<option value="BG">Bogor</option>
<option selected="selected" value="Jawa Barat">Jawa Barat</option>
</select>

<label for="billing-address-country" class="select-label">Kota *</label>
<select class="select ng-pristine ng-untouched ng-valid" data-id="billing-address" id="billing-address-country" name="BillingAddress.CountryCode" ng-change="setMarket(&#39;billing-address&#39;)" ng-model="selectedCountryCode" style="display: none;"><option value="AL">Kabupaten Bandung</option>
<option value="AT">Surabaya</option>
<option value="BE">Jakarta Pusat</option>
<option value="BG">Kota Bogor</option>
<option selected="selected" value="Bandung">Kota Bandung</option>
</select>

<label class="text-input-label" for="BillingAddress_Telephone">Kode Pos *</label>
<input class="text-input" id="BillingAddress_Telephone" maxlength="15" name="BillingAddress.Telephone" type="text" value="">
<span class="field-validation-valid" data-valmsg-for="BillingAddress.Telephone" data-valmsg-replace="true"></span>
            </div>
        </div>
	<? } ?>
    </div>
    
<div id="checkout-order" class="checkout-column">
    <div class="checkout-receipt">
        <h2>Pesanan Anda</h2>


    <ul>
	<?php
	$subtot = 0;
foreach($query_cart->result() as $row) {
	$idprod   = $row->idcart;
	$nama = $row->nama;
	$wrn  = $row->warna;
	$hrg  = $row->harga;
	$foto = $row->foto;
	$size = $row->size;
	$jml  = $row->jumlah;
	$tot  = $hrg * $jml;
	$subtot += $tot;

	if(!isset($jml)) {
	echo '';
	}else{
?>
        <li ng-repeat="p in products">
        <button type="button" class="remove-from-cart" onclick="removeFromCart(<?php echo $idprod; ?>)" title="Remove product from shopping cart"></button>
                <h3><?php echo $nama; ?></h3>
                <p><?php echo $wrn; ?></p>
                    <p>Size : <?php echo $size; ?>, Qty : <input type="text" class="checkout-receipt__edit-qty" ng-model="p.Quantity" ng-change="updateQuantity(p, '{{p.Quantity}}')" value="<?php echo $jml; ?>"></p>
                <div class="checkout-receipt__price"> <span class="currency"><?php echo number_format($hrg); ?></span></div>
            </li>
  
<?php }} ?>
	<li class="checkout-receipt__total">
                <h3>Total</h3>
                <div class="checkout-receipt__price"> <span class="currency"><?php echo number_format($subtot); ?></span></div>
            </li>
		</ul>
    </div>
</div>

    <div class="terms-and-conditions">
                        <input class="checkbox" id="terms-and-conditions" name="AcceptedTerms" type="checkbox" value="true"><input name="AcceptedTerms" type="hidden" value="false">
                        <label for="terms-and-conditions">I accept the <a href="https://www.swims.com/help--info/terms-conditions/">terms and conditions</a></label>
                    </div>
                    <div class="checkout-nav">
                        <a href="https://www.swims.com/checkout/cart/" class="checkout-nav__prev">Previous step</a>
                        <!-- ignorer onclicken på submit -->
                        <input type="submit" class="checkout-nav__next" value="Next - Payment" disabled="">
                        <!-- lenke uten submit: <a href="lenke" class="checkout-nav__next">lenke-tekst</a> -->
                    </div>
</form>
</div>
<script>
function removeFromCart(idprod) {
	if (confirm("Anda yakin akan menghapus transaksi ini?")) {
        $.ajax({
            url: '<?php echo site_url('con_global/delete'); ?>',
            type: "POST",
            data: 
			{
                idprod: idprod
            },
            success: function(data) {
				window.location.reload();
            }
        });
		 $(this).parents(".record").animate("fast").animate({
                        opacity : "hide"
                    }, "slow");
		}
		return false;
    }
</script>
