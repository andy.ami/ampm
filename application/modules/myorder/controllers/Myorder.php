<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Myorder extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_account');
			if($this->session->userdata('loc_id') == ''){
			redirect('home');
		}
        }
		
     public function index() {
		$id = $this->session->userdata('loc_id');
		$data['query_cart'] = $this->m_general->addCart();
		$data['query_add'] = $this->m_account->getAddress();
		$data['province'] = $this->m_account->getFirstChoiceProvince();
		$data['city'] = $this->m_account->getCityChoice();
		$data['order'] = $this->db->query('select * from tbl_order where user="'.$id.'" and status !="Pending" order by id asc');
		$data['content'] = $this->load->view('myorder_view',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
}

?>