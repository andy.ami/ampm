<?  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class M_account extends CI_Model {
	   
	public function insert_data($tabel, $data){
		$this->db->set('user',$this->session->userdata('loc_id'));
		$this->db->insert($tabel, $data);
	}
	
	function getAddress() {
		$id = $this->session->userdata('loc_id');
		$sql = $this->db->query('select a.id as id_alamat,a.user,a.nama,a.alamat as nama_alamat,a.kota,a.provinsi,a.negara,a.kodepos,a.telepon, b.id,b.email, b.nama as nama_user,b.telepon as tlp_user, c.idcity,c.name_city, d.idprovince,d.name_province from tbl_alamat a 
		LEFT JOIN tbl_user b ON b.id=a.user 
		LEFT JOIN tbl_city c ON c.idcity=a.kota
		LEFT JOIN tbl_province d ON d.idprovince=a.provinsi where a.user="'.$id.'"');
		return $sql;
	}
	
	public function editDataAccount($tabel, $data, $field, $kode){
		$this->db->where($field, $kode);
		$this->db->set('id',$this->session->userdata('loc_id'));
		$this->db->update($tabel, $data);
	}
	
	public function editDataPassword($tabel, $data, $field, $kode){
		$this->db->where($field, $kode);
		$this->db->set('id',$this->session->userdata('loc_id'));
		$this->db->update($tabel, $data);
	}
	
	public function editDataAddress($tabel, $data, $field, $kode){
		
		$this->db->where($field, $kode);
		$this->db->set('user',$this->session->userdata('loc_id'));
		$this->db->update($tabel, $data);
	}
	
	function cek_password($password){
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('password', $password);
        $this->db->where('id', $this->session->userdata('loc_id'));
        return $this->db->get();
    }
	
	function getFirstChoiceProvince() {
	    $sql = $this->db->query('select * from tbl_province order by name_province asc');
		return $sql;
	  }
	  
	function getCityChoice() {
		$idprovince = $this->input->post('idprovince');
		$sql = $this->db->query('select * from tbl_city where idprovince="'.$idprovince.'" order by name_city asc');
		return $sql;
	  }
	  
	function editCityChoice() {
		$edit_idprovince = $this->input->post('edit_idprovince');
		$sql = $this->db->query('select * from tbl_city where idprovince="'.$edit_idprovince.'" order by name_city asc');
		return $sql;
	  }
}
?>