
<div class="page" tabindex="-1">
<br/><br/><br/><br/>
<div class="heading">
        <h1>My order</h1>

    <p><a class="my-account-log-out" href="<?php echo site_url("admin/logout"); ?>">Log out</a></p>
</div>

<div class="my-account-container">

	<br/>
	<h2>Orders</h2>
    <ul class="order-history">
		<table border="1" height="100%" width="100%">
			<tr>
				<td align="center"><b>INVOICE</b></td>
				<td align="center"><b>TOTAL</b></td>
				<td align="center"><b>STATUS</b></td>
			</tr>
			<?php
				foreach($order->result() as $rsl) {
					$kurir = $rsl->kurir;
					$idinv = $rsl->id;
					$user = $rsl->user;
					$resi  = $rsl->resi_kiriman;
				if($kurir!="" && $resi!=""){
					$nores  	= $resi;
					$namakurir	= $kurir;
				}
			?>
			<tr>
				<td align="center"><a href="<?php echo site_url('invoice/check_invoice/'.$idinv); ?>"><?php echo '#'.$rsl->id; ?></a></td>
				<td align="right">Rp. <?php echo number_format($rsl->jml_bayar); ?></td>
				<td align="center"><?php echo strtoupper($rsl->status); ?><br> Nomor Resi : <?php echo $nores;?><br><?php echo $kurir; ?></td>
			</tr>
				<?php } ?>
		</table>
    </ul>
</div>

</div>
<script type="text/javascript">
    function updateAccountTabs() {
        $('#account-tabs-account-info, #account-content-account-info').toggleClass('is-active', (window.location.hash == '#account-info' || window.location.hash == ''));
        $('#account-tabs-addresses, #account-content-addresses').toggleClass('is-active', (window.location.hash == '#addresses'));
        $('#account-tabs-orders, #account-content-orders').toggleClass('is-active', (window.location.hash == '#orders'));
        $('#account-tabs-returnorders, #account-content-returnorders').toggleClass('is-active', (window.location.hash == '#returnorders'));
    }

    updateAccountTabs();

    $('#account-tabs-account-info').click(function () {
        window.location.hash = "#account-info";
        updateAccountTabs();
    });
    $('#account-tabs-addresses').click(function () {
        window.location.hash = "#addresses";
        updateAccountTabs();
    });
    $('#account-tabs-orders').click(function () {
        window.location.hash = "#orders";
        updateAccountTabs();
    });
    $('#account-tabs-returnorders').click(function () {
        window.location.hash = "#returnorders";
        updateAccountTabs();
    });

$("#idprovince").change(function(){				
                var selectValues = $("#idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#city').html(msg);
                }else{
                    var idprovince = {idprovince:$("#idprovince").val()};
                    $('#idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/select_city')?>",
                            data: idprovince,
                            success: function(msg){
                                $('#city').html(msg);
                            }
                    });
                }
        });
		
$("#edit_idprovince").change(function(){				
                var selectValues = $("#edit_idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"edit_idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#edit_city').html(msg);
                }else{
                    var edit_idprovince = {edit_idprovince:$("#edit_idprovince").val()};
                    $('#edit_idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/edit_select_city')?>",
                            data: edit_idprovince,
                            success: function(msg){
                                $('#edit_city').html(msg);
                            }
                    });
                }
        });
</script>


</div>
