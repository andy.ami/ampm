<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Contact extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
        }
		
    public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('contact_view',$data,TRUE);
		//$this->m_general->addCart();
		$this->load->view('home_index',$data);
	}
}

?>