<div class="page" tabindex="-1">
<br/><br/><br/>
<div class="centered-content">
    <div class="heading">
        <h1>Log in</h1>
    </div>
    <p>Silahkan input email dan password Anda. Belum punya akun?, <a href="<?php echo site_url('reg_apps'); ?>">silahkan lakukan registrasi.</a></p>
    <div id="loginRegistration">
        <h2>Eksisting Akun</h2>
        <style>
    .field-validation-valid {
        display: none;
    }
</style>
<form action="<?php echo site_url('login_apps/login_proses'); ?>" class="form ng-pristine ng-valid" id="loginForm" method="post" novalidate="novalidate">
 <div class="form-group">
        <label class="text-input-label" for="login_email">E-mail</label>
        <input class="text-input" data-val="true" data-val-required="The Email: field is required." id="login_email" name="login_email" type="text" value="">
        <span class="field-validation-valid" data-valmsg-for="login_email" id="email_kosong" data-valmsg-replace="false">Masukkan nama email.</span>
    </div>
    <div class="form-group">
        <div class="password-wrapper"><label class="text-input-label" for="login_password">Password</label><input class="text-input" data-val="true" data-val-required="The Password: field is required." id="login_password" name="login_password" type="password"><button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        
        <span class="field-validation-valid" data-valmsg-for="login_password" id="password_kosong" data-valmsg-replace="false">Masukkan password.</span>
    </div>
    <div class="form-group">
        <input class="checkbox" id="LoginForm_RememberMe" name="LoginForm.RememberMe" type="checkbox" value="true"><input name="LoginForm.RememberMe" type="hidden" value="false">
        <label for="LoginForm_RememberMe">Remember me</label>
    </div>
    <div class="form-group">
        <input type="submit" id="loginSubmit" value="Log in">
    </div>
</form>
	<p>
        <div class="links">
            <a href="<?php echo site_url('reset_password'); ?>" class="forgot">Forgot password?</a>
        </div>
	</p>
    </div>
</div>

 <script type="text/javascript">
            $('#loginSubmit').click(function () {
				var email = document.getElementById('login_email').value;
				var password = document.getElementById('login_password').value;
				if(email == '' || password == '') {
					$('#email_kosong').show();
					$('#password_kosong').show();
				}else{
                $.ajax({
                    url: '<?php echo site_url('login_apps'); ?>',
					type: 'POST',
					data: {'login_email':email, 'login_password':password},
                    success: function (data) {
						if(data == '') {
							window.location.href('login_apps');
                        } else {
							$('#login-form-error').hide();
						    //window.location.href('home');
                        }
                    }
                });
				}
            });
		//}
            $('#login-form-error').hide();
    </script>
</div>