<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_apps extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_loginapps');
        }
		
    public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('login_appsview',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	 public function login_proses() {
		$email = $this->input->post('login_email');
		$password = md5($this->input->post('login_password'));
		$query	= $this->m_loginapps->getLoginData($email,$password);
		if($query->num_rows() > 0){
			$row	= $query->row();
			$data	= array(
				'loc_id'	=> $row->id,
				'loc_nama'  => $row->nama,
				'loc_email' => $row->email,
				'loc_telepon' => $row->telepon,
				'loc_login' => TRUE
			);
			$this->session->set_userdata($data);
			redirect('account');
		}else{
			$this->session->set_flashdata('pesan_error', 'Login Gagal, Periksa Kembali Email atau Password Anda ! Silahkan Ulangi Kembali...');
			redirect('login_apps');
		}
	}
        	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>