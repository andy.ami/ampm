<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_loginapps extends CI_Model{	

	public function getLoginData($email,$password){
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		return $this->db->get('tbl_user');
	}
	
	function ubah_pass($tabel,$email,$data){
        $this->db->where('username',$email);
		$this->db->update($tabel,$data);
    }
}