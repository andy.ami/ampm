<script src="http://ampmstore.com/demo/assets/libraries/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="http://ampmstore.com/demo/assets/libraries/jquery-2.1.0.min.js"></script>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.min.css">
	
    <!-- Scripts -->
    <script type="text/javascript" src="http://ampmstore.com/demo/assets/js/plugins.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
<div class="page">
<br/><br/><br/><br/>
<article class="article">
	<div class="image-link-list ">
    	<ul></ul>
		<img data-epi-overlay-z-index="1000" src="<?php echo base_url(); ?>assets/images/header9.jpg" title="" alt=""/>
	</div>
    <div class="heading">
        <!--<p class="heading__tag">Help</p>-->
        <h1>CARA BELANJA</h1>
        <p class="heading__tagline"></p>
    </div>
	<div class="article__content">    	
        <div>
           <ol>
				<li>
                	Melalui Email&nbsp; - hello@ampmstore.com <br />Cukup saja detail pesanan: Nama Artikel, ukuran, jumlah, serta nama dan alamat kirim. Kami akan membalas dengan total biaya dan nomer rekening untuk transaksi.
                </li>
                <li>
                	Melalui SMS/Whatsapp &nbsp; -  0878-255-256-29 <br />SMS saja detail pesanan: Nama Artikel, ukuran, jumlah, serta nama dan alamat kirim. Kami akan membalas dengan total biaya dan nomer rekening untuk transaksi.
                </li>
                <li>
                	Melalui Aplikasi LINE@  &nbsp;<br /><a href="http://line.me/ti/p/%40bvp9875s" >KLIK DISINI</a>
                </li>
                
                </ol> 
                	
                <p style=" max-width: 800px;  margin: 40px auto;" >
                	_________________________________________&nbsp;<br />
                </p>
                 <p style=" max-width: 800px;  margin: 40px auto;" >
                	Syarat dan ketentuan lainnya:  &nbsp;<br />
                </p>
                 <ol>
                <li>
                	Pembayaran & Konfirmasi sebelum jam 12.00 maka akan kita proses pengiriman ke Kurir pada hari yang sama (hari kerja), Jika lebih dari jam 12:00 maka akan kita kirimkan keesokan harinya.  &nbsp;
                </li>
                
                 <li>
                	Konfirmasi pembayaran bisa dengan sms atau whatsapp ke 0878-255-256-29 (sertakan juga bukti transfer yang ada) &nbsp;
                </li>
                
                 <li>
                	Nomer Resi Pengiriman akan kita sms atau email maksimal pada besok malamnya (hari kerja)&nbsp;
                </li>
                <li>
                	Semua nomor rekening (BCA / Mandiri / BNI) untuk pembayaran adalah atas nama GALIH AGUSTIAN &nbsp;
                </li>
                

           </ol> 
        </div>        
	</div>
</article>

<footer class="global-footer">
    <div class="centered-content">
        <div>
        	<div class="block instagramblock narrow ">
				<h2 class="global-footer__heading">World of AMPM</h2>
				<div class="global-footer__hashtags"><p>Tag us on Instagram <a href="https://www.instagram.com/ampm.magz/">@ampm.magz</a> <a href="https://www.instagram.com/explore/tags/weareequal/">#weareequal</a> <a href="https://www.instagram.com/explore/tags/colorwear/">#colorwear</a>&nbsp;</p></div>
                <ul class="global-footer__instagram-images">
                    <li><img src="<?php echo base_url(); ?>assets/images/footer2.jpg"></li>
                    <li><img src="<?php echo base_url(); ?>assets/images/footer4.jpg" alt="/"></li>
                    <li><img src="<?php echo base_url(); ?>assets/images/footer3.jpg" alt="/"></li>
                    <li><img src="<?php echo base_url(); ?>assets/images/footer1.jpg" alt="/"></li>
                </ul>
			</div>
            <div class="block titleandtextblock narrow ">
				<h2 class="global-footer__heading">How may we help you today?</h2>
				<div class="global-footer__sub-title"></div>
			</div>
		</div>
         <ul class="global-footer__content">
        	<li class="global-footer__content__item">
				<h5>Tentang AMPM</h5>
                <ul class="global-footer__link-list">
                    <!--<li><a href="http://ampmstore.com/story">AMPM Story</a></li>-->
                    <li><a href="http://ampmstore.com/contact">Kontak Kami</a></li>
                </ul>
			</li>
			<li class="global-footer__content__item">
                <h5>Bantuan & Info</h5>
                <ul class="global-footer__link-list">
                    <li><a href="http://ampmstore.com/how-to-order">Cara Belanja</a></li>
                    <li><a href="http://ampmstore.com/size-guide">Panduan Ukuran</a></li>
                </ul>
			</li>
			<li class="global-footer__content__item">
				<h5>Ikuti Kami</h5>
				<ul class="social-links">
                    <li>
                        <a href="https://web.facebook.com/ampm.magz/" class="social-links__item facebook"></a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/ampm.magz/" class="social-links__item instagram"></a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                    <li>
                        <a href="http://line.me/ti/p/%40bvp9875s" class="social-links__item line"> &nbsp;&nbsp;
                        	<img src="<?php echo base_url(); ?>assets/images/line.png" width="25">
                        </a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                </ul>
			</li>
		</ul>
    </div>

    <div class="global-footer__footer">
		<div class="centered-content">
            <ul class="shipping-information">
                <li>Gratis ongkos kirim se-pulau jawa setiap minimal pembelian Rp. 200.000</li>
            </ul>
		</div>
	</div>
</footer>    

</div>

<!-- JS and analytics only. -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
