<div class="page">
<br/><br/><br/><br/>
<article class="article">
	<div class="image-link-list ">
    	<ul></ul>
		<img data-epi-overlay-z-index="1000" src="<?php echo base_url(); ?>assets/images/header8.jpg" title="" alt=""/>
	</div>
    <div class="heading">
        <!--<p class="heading__tag">SIZE GUIDE</p>-->
        <h1>PANDUAN UKURAN</h1>
        <p class="heading__tagline"></p>
        <div class="heading__intro"><p>Temukan ukuran kamu di AMPM.</p></div>
    </div>
	<div class="article__content">    	
       
        <div>
        	<p><strong>&nbsp;</strong></p>
            <table border="0"><caption>Men's Tshirt</caption>
                <tbody>
                    <tr>
                        <td>Ukuran</td>
                        <td>Small</td>
                        <td>Medium</td>
                        <td>Large</td>
                        <td>Extra Large</td>
                      
                    </tr>
                        <td>Panjang</td>
                        <td>-</td>
                        <td>67cm</td>
                        <td>71cm</td>
                        <td>76cm</td>
                       
                     
                    </tr>
                    <tr>
                        <td>Lebar</td>
                        <td>-</td>
                       <td>52cm</td>
                        <td>56cm</td>
                        <td>59cm</td>
                     
                    </tr>
                  
                </tbody>
            </table>            
        </div>
        <div>
        	<p>Penting: Kami memakai ukuran besar.</p>
		</div>
	</div>
</article>

</div>

<footer class="global-footer">
    <div class="centered-content">
        <div>
        	<div class="block instagramblock narrow ">
				<h2 class="global-footer__heading">World of AMPM</h2>
				<div class="global-footer__hashtags"><p>Tag us on Instagram <a href="https://www.instagram.com/ampm.magz/">@ampm.magz</a> <a href="https://www.instagram.com/explore/tags/weareequal/">#weareequal</a> <a href="https://www.instagram.com/explore/tags/colorwear/">#colorwear</a>&nbsp;</p></div>
                <ul class="global-footer__instagram-images">
                    <li><img src="../images/footer2.jpg"></li>
                    <li><img src="../images/footer4.jpg" alt="/"></li>
                    <li><img src="../images/footer3.jpg" alt="/"></li>
                    <li><img src="../images/footer1.jpg" alt="/"></li>
                </ul>
			</div>
            <div class="block titleandtextblock narrow ">
				<h2 class="global-footer__heading">How may we help you today?</h2>
				<div class="global-footer__sub-title"></div>
			</div>
		</div>
        
         <ul class="global-footer__content">
        	<li class="global-footer__content__item">
				<h5>Tentang AMPM</h5>
                <ul class="global-footer__link-list">
                    <!--<li><a href="http://ampmstore.com/story">AMPM Story</a></li>-->
                    <li><a href="http://ampmstore.com/contact">Kontak Kami</a></li>
                </ul>
			</li>
			<li class="global-footer__content__item">
                <h5>Bantuan & Info</h5>
                <ul class="global-footer__link-list">
                    <li><a href="http://ampmstore.com/how-to-order">Cara Belanja</a></li>
                    <li><a href="http://ampmstore.com/size-guide">Panduan Ukuran</a></li>
                </ul>
			</li>
			<li class="global-footer__content__item">
				<h5>Ikuti Kami</h5>
				<ul class="social-links">
                    <li>
                        <a href="https://web.facebook.com/ampm.magz/" class="social-links__item facebook"></a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/ampm.magz/" class="social-links__item instagram"></a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                    <li>
                        <a href="http://line.me/ti/p/%40bvp9875s" class="social-links__item line"> &nbsp;&nbsp;
                        	<img src="../images/line.png" width="25">
                        </a>
                        <span class="social-links__tooltip">Ikuti Kami</span>
                    </li>
                </ul>
			</li>
		</ul>
    </div>

    <div class="global-footer__footer">
		<div class="centered-content">
            <ul class="shipping-information">
                 <li>Gratis ongkos kirim se-pulau jawa setiap minimal pembelian Rp. 200.000</li>
            </ul>
		</div>
	</div>
</footer>    

