<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Home extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('home_model');
        }
		
    public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('home_page',$data,TRUE);
		$this->m_general->addCart();
		$this->load->view('home_index',$data);
	}
    
	public function reset_password() {
		$data_user 		= $this->home_model->get_by_email($this->input->post('reset_password'))->row();
		if(count($data_user) > 0) {
		echo 'OK';
			$reset		= $this->generate_kode();
			$data 		= array('password' => md5($reset), 'code' => $reset);
		$this->home_model->edit($data_user->id, $data);
		
			$to   = $data_user->email;
		  	$from = "clothingampm@gmail.com";
			$this->load->library('email');
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
		  	$this->email->subject('AMPM Official | Reset Password');
		  	$message  = "<html><body>";
		  	$message .= "<p>Password Anda berhasil di reset. <br />";
			$message .= "Password Baru  : <strong>".$reset."</strong></p><br/>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
		}else{
			echo 'Nama email tidak terdaftar';
		}
	}
	
	function get_data(){
	$term = trim(strip_tags($_GET['term']));//retrieve the search term that autocomplete sends
	$qstring = "SELECT * FROM tbl_products WHERE nama LIKE '%".$term."%'";
	$result = mysql_query($qstring);//query the database for entries containing the term
		while ($row = mysql_fetch_array($result))//loop through the retrieved values
		{
		$row['nama']=htmlentities(stripslashes($row['nama']));
		$row['warna']=htmlentities(stripslashes($row['warna']));
		$row_set[] = $row;//build an array
		}
	echo json_encode($row_set);//format the array into json data
  }
  
	# Mengenerate kode verifikasi (5 karakter)
	public function generate_kode()
	{
		$seed 			= "123456789123456789123456789";
		$seed_length	= 5;
		$str 			= ''; 

		for ($i=0; $i<=$seed_length; $i++) 
		{ 
			$str .= substr ($seed, rand() % 27, 1); 
		} 

		return $str; 
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>