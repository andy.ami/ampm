<div class="page">
<div class="frontpage-grid">
    <div class="frontpage-grid__column" data-image="<?php echo base_url(); ?>assets/images/left.jpg"
         data-mobile-image="<?php echo base_url(); ?>assets/images/leftmobile.jpg">
        <div class="frontpage-grid__scroll-indicator">More</div>
        <div class="frontpage-grid__column__heading">
            <h2>#Bradleys</h2>
            <p class="intro-large">Functional fashion for travel and leisure</p>
        </div>
        <div class="frontpage-grid__content">
            <div class="frontpage-grid__content-wrapper">
                <div class="frontpage-grid__cell desktop-full-width">
                    <h2>#Bradleys</h2>
                    <p class="intro-large">Functional fashion for travel and leisure</p>
                </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Braided Lace Loafer</p>
            <p class="product__color">Navy/Grenadine</p>
                    <p class="product__price">
                160
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Braided Lace Loafer</p>
            <p class="product__color">Khaki/Green</p>
                    <p class="product__price">
                160
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/henrikkristoffersen/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src=" />

        </div>
        <div class="media-block__text">
            <p class="tag">Style</p>
<h2>Henrik Kristoffersen</h2>
<p>My style varies according to the setting I find myself in.</p>

        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/anson-low/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">Style</p>
<h2>Anson Low enjoys the last days of summer</h2>


        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Breeze Flat Front</p>
            <p class="product__color">Navy</p>
                    <p class="product__price">
                1000
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Breeze Flat Front</p>
            <p class="product__color">Steel/Green</p>
                    <p class="product__price">
                1000
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/trendstyled/" class="promo-image">
    <div class="promo-image__image">
        
    <img src="" />

        <h2>Trendstyled</h2>
    </div>
    <div class="promo-image__text">
        <p class="promo-image__tag">Breeze</p>
        <p>Saul Carrasco styles our city look</p>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/Breeze-through-life/" class="promo-image">
    <div class="promo-image__image">
        
    <img src="/siteassets/home-page/launch/left-menu/breezelaunchlink.jpg" alt="" title="" />

        <h2>Breeze</h2>
    </div>
    <div class="promo-image__text">
        <p class="promo-image__tag">Spring 2016</p>
        <p>Find out why your feet should'nt live without them!</p>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Breeze</p>
            <p class="product__color">Gray/Lime</p>
                    <p class="product__price">
                140
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="">
        <div class="product__image">
            <img src="" />
                <div class="product__hover-image">
                    <img src="" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Breeze</p>
            <p class="product__color">Navy/Orange</p>
                    <p class="product__price">
                140
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/boat-loafer/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="" />

        </div>
        <div class="media-block__text">
            <p class="tag">ss16</p>
<h2>Your ultimate boat shoe</h2>


        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/breeze-technology/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">Breeze</p>
<h2>Technology</h2>
<p>The technology behind the <em>BREEZE</em> loafer</p>

        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/primaloft/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="/siteassets/world-of-swims/ss16/primaloft/mediablockimageprimaloft2.jpg" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">SS16</p>
<h2>Primaloft</h2>
<p>The problem-solving polos and t-shirts</p>

        </div>
    </div>
</a>
        </div>

            </div>
        </div>
    </div>

    <div class="frontpage-grid__column" data-image="<?php echo base_url(); ?>assets/images/fw161.jpg"
         data-mobile-image="<?php echo base_url(); ?>assets/images/fw161mobile.jpg">
        <div class="frontpage-grid__scroll-indicator">More</div>
        <div class="frontpage-grid__column__heading">
            <h2>#Seducetherain</h2>
            <p class="intro-large">Beat the fall season with flair</p>
        </div>
        <div class="frontpage-grid__content">
            <div class="frontpage-grid__content-wrapper">
                <div class="frontpage-grid__cell desktop-full-width">
                    <h2>#Seducetherain</h2>
                    <p class="intro-large">Beat the fall season with flair</p>
                </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/grenoble/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="<?php echo base_url(); ?>assets/images/blockimagegrenoble.jpg" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">Grenoble</p>
<h2>The highly addictive business coat</h2>


        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="/swimscatalog/man/apparel/m-jackets/grenoble/grenoble-midnight-blue/">
        <div class="product__image">
            <img src="//swimscdnprod.azureedge.net/siteassets/mens/apparel/jackets/grenoble/31236_418_1.jpg?Width=500&amp;transform=fit" />
                <div class="product__hover-image">
                    <img src="//swimscdnprod.azureedge.net/siteassets/mens/apparel/jackets/grenoble/31236_418_2.jpg?Width=500&amp;transform=fit" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Grenoble</p>
            <p class="product__color">Midnight Blue</p>
                    <p class="product__price">
                450
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell">

<a href="/help--info/return-policy/" class="bubble-link">
    <span>WE OFFER FREE RETURNS</span>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/barryline/" class="promo-image">
    <div class="promo-image__image">
        
    <img src="/siteassets/world-of-swims/fw16/barryline/articlemedialargebarrybroguehigh2.jpg" alt="" title="" />

        <h2>The Barry Line</h2>
    </div>
    <div class="promo-image__text">
        <p class="promo-image__tag">FW16</p>
        <p>How to tackle bad weather without compromising on style.</p>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="/swimscatalog/man/footwear/barry-line/barry-derby-classic/barry-derby-classic-blackwhite/">
        <div class="product__image">
            <img src="//swimscdnprod.azureedge.net/siteassets/mens/footwear/lace-ups/barry-derby/21255_023_1.jpg?Width=500&amp;transform=fit" />
                <div class="product__hover-image">
                    <img src="//swimscdnprod.azureedge.net/siteassets/mens/footwear/lace-ups/barry-derby/21255_023_4.jpg?Width=500&amp;transform=fit" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Barry Derby</p>
            <p class="product__color">Black/White</p>
                    <p class="product__price">
                220
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell">

<a class="product" href="/swimscatalog/man/footwear/barry-line/barry-penny-classic/barry-penny-classic-brownwhite/">
        <div class="product__image">
            <img src="//swimscdnprod.azureedge.net/siteassets/mens/footwear/loafers/barry-penny/21257_025_1_gray1200.jpg?Width=500&amp;transform=fit" />
                <div class="product__hover-image">
                    <img src="//swimscdnprod.azureedge.net/siteassets/mens/footwear/loafers/barry-penny/21257_025_4_gray1200.jpg?Width=500&amp;transform=fit" />
                </div>
        </div>
    <div class="product__description">
        <p class="product__name">Barry Penny</p>
            <p class="product__color">Brown/White</p>
                    <p class="product__price">
                220
                <span class="currency">USD</span>
            </p>
    </div>
</a>

        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/premium-loafers/" class="promo-image">
    <div class="promo-image__image">
        
    <img src="/siteassets/home-page/may-2016/right/laceloaferwoven2.jpg" alt="" title="" />

        <h2>Evening Loafers</h2>
    </div>
    <div class="promo-image__text">
        <p class="promo-image__tag">Summer 2016</p>
        <p>Sleek evening footwear that can handle an unexpected dip in the pool!</p>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/Summer-showers/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="/siteassets/world-of-swims/ss16/dora--winchester/wosdorawinchestermediablock.jpg" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">spring 16</p>
<h2>Dora &amp; Winchester</h2>
<p>How to look great in summer showers</p>

        </div>
    </div>
</a>
        </div>
        <div class="frontpage-grid__cell desktop-full-width">

<a href="/articles/urban-spring-outerwear/" class="media-block__link">
    <div class="media-block">
        <div class="media-block__image">
            <button type="button" class="media-block__image-toggler" title="Show full image"></button>
            
    <img src="/siteassets/world-of-swims/ss16/urban-spring-outerwear/article-top-image-urbanspringouterwear1.jpg" alt="" title="" />

        </div>
        <div class="media-block__text">
            <p class="tag">Spring 16</p>
<h2>Spring Shower Seduction</h2>


        </div>
    </div>
</a>
        </div>

            </div>
        </div>
    </div>
</div>
</div>
