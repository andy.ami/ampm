<!-- Ini Body Page -->
	
<div class="page">
	<div class="frontpage-grid">
    	<div class="frontpage-grid__column" data-image="<?php echo base_url(); ?>assets/images/promoonline.jpg"
         data-mobile-image="<?php echo base_url(); ?>assets/images/promomobile.jpg">
			<div class="frontpage-grid__scroll-indicator">More</div>
        	<div class="frontpage-grid__column__heading">
            	<h3 style="color:#fff;"></h3>
            	<!--<p class="intro-large">Functional fashion for travel and leisure</p>-->
	        </div>
        	<div class="frontpage-grid__content">
            	<div class="frontpage-grid__content-wrapper">
                	<div class="frontpage-grid__cell desktop-full-width">
                    	<h2>Make color work for you.</h2>
                    	<p class="intro-large">Bikin hari kamu lebih berwarna</p>
                	</div>
        
        			
                    
                    <div class="frontpage-grid__cell">
						<a class="product ampmfront" href="http://ampmstore.com/brownrain">
                            <div class="product__image">
                                <img src="<?php echo base_url(); ?>assets/images/brownrain/brownrain.jpg" />
                                <div class="product__hover-image">
                                    <img src="<?php echo base_url(); ?>assets/images/brownrain/brownrain2.jpg" />
                                </div>
                            </div>
                            <div class="product__description">
                                <p class="product__name">Brown Rain</p>
                               <p class="product__color"><del>IDR 79.000</del></p>
                               <p class="product__price" ng-hide="product.DiscountedPriceAmount">IDR<span class="currency"> 63.200</span></p>
                               
                            </div>
                        </a>
                    </div>
                    
                    <div class="frontpage-grid__cell">
						<a class="product ampmfront" href="http://ampmstore.com/creamsprinkles">
                            <div class="product__image">
                                <img src="<?php echo base_url(); ?>assets/images/creamsprinkles/creamsprinkles.jpg" />
                                <div class="product__hover-image">
                                    <img src="<?php echo base_url(); ?>assets/images/creamsprinkles/creamsprinkles2.jpg" />
                                </div>
                            </div>
                            <div class="product__description">
                                <p class="product__name">Red Brick</p>
                               <p class="product__color"><del>IDR 79.000</del></p>
                               <p class="product__price" ng-hide="product.DiscountedPriceAmount">IDR<span class="currency"> 63.200</span></p>
                               
                            </div>
                        </a>
                    </div>
                    
                    <div class="frontpage-grid__cell desktop-full-width">
						<a href="http://ampmstore.com/brownrain" class="media-block__link">
    						<div class="media-block">
        						<div class="media-block__image">
            						<button type="button" class="media-block__image-toggler" title="Show full image"></button>
									<img src="<?php echo base_url(); ?>assets/images/brownrain/brownrain6.jpg" alt="" title="" />
        						</div>
                                <div class="media-block__text">
                                    <h2 class="tag " style="font-size:16px;">Over Deck</h2>
                                    <h2 style="font-size:15px;  ">Jahitan full over deck. jahitan yang menggunakan 3 jarum, agar terlihat lebih artistik dan secara kualitas jahitan ini lebih kuat daripada jahitan pada umumnya.</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                    
                     <div class="frontpage-grid__cell desktop-full-width">
                      <!--  <a href="/articles/premium-loafers/" class="promo-image">
                            <div class="promo-image__image">
                                <h2>Make color work for you</h2>
                            </div><br><br>
                            <div class="promo-image__text">
                                <p class="promo-image__tag">Breeze</p>
                                <p>Saul Carrasco styles our city look</p>
                            </div>
                        </a>-->
                    </div>
                    
                    <!--<div class="frontpage-grid__cell desktop-full-width">
						<a href="/articles/Breeze-through-life/" class="promo-image">
                            <div class="promo-image__image">
								<img src="http://ampmstore.com/demo/assets/images/produk/redbrick/redbrick5.jpg" />
						        <h2>Breeze</h2>
                            </div>
                            <div class="promo-image__text">
                                <p class="promo-image__tag">Spring 2016</p>
                                <p>Find out why your feet should'nt live without them!</p>
                            </div>
                        </a>
                    </div>
                    -->
                    <div class="frontpage-grid__cell">
						<a class="product ampmfront" href="http://ampmstore.com/greycharcoal">
                            <div class="product__image">
                                <img src="<?php echo base_url(); ?>assets/images/greycharcoal/greycharcoal.jpg" />
                                <div class="product__hover-image">
                                    <img src="<?php echo base_url(); ?>assets/images/greycharcoal/greycharcoal2.jpg" />
                                </div>
                            </div>
                            <div class="product__description">
                                <p class="product__name">Grey Charcoal</p>
                               <p class="product__color"><del>IDR 69.000</del></p>
                               <p class="product__price" ng-hide="product.DiscountedPriceAmount">IDR<span class="currency"> 52.200</span></p>
                               
                            </div>
                        </a>
                    </div>
                    
                    <div class="frontpage-grid__cell">
						<a class="product ampmfront" href="http://ampmstore.com/redbrick">
                            <div class="product__image">
                                <img src="<?php echo base_url(); ?>assets/images/redbrick/redbrick.jpg" />
                                <div class="product__hover-image">
                                    <img src="<?php echo base_url(); ?>assets/images/redbrick/redbrick2.jpg" />
                                </div>
                            </div>
                            <div class="product__description">
                                <p class="product__name">Red Brick</p>
                               <p class="product__color"><del>IDR 69.000</del></p>
                               <p class="product__price" ng-hide="product.DiscountedPriceAmount">IDR<span class="currency"> 52.200</span></p>
                               
                            </div>
                        </a>
                    </div>
                    
                    <div class="frontpage-grid__cell desktop-full-width">
						<a href="http://ampmstore.com/redbrick" class="media-block__link">
    						<div class="media-block">
        						<div class="media-block__image">
            						<button type="button" class="media-block__image-toggler" title="Show full image"></button>
									<img src="<?php echo base_url(); ?>assets/images/redbrick/redbrick4.jpg" alt="" title="" />
        						</div>
                                <div class="media-block__text">
                                    <h2 class="tag " style="font-size:16px;">SIGNATURE</h2>
                                    <h2 style="font-size:15px;  ">#Weareequal</h2>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    
                  

            	</div>
        	</div>
    	</div>

    	<div class="frontpage-grid__column" data-image="<?php echo base_url(); ?>assets/images/promoonline2.jpg" data-mobile-image="<?php echo base_url(); ?>assets/images/promoonlinemobile2.jpg">
        	<div class="frontpage-grid__scroll-indicator">More</div>
        	<div class="frontpage-grid__column__heading">
            	<h3 style="color:#fff;"> </h3>
            	<!--<p class="intro-large">Lorem Ipsum dolor sit amet</p>-->
        	</div>
        	<div class="frontpage-grid__content">
            	<div class="frontpage-grid__content-wrapper">
                	<div class="frontpage-grid__cell desktop-full-width">
                    	<h2>#colorwear.</h2>
                    	<p class="intro-large">Bikin hari kamu lebih berwarna</p>
                	</div>

                	<!--<div class="frontpage-grid__cell desktop-full-width">
						<a href="/articles/premium-loafers/" class="promo-image">
                            <div class="promo-image__image">
								<h2>Power of color</h2>
                            </div><br>
                            <div class="promo-image__text">
                                <p class="promo-image__tag">Summer 2016</p>
                                <p>Aenean accumsan imperdiet placerat. Integer facilisis ut leo fermentum imperdiet. </p>
                            </div>
                        </a>
                    </div>-->

                    <div class="frontpage-grid__cell desktop-full-width">
						<a href="http://ampmstore.com/color_relationship/" class="media-block__link">
                            <div class="media-block">
                                <div class="media-block__image">
                                    <button type="button" class="media-block__image-toggler" title="Show full image"></button>
								    <img src="<?php echo base_url(); ?>assets/images/header2.jpg">
						        </div>
                                <div class="media-block__text">
                                    <h2  style="font-size:18px;">COLOR IN HUMAN RELATIONSHIP</h2>
                                    <h2 style="font-size:15px;  ">Warna memainkan peranan sangat penting dalam kehidupan seharian. Ada ungkapan yang mengatakan bahwa warna merupakan cermin pribadi seseorang...<br><i>Selengkapnya</i></h2>
                                </div>
                            </div>
                        </a>
                    </div>
        
                    <div class="frontpage-grid__cell desktop-full-width">
						<a href="http://ampmstore.com/emotional_color/" class="media-block__link">
                            <div class="media-block">
                                <div class="media-block__image">
                                    <button type="button" class="media-block__image-toggler" title="Show full image"></button>
								    <img src="<?php echo base_url(); ?>assets/images/header4.jpg">
						        </div>
                                <div class="media-block__text">
                                    <h2  style="font-size:18px;">FIND COLOR OF YOUR EMOTIONAL</h2>
                                     <h2 style="font-size:15px;  ">Orang yang menyukai warna merah adalah pribadi yang berani, semangat dan suka tantangan. Mereka juga cocok dijadikan pemimpin karena...<br><i>Selengkapnya</i></h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    
                    <div class="frontpage-grid__cell desktop-full-width">
						<a href="http://ampmstore.com/secret_of_the_power_of_color/" class="media-block__link">
                            <div class="media-block">
                                <div class="media-block__image">
                                    <button type="button" class="media-block__image-toggler" title="Show full image"></button>
								    <img src="<?php echo base_url(); ?>assets/images/header6.jpg">
						        </div>
                                <div class="media-block__text">
                                    <h2   style="font-size:18px;">SECRET OF THE POWER OF COLOR</h2>
                                     <h2 style="font-size:15px;  ">Biru Mengandung makna Spiritual tinggi yang memberi ketenangan dan kesejukan air. Seperti yang banyak digunakan...<br><i>Selengkapnya</i></h2>
                                </div>
                            </div>
                        </a>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>