<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Account extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_account');
			if($this->session->userdata('loc_id') == ''){
			redirect('home');
		}
        }
		
     public function index() {
		$id = $this->session->userdata('loc_id');
		$sql = $this->db->query('select * from tbl_alamat');
			foreach($sql->result() as $hsl) {
				$idal = $hsl->id;
			}
		$data['query_cart'] = $this->m_general->addCart();
		$data['query_add'] = $this->m_account->getAddress();
		$data['province'] = $this->m_account->getFirstChoiceProvince();
		$data['city'] = $this->m_account->getCityChoice();
		$data['kecamatan'] = $this->m_account->getKecChoice();
		$data['deldress'] = $this->db->query('select * from tbl_alamat where id="'.$id.'"');
		$data['content'] = $this->load->view('account_view',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	
	
	public function editAccount() {
		$email = $this->input->post('email_account');
		$nama  = $this->input->post('nama_account');
		$no    = $this->input->post('no_telp');
		
		$data = array(
			'id'		=> $this->input->post('id_account'),
			'nama'     => $nama,
			'email'    => $email,
			'telepon'  => $no
		);
		$tabel = 'tbl_user';
		$kode  = $this->input->post('id_account');
		$field = 'id';
		$this->m_account->editDataAccount($tabel, $data, $field, $kode);
	}
	
	public function get_password(){
    	$password 	= md5($this->input->post('password'));
		$query 		= $this->m_account->cek_password($password);
		if ($query->num_rows() > 0) {
			echo $query->num_rows();
		}
    }
	
	public function editPasswordAccount() {
		$id = $this->input->post('id');
		$password = md5($this->input->post('password'));
		
		$data = array('id' => $id,
						'password'  => $password);
		$tabel = 'tbl_user';
		$kode  = $this->input->post('id');
		$field = 'id';
		$this->m_account->editDataPassword($tabel, $data, $field, $kode);
	}
	
	public function addAddress() {
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('telepon', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Nama Alamat', 'required');
		$this->form_validation->set_rules('country', 'Negara', 'required');
		$this->form_validation->set_rules('idprovince', 'Nama Provinsi', 'required');
		$this->form_validation->set_rules('idcity', 'Nama Kota', 'required');
		$this->form_validation->set_rules('idkecamatan', 'Nama Kecamatan', 'required');
		$this->form_validation->set_rules('kode_pos', 'Kodepos', 'required');
		if ($this->form_validation->run() == TRUE) {
		$nama = $this->input->post('nama');
		$telepon = $this->input->post('telepon');
		$alam = $this->input->post('alamat');
		$count = $this->input->post('country');
		$idpro = $this->input->post('idprovince');
		$city = $this->input->post('idcity');
		$kec  = $this->input->post('idkecamatan');
		$kode = $this->input->post('kode_pos');
		
		$data = array('nama'  => $nama,
						'alamat'=> $alam,
						'kecamatan' => $kec,
						'kota' => $city,
						'provinsi' => $idpro,
						'negara' => $count,
						'kodepos' => $kode,
						'telepon' => $telepon);
		$tabel = 'tbl_alamat';
		$this->m_account->insert_data($tabel, $data);
		$this->session->set_flashdata('pesan', 'Penambahan alamat baru berhasil.');
		redirect('account');
	}else{
		redirect('account');
	}
}
	
	public function editAddress() {
		$id = $this->input->post('idAdd');
		$nama = $this->input->post('edit_nama_lengkap');
		$telp = $this->input->post('edit_telp');
		$alam = $this->input->post('edit_alamat');
		$count = $this->input->post('edit_country');
		$idpro = $this->input->post('edit_idprovince');
		$city = $this->input->post('edit_idcity');
		$kec  = $this->input->post('edit_idkecamatan');
		$kode = $this->input->post('edit_kode_pos');
		$data = array('id' => $id,
						'nama'  => $nama,
						'alamat'=> $alam,
						'kecamatan' => $kec,
						'kota' => $city,
						'provinsi' => $idpro,
						'negara' => $count,
						'kodepos' => $kode,
						'telepon' => $telp);
		
		$tabel = 'tbl_alamat';
		$kode  = $id;
		$field = 'id';
		$this->m_account->editDataAddress($tabel, $data, $field, $kode);
		redirect('account');
	}
	
	public function delete_address(){
		$id = $this->session->userdata('loc_id');
    	$idal 	= $this->uri->segment(3);
		$this->db->where('id', $idal);
		$this->db->where('user', $id);
		$this->db->delete('tbl_alamat');
		redirect('account');
    }
	
	function select_city() {
		if('IS_AJAX') {
			$data['query'] = $this->m_account->getCityChoice();
			$this->load->view('pilihcity',$data);
		}
    }
	
	function select_kecamatan() {
		if('IS_AJAX') {
			$data['query'] = $this->m_account->getKecChoice();
			$this->load->view('pilih_kecamatan',$data);
		}
    }
	
	function edit_select_city() {
		if('IS_AJAX') {
			$data['query'] = $this->m_account->editCityChoice();
			$this->load->view('edit_alamat',$data);
		}
    }
	
	function edit_select_kecamatan() {
		if('IS_AJAX') {
			$data['query'] = $this->m_account->editKecChoice();
			$this->load->view('edit_alamat_kecamatan',$data);
		}
    }
}

?>