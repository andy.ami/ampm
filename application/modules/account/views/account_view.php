
<div class="page" tabindex="-1">
<br/><br/><br/><br/>
<div class="heading">
        <h1>My account</h1>

    <p><a class="my-account-log-out" href="<?php echo site_url("admin/logout"); ?>">Log out</a></p>
	<p><h4>Silahkan lengkapi informasi alamat dan akun Anda.</h4></p>
	
</div>

<div class="my-account-container">
    <ul class="tabs__triggers">
        <li class="is-active" id="account-tabs-account-info" aria-expanded="true" aria-controls="account-content-account-info" tabindex="0">Akun info</li>
        <li id="account-tabs-addresses" aria-expanded="false" aria-controls="account-content-addresses" tabindex="0">Alamat</li>
    </ul>
    <ul class="tabs__content-container">
        <li class="is-active" id="account-content-account-info">
            <div class="tabs__mobile-trigger" aria-expanded="true" aria-controls="mobile-tabs-id-0" tabindex="0">Account info</div>
<div class="tabs__content ng-scope" ng-controller="CheckboxNewsLetterController" id="mobile-tabs-id-0">
    <h2>Informasi Akun</h2>
	<?php
	foreach($query_add->result() as $row) {
		$id 	= $row->id;
		$email 	= $row->email;
		$nama	= $row->nama_user;
		$tlp	= $row->tlp_user;
	}

	if(!isset($id)) {
	echo '';
	}else{
	?>
<form class="form ng-pristine ng-valid" id="account-info-form" method="post" action="#" novalidate="novalidate">
        <label class="text-input-label is-active" for="account_email">E-mail</label>
		<input type="hidden" class="text-input" id="id_account" name="id_account" value="<?php echo $id; ?>">
        <input type="text" class="text-input" id="email_account" name="email_account" value="<?php echo $email; ?>" maxlength="50">
        <label class="text-input-label" for="nama_account">Nama Lengkap</label>
        <input type="text" class="text-input" id="nama_account" name="nama_account" value="<?php echo $nama; ?>" maxlength="15">
        <label class="text-input-label" for="no_telp">Nomer Telepon</label>
        <input type="text" class="text-input" id="no_telp" name="no_telp" value="<?php echo $tlp; ?>" maxlength="15">
  
        <p class="error-message" id="account-info-form-error" style="display: none;">The e-mail is not valid</p>
        <p class="confirmation-message" id="account-info-form-confirmation" style="display: none;">Account telah terupdate.</p>
        <input type="button" id="edit-account" value="Save changes">
    </form>
	<?php } ?>
    <script type="text/javascript">
        $('#edit-account').click(function () {
             var email_account = $('#email_account').val();
             var nama_account = $('#nama_account').val();
             var no_telp = $('#no_telp').val();
            $.ajax({
                url: '<?php echo site_url('Account/editAccount'); ?>',
                type: 'POST',
                data: $('#account-info-form').serialize(),
                success: function (data) {
                    if(data == '') {
						$('#account-info-form-error').slideUp(500);
                        $('#account-info-form-confirmation').slideDown(500);
                    }
                }
            });
        });
        $('#account-info-form-confirmation').hide();
        $('#account-info-form-error').hide();
    </script>
	<?php
	if(!isset($id)) {
	echo '';
	}else{
	?>
    <form class="form ng-pristine ng-valid" id="account-info-change-password-form" method="post" action="#" novalidate="novalidate">
        <h4>Ubah password</h4>
		<input type="hidden" class="text-input" id="id" name="id" value="<?php echo $id; ?>">
        <div class="password-wrapper"><label class="text-input-label" for="old_password">Old password</label><input type="password" class="text-input" id="old_password" name="old_password" onkeyup="cek_password();">
		<button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        
        <div class="password-wrapper"><label class="text-input-label" for="password">New password</label><input type="password" class="text-input" id="password" name="password"><button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        
        <p class="error-message" id="account-info-change-password-form-error" style="display: none;">Tidak dapat merubah password.</p>
        <p class="confirmation-message" id="account-info-change-password-form-confirmation" style="display: none;">Password telah diubah</p>
        <input type="button" id="edit-password" value="Ubah password">
    </form>
	<?php } ?>
    <script>
	$(document).ready(function(){
    document.getElementById("password").disabled = true;
  });
  
	
  
  function cek_password() {
    var password = document.getElementById("old_password").value;
    var bg = document.getElementById("old_password");
    if ($(password).val() != 0) {
    $.post('<?php echo base_url('account/get_password'); ?>', 
		{password:password}, function(result) {
        if (result != '') {
            document.getElementById("old_password").disabled = true;
            document.getElementById("password").disabled = false;
            document.getElementById('password').focus();
            document.getElementById("old_password").value = "Password yang anda masukan valid...";
            bg.style.backgroundColor = "#eeeeee";
			
          }else{
            document.getElementById("old_password").disabled = false;
            document.getElementById("password").disabled = true;

            if(bg.value != ''){
                bg.style.backgroundColor = "#fdee21";
            }else{
                bg.style.backgroundColor = "#fff";
            }
          }
          
    });
    }
  }
		

		$('#edit-password').click(function () {
             var password = document.getElementById("password").value;
			 if(password == '') {
				$('#account-info-change-password-form-error').show();
			 }else{
            $.ajax({
                url: '<?php echo site_url('account/editPasswordAccount'); ?>',
                type: 'POST',
				data: $('#account-info-change-password-form').serialize(),
                success: function (data) {
                    if(data == '') {
						$('#account-info-change-password-form-error').slideUp(500);
                        $('#account-info-change-password-form-confirmation').slideDown(500);
					}
				}
			});
			}
		});
	</script>
</div>

<script type="text/javascript">
/*		$('#delete-alamat').click(function () {
			alert('anjiing');
			var idal = document.getElementById("del_alamat").value;
			alert(idal);
        $.ajax({
            url: '<?php echo site_url('account/delete_address'); ?>',
            type: 'POST',
            data: {idal: idal},
            success: function(data) {
				//alert(data);
				window.location.reload();
            }
        });
    });*/
	</script>
	
        </li>
        <li id="account-content-addresses">
            


<div class="tabs__mobile-trigger" aria-expanded="true" aria-controls="mobile-tabs-id-1" tabindex="0">Address</div>
<div class="tabs__content" id="mobile-tabs-id-1">
    <h2>Alamat User</h2>
    <form class="customer-address-list ng-pristine ng-valid" id="my-account-addresses" method="post" action="<?php echo site_url('account/addAddress'); ?>" novalidate="novalidate">
    <ul>
        <?php 
		foreach($query_add->result() as $row) {
			$idal = $row->id_alamat;
		?>
			<li>
	            <div class="customer-address-list__data" tabindex="-1">
	<p>
    <?php echo $row->nama; ?><br>
    <?php echo $row->nama_alamat; ?><br>
    
     <?php echo $row->name_city; ?> - <?php echo $row->name_kecamatan; ?> <br>
     <?php echo $row->name_province; ?>  <?php echo $row->kodepos; ?><br>
    <?php echo $row->telepon; ?>
</p>
		
                        <!--<input class="radio" type="radio" name="account-shipping-addr" id="de56c759-21c0-4b93-b016-803df21da008-shipping-radio">
                        <label for="de56c759-21c0-4b93-b016-803df21da008-shipping-radio">Shipping address</label>
                        <input class="radio" type="radio" name="account-billing-addr" id="de56c759-21c0-4b93-b016-803df21da008-billing-radio">
                        <label for="de56c759-21c0-4b93-b016-803df21da008-billing-radio">Billing address</label> -->
                        <!--<button class="edit-address" onclick="window.location.href='<?php echo site_url('account/edit_alamat'); ?>'">Edit</button>-->
						<button class="edit-address" onClick="myFunction()">Edit</button>
                        <button><a href="<?php echo site_url('account/delete_address/'.$row->id_alamat); ?>">Delete</a></button>
						
                    </div>
			<?php } ?>
                    <div class="customer-address-list__form" tabindex="-1">
                        <input type="checkbox" class="address-was-changed" id="<?php echo $idal; ?>" style="display: block">
                        
<?php
	foreach($query_add->result() as $row) {
		$idAdd 	= $row->id_alamat;
		$idpro  = $row->idprovince;
		$idcit  = $row->idcity;
		$idkec  = $row->idkecamatan;
		$email 	= $row->email;
		$nama	= $row->nama;
		$tlp	= $row->telepon;
		$alm	= $row->nama_alamat;
		$neg	= $row->negara;
		$pro	= $row->name_province;
		$cit	= $row->name_city;
		$kec	= $row->name_kecamatan;
		$kode	= $row->kodepos;
	}

	if(!isset($alm)) {
	echo '';
	}else{
	?>
<input type="hidden" class="text-input" id="idAdd" name="idAdd" value="<?php echo $idAdd; ?>" maxlength="35">
<label class="text-input-label" for="edit_nama_lengkap">Nama Lengkap *</label>
<input type="text" class="text-input" id="edit_nama_lengkap" name="edit_nama_lengkap" value="<?php echo $nama; ?>" maxlength="35">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="no_telp"> Telepon *</label>
<input type="text" class="text-input" id="edit_telp" name="edit_telp" maxlength="20" value="<?php echo $tlp; ?>">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="edit_alamat">Alamat *</label>
<input type="text" class="text-input" id="edit_alamat" name="edit_alamat" maxlength="35" value="<?php echo $alm; ?>">
<span class="field-validation-valid"></span>

<label for="country" class="select-label">Country *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="edit_country" name="edit_country" onchange="countryUpdated(this)">
<option value="<?php echo $neg; ?>"><?php echo $neg; ?></option>
</select></div>


<label for="idprovince" class="select-label">Provinsi *</label>
<div class="select"><select class="select" id="edit_idprovince" name="edit_idprovince">
 <?php
 if(isset($idpro) && ($pro)) { ?>
 <option value="<?php echo $idpro; ?>"><?php echo $pro; ?></option>
<?php
	foreach($province->result() as $row) {
?>
<option value="<?php echo $row->idprovince; ?>"><?php echo $row->name_province; ?></option>
	<?php }} ?>	
</select></div>

<div id='edit_city'>
<label for="kota" class="select-label">Kota *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="edit_idcity" name="edit_idcity">
<?php
 if(isset($idcit) && ($cit)) { ?>
 <option value="<?php echo $idcit; ?>"><?php echo $cit; ?></option>
<?php
	foreach($city->result() as $row) {
?>
<option value="<?php echo $row->idcity; ?>"><?php echo $row->name_city; ?></option>
	<?php }} ?>
</select></div></div>

<div id='edit_kecamatan'>
<label for="kecamatan" class="select-label">Kecamatan *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="edit_idkecamatan" name="edit_idkecamatan" disabled="disabled">
<?php
 if(isset($idkec) && ($kec)) { ?>
 <option value="<?php echo $idkec; ?>"><?php echo $kec; ?></option>
<?php
	foreach($kecamatan->result() as $row) {
?>
<option value="<?php echo $row->idkecamatan; ?>"><?php echo $row->name_kecamatan; ?></option>
	<?php }} ?>
</select></div></div>

<label class="text-input-label" for="kode_pos">Postal code *</label>
<input type="text" class="text-input" id="edit_kode_pos" name="edit_kode_pos" maxlength="9" value="<?php echo $kode; ?>">

                        <button class="cancel-edit-address">Cancel</button>
						<br/><br/>
						<span class="field-validation-valid" id="edit_valid"></span>
						<br/><br/>
						<input type="button" id="btn_edit_address" value="Edit">
                    </div>
	<?php } ?>
                </li>
        </ul>
		</form>
		
		
		

	
<!-- Tambah Alamat Baru -->
<form class="customer-address-list ng-pristine ng-valid" id="edit-addresses" method="post" action="<?php echo site_url('account/addAddress'); ?>" novalidate="novalidate">
        <div class="customer-address-list__add-form">
            <h3>Alamat Baru</h3>
           

<label class="text-input-label" for="nama">Nama Lengkap *</label>
<input type="text" class="text-input" id="nama" name="nama" maxlength="35">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="telepon">Nomer Telepon *</label>
<input type="text" class="text-input" id="telepon" name="telepon" maxlength="20">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="alamat">Alamat *</label>
<input type="text" class="text-input" id="alamat" name="alamat" maxlength="35">
<span class="field-validation-valid"></span>

<label for="country" class="select-label">Country *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="country" name="country">
<option value="INDONESIA">Indonesia</option>
</select></div>


<label for="idprovince" class="select-label">Provinsi *</label>
<div class="select"><select class="select" id="idprovince" name="idprovince">
 <?php
	foreach($province->result() as $row) {
?>
<option value="<?php echo $row->idprovince; ?>"><?php echo $row->name_province; ?></option>
	<?php } ?>
</select></div>

<div id='city'>
<label for="kota" class="select-label">Kota *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="idcity" name="idcity" disabled="disabled">
<option value="">Pilih Kota</option>
<?php
	foreach($city->result() as $row) {
?>
<option value="<?php echo $row->idcity; ?>"><?php echo $row->name_city; ?></option>
<?php } ?>
</select></div></div>

<div id='kecamatan'>
<label for="kecamatan" class="select-label">Kecamatan *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="idkecamatan" name="idkecamatan" disabled="disabled">
<option value="">Pilih Kecamatan</option>
<?php
	foreach($kecamatan->result() as $row) {
?>
<option value="<?php echo $row->idkecamatan; ?>"><?php echo $row->name_kecamatan; ?></option>
<?php } ?>
</select></div></div>

<label class="text-input-label" for="kode_pos">Postal code *</label>
<input type="text" class="text-input" id="kode_pos" name="kode_pos" maxlength="9">
<span class="field-validation-valid"></span>
</div>

    <p class="error-message" id="my-account-addresses-error"></p>
        <button class="customer-address-list__add-form-trigger" id="customer-address-list-new-add-button" data-added="false">Add address</button>
        <input type="submit" id="btn_add_address" value="Save changes">
    </form>
	
	<!-- Javascriipt untuk tambah Alamat User -->
	<script>
	$('#btn_add_address').click(function (event) {
		event.preventDefault();
             var nama = document.getElementById("nama").value;
			 var telepon = document.getElementById("telepon").value;
			 var alamat = document.getElementById("alamat").value;
			 var country = document.getElementById("country").value;
			 var idprovince = document.getElementById("idprovince").value;
			 var idcity = document.getElementById("idcity").value;
			 var idkecamatan = document.getElementById("idkecamatan").value;
			 var kode_pos = document.getElementById("kode_pos").value;
		if(nama == '' || telp == '' || alamat == '' || country == '' || idprovince == '' || idcity == '' || idkecamatan == '' || kode_pos == '') {
				$('#my-account-addresses-error').html('Lengkapi data akun Anda.');
			 }else{
            $.ajax({
                url: '<?php echo site_url('account/addAddress'); ?>',
                type: 'POST',
				data: {'nama':nama, 'telepon':telepon, 'alamat':alamat, 'country':country, 'idprovince':idprovince, 'idcity':idcity, 'idkecamatan':idkecamatan, 'kode_pos':kode_pos},
                success: function (data) {
                    if(data) {
						window.location.reload('account');
					}
				}
			});
			}
		});
	</script>
	
	<!-- Javascriipt untuk Edit Alamat User -->
	<script>
	$('#btn_edit_address').click(function () {
			var idAdd = document.getElementById("idAdd").value;
             var edit_nama_lengkap = document.getElementById("edit_nama_lengkap").value;
			 var edit_telp = document.getElementById("edit_telp").value;
			 var edit_alamat = document.getElementById("edit_alamat").value;
			 var edit_country = document.getElementById("edit_country").value;
			 var edit_idprovince = document.getElementById("edit_idprovince").value;
			 var edit_idcity = document.getElementById("edit_idcity").value;
			 var edit_idkecamatan = document.getElementById("edit_idkecamatan").value;
			 var edit_kode_pos = document.getElementById("edit_kode_pos").value;
			if(edit_nama_lengkap == '' || edit_telp == '' || edit_alamat == '' || edit_country == '' || edit_idprovince == '' || edit_idcity == '' || edit_idkecamatan == '' || edit_kode_pos == '') {
				$('#my-account-addresses-error').html('Lengkapi data diri Anda.');
			 }else{
            $.ajax({
                url: '<?php echo site_url('account/editAddress'); ?>',
                type: 'POST',
				data: {'idAdd':idAdd, 'edit_nama_lengkap':edit_nama_lengkap, 'edit_telp':edit_telp, 'edit_alamat':edit_alamat, 'edit_country':edit_country, 'edit_idprovince':edit_idprovince, 'edit_idcity':edit_idcity, 'edit_idkecamatan':edit_idkecamatan, 'edit_kode_pos':edit_kode_pos},
                success: function (data) {
                    if(data) {
						$('#my-account-addresses-error').slideUp(500);
                        $('#edit_valid').html('Perubahan alamat berhasil.');
						window.location.reload('account');
					}
				}
				
			});
			 }
			});
		
	</script>
	
	
    <script type="text/javascript">
        $('#customer-address-list-new-add-button').click(function () {
            $(this).data("added", "true");
        });
		
        /*$('.delete-address').click(function () {
            event.preventDefault();
			var idal = $('#del_alamat').val();
			alert(idal);
			/*var data = {
                id = $(this).data('address-id')
            };
            $.ajax({
                url: '<?php echo site_url('account/delete_address'); ?>',
                type: 'POST',
                data: {idal:idal},
//                processData: false,
                success: function (data) {
					alert('done');
                    window.location.reload();
                }
            });
        }); */
        function getAddressFromForm(prefix, id) {
            return {
                Id: id,
                FirstName: $('#' + prefix + '-firstname').val(),
                LastName: $('#' + prefix + '-lastname').val(),
                Address1: $('#' + prefix + '-address1').val(),
                Address2: $('#' + prefix + '-address2').val(),
                PostalCode: $('#' + prefix + '-postalcode').val(),
                City: $('#' + prefix + '-city').val(),
                State: $('#' + prefix + '-state').val(),
                Province: $('#' + prefix + '-province').val(),
                CountryCode: $('#' + prefix + '-country').val(),
                Telephone: $('#' + prefix + '-telephone').val(),
                IsPreferredBillingAddress: $('#' + prefix + '-billing-radio').prop('checked'),
                IsPreferredShippingAddress: $('#' + prefix + '-shipping-radio').prop('checked')

            };
        }

        function removeValidation() {
            $("#my-account-addresses .field-validation-error").text("");
            $("#my-account-addresses .field-validation-error").removeClass("field-validation-error").addClass("field-validation-valid");
            $("#my-account-addresses input.validation-error").removeClass("validation-error");
        }

        function setErrorMessage(id, text) {
            $(id).addClass("validation-error");
            $(id).next().text(text);
        }

        function countryUpdatedMyPage(obj) {
            $("#" + $(obj).data("id") + "-canadaProvince").hide();
            $("#" + $(obj).data("id") + "-usStates").hide();
            if ($(obj).val().toLowerCase() === "us") {
                $("#" + $(obj).data("id") + "-usStates").show();
            }
            if ($(obj).val().toLowerCase() === "ca") {
                $("#" + $(obj).data("id") + "-canadaProvince").show();
            }
        }
		
		function myFunction() {
			//document.getElementById("customer-address-list-new-add-button").hide();
			document.getElementById('customer-address-list-new-add-button').style.display='none';
			document.getElementById("btn_add_address").style.visibility="hidden";
		}
		
        $(document).ready(function() {
            $("#customer-address-list-new-country").val('US');
            countryUpdatedMyPage($("#customer-address-list-new-country"));

            $(".cancel-edit-address").click(function () {
				
                $(this).parent().children(".address-was-changed").prop('checked', false);;
            });
			
			$(".edit-address").click(function () {
				//$('#customer-address-list-new-add-button').hide();
				document.getElementById('customer-address-list-new-add-button').style.display='none';
			document.getElementById("btn_add_address").style.visibility="hidden";
				//$('#edit-addresses').hide();
				
            });
        });

    </script>
</div>

        </li>
    </ul>
</div>
<script type="text/javascript">
    function updateAccountTabs() {
        $('#account-tabs-account-info, #account-content-account-info').toggleClass('is-active', (window.location.hash == '#account-info' || window.location.hash == ''));
        $('#account-tabs-addresses, #account-content-addresses').toggleClass('is-active', (window.location.hash == '#addresses'));
        $('#account-tabs-orders, #account-content-orders').toggleClass('is-active', (window.location.hash == '#orders'));
        $('#account-tabs-returnorders, #account-content-returnorders').toggleClass('is-active', (window.location.hash == '#returnorders'));
    }

    updateAccountTabs();

    $('#account-tabs-account-info').click(function () {
        window.location.hash = "#account-info";
        updateAccountTabs();
    });
    $('#account-tabs-addresses').click(function () {
        window.location.hash = "#addresses";
        updateAccountTabs();
    });
    $('#account-tabs-orders').click(function () {
        window.location.hash = "#orders";
        updateAccountTabs();
    });
    $('#account-tabs-returnorders').click(function () {
        window.location.hash = "#returnorders";
        updateAccountTabs();
    });

$("#idprovince").change(function(){
                var selectValues = $("#idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#city').html(msg);
                }else{
                    var idprovince = {idprovince:$("#idprovince").val()};
                    $('#idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/select_city')?>",
                            data: idprovince,
                            success: function(msg){
                                $('#city').html(msg);
                            }
                    });
                }
        });
		
$('body').delegate("#idcity","change", function() {
                var selectValues = $("#idcity").val();
                if (selectValues == 0){
                    var msg = "<select name=\"idkecamatan\" disabled><option value=\"Pilih Kecamatan\">Pilih Kota Dulu</option></select>";
                    $('#kecamatan').html(msg);
                }else{
                    var idcity = {idcity:$("#idcity").val()};
                    $('#idkecamatan').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/select_kecamatan')?>",
                            data: idcity,
                            success: function(msg){
                                $('#kecamatan').html(msg);
                            }
                    });
                }
        });
		
$('body').delegate("#edit_idcity","change", function() {
        var selectValues = $("#edit_idcity").val();
		//alert(selectValues);
        if (selectValues == 0){
            var msg = "<select name=\"edit_idkecamatan\" disabled><option value=\"Pilih Kecamatan\">Pilih Kota Dulu</option></select>";
            $('#edit_kecamatan').html(msg);
        }else{
            var edit_idcity = {edit_idcity:$("#edit_idcity").val()};
            $('#edit_idkecamatan').attr("disabled",true);
            $.ajax({
                type: "POST",
                url : "<?php echo site_url('account/edit_select_kecamatan')?>",
                data: edit_idcity,
                success: function(msg){
                    $('#edit_kecamatan').html(msg);
                }
            });
        }
    });
	
$("#edit_idprovince").change(function(){				
                var selectValues = $("#edit_idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"edit_idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#edit_city').html(msg);
                }else{
                    var edit_idprovince = {edit_idprovince:$("#edit_idprovince").val()};
                    $('#edit_idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/edit_select_city')?>",
                            data: edit_idprovince,
                            success: function(msg){
                                $('#edit_city').html(msg);
                            }
                    });
                }
        });
	
	
</script>


</div>
