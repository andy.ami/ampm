
<div class="page" tabindex="-1">
<br/><br/><br/><br/>
<div class="heading">
        <h1>My account</h1>

    <p><a class="my-account-log-out" href="<?php echo site_url("admin/logout"); ?>">Log out</a></p>
</div>

<div class="my-account-container">
    <ul class="tabs__triggers">
        <li class="is-active" id="account-tabs-account-info" aria-expanded="true" aria-controls="account-content-account-info" tabindex="0">Account info</li>
        <li id="account-tabs-addresses" aria-expanded="false" aria-controls="account-content-addresses" tabindex="0">Address</li>
        <li id="account-tabs-orders" aria-expanded="false" aria-controls="account-content-orders" tabindex="0">Orders</li>
    </ul>
    <ul class="tabs__content-container">
        <li class="is-active" id="account-content-account-info">
            <div class="tabs__mobile-trigger" aria-expanded="true" aria-controls="mobile-tabs-id-0" tabindex="0">Account info</div>
<div class="tabs__content ng-scope" ng-controller="CheckboxNewsLetterController" id="mobile-tabs-id-0">
    <h2>Account information</h2>
	<?php
	foreach($query_add->result() as $row) {
		$id 	= $row->id;
		$email 	= $row->email;
		$nama	= $row->nama_user;
		$tlp	= $row->tlp_user;
	}
	?>
<form class="form ng-pristine ng-valid" id="account-info-form" method="post" action="#" novalidate="novalidate">
        <label class="text-input-label is-active" for="account_email">E-mail</label>
		<input type="hidden" class="text-input" id="id_account" name="id_account" value="<?php echo $id; ?>">
        <input type="text" class="text-input" id="email_account" name="email_account" value="<?php echo $email; ?>" maxlength="50">
        <label class="text-input-label" for="nama_account">Nama Lengkap</label>
        <input type="text" class="text-input" id="nama_account" name="nama_account" value="<?php echo $nama; ?>" maxlength="15">
        <label class="text-input-label" for="no_telp">Nomer Telepon</label>
        <input type="text" class="text-input" id="no_telp" name="no_telp" value="<?php echo $tlp; ?>" maxlength="15">
  
        <p class="error-message" id="account-info-form-error" style="display: none;">The e-mail is not valid</p>
        <p class="confirmation-message" id="account-info-form-confirmation" style="display: none;">Account telah terupdate.</p>
        <input type="button" id="edit-account" value="Save changes">
    </form>
    <script type="text/javascript">
        $('#edit-account').click(function () {
             var email_account = $('#email_account').val();
             var nama_account = $('#nama_account').val();
             var no_telp = $('#no_telp').val();
            $.ajax({
                url: '<?php echo site_url('Account/editAccount'); ?>',
                type: 'POST',
                data: $('#account-info-form').serialize(),
                success: function (data) {
                    if(data == '') {
						$('#account-info-form-error').slideUp(500);
                        $('#account-info-form-confirmation').slideDown(500);
                    }
                }
            });
        });
        $('#account-info-form-confirmation').hide();
        $('#account-info-form-error').hide();
    </script>
    <form class="form ng-pristine ng-valid" id="account-info-change-password-form" method="post" action="#" novalidate="novalidate">
        <h4>Ubah password</h4>
		<input type="hidden" class="text-input" id="id" name="id" value="<?php echo $id; ?>">
        <div class="password-wrapper"><label class="text-input-label" for="old_password">Old password</label><input type="password" class="text-input" id="old_password" name="old_password" onkeyup="cek_password();">
		<button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        
        <div class="password-wrapper"><label class="text-input-label" for="password">New password</label><input type="password" class="text-input" id="password" name="password"><button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        
        <p class="error-message" id="account-info-change-password-form-error" style="display: none;">Tidak dapat merubah password.</p>
        <p class="confirmation-message" id="account-info-change-password-form-confirmation" style="display: none;">Password telah diubah</p>
        <input type="button" id="edit-password" value="Ubah password">
    </form>
    <script>
	$(document).ready(function(){
    document.getElementById("password").disabled = true;
  });
  
	
  
  function cek_password() {
    var password = document.getElementById("old_password").value;
    var bg = document.getElementById("old_password");
    if ($(password).val() != 0) {
    $.post('<?php echo base_url('account/get_password'); ?>', 
		{password:password}, function(result) {
        if (result != '') {
            document.getElementById("old_password").disabled = true;
            document.getElementById("password").disabled = false;
            document.getElementById('password').focus();
            document.getElementById("old_password").value = "Password yang anda masukan valid...";
            bg.style.backgroundColor = "#eeeeee";
			
          }else{
            document.getElementById("old_password").disabled = false;
            document.getElementById("password").disabled = true;

            if(bg.value != ''){
                bg.style.backgroundColor = "#fdee21";
            }else{
                bg.style.backgroundColor = "#fff";
            }
          }
          
    });
    }
  }
		

		$('#edit-password').click(function () {
             var password = document.getElementById("password").value;
			 if(password == '') {
				$('#account-info-change-password-form-error').show();
			 }else{
            $.ajax({
                url: '<?php echo site_url('account/editPasswordAccount'); ?>',
                type: 'POST',
				data: $('#account-info-change-password-form').serialize(),
                success: function (data) {
                    if(data == '') {
						$('#account-info-change-password-form-error').slideUp(500);
                        $('#account-info-change-password-form-confirmation').slideDown(500);
					}
				}
			});
			}
		});
	</script>
</div>

        </li>
        <li id="account-content-addresses">
            


<div class="tabs__mobile-trigger" aria-expanded="true" aria-controls="mobile-tabs-id-1" tabindex="0">Address</div>
<div class="tabs__content" id="mobile-tabs-id-1">
   
    <form class="customer-address-list" id="edit-addresses" method="post" action="<?php echo site_url('account/addAddress'); ?>" novalidate="novalidate">
        <ul>
			<li>
				
                    <div class="customer-address-list__form">
                        <input type="checkbox" class="address-was-changed" id="de56c759-21c0-4b93-b016-803df21da008" style="display: block">
                        

<label class="text-input-label" for="edit_nama_lengkap">Nama  *</label>
<input type="text" class="text-input" id="edit_nama_lengkap" name="edit_nama_lengkap" maxlength="35">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="no_telp"> Telepon *</label>
<input type="text" class="text-input" id="edit_telp" name="edit_telp" maxlength="20">
<span class="field-validation-valid"></span>
<label class="text-input-label" for="edit_alamat">Alamat *</label>
<input type="text" class="text-input" id="edit_alamat" name="edit_alamat" maxlength="35">
<span class="field-validation-valid"></span>

<label for="country" class="select-label">Country *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="edit_country" name="edit_country" onchange="countryUpdated(this)">
<option value="INDONESIA">Indonesia</option>
</select></div>


<label for="idprovince" class="select-label">Provinsi *</label>
<div class="select"><select class="select" id="edit_idprovince" name="edit_idprovince">
 <?php
	foreach($province->result() as $row) {
?>
<option value="<?php echo $row->idprovince; ?>"><?php echo $row->name_province; ?></option>
	<?php } ?>
</select></div>
<div id='edit_city'>
<label for="kota" class="select-label">Kota *</label>
<div class="select"><select class="select" data-id="customer-address-list-new" id="edit_idcity" name="edit_idcity">
<option value="INDONESIA">Pilih Kota</option>

</select></div></div>

<label class="text-input-label" for="kode_pos">Postal code *</label>
<input type="text" class="text-input" id="edit_kode_pos" name="edit_kode_pos" maxlength="9">
<span class="field-validation-valid"></span>

                        <button class="cancel-edit-address">Cancel</button>
						<button class="customer-address-list__add-form-trigger" id="customer-address-list-new-add-button" data-added="false">Edit address</button>
                    </div>
                </li>
        </ul>

    <p class="error-message" id="my-account-addresses-error"></p>
        <button class="customer-address-list__add-form-trigger" id="customer-address-list-new-add-button" data-added="false">Add address</button>
        <input type="button" id="btn_add_address" value="Save changes">
    </form>
	<script>
	$('#btn_add_address').click(function () {
             var nama = document.getElementById("nama_lengkap").value;
			 var telp = document.getElementById("telp").value;
			 var alamat = document.getElementById("alamat").value;
			 var country = document.getElementById("country").value;
			 var idpro = document.getElementById("idprovince").value;
			 var idcity = document.getElementById("idcity").value;
			 var kode = document.getElementById("kode_pos").value;
	if(nama == '' || telp == '' || alamat == '' || country == '' || idpro == '' || idcity == '' || kode == '') {
				$('#my-account-addresses-error').html('Lengkapi data diri Anda.');
			 }else{
            $.ajax({
                url: '<?php echo site_url('account/addAddress'); ?>',
                type: 'POST',
				data: {'nama':nama, 'telp':telp, 'alamat':alamat, 'country':country, 'idpro':idpro, 'idcity':idcity, 'kode':kode},
                success: function (data) {
                    if(data == '') {
						$('#my-account-addresses-error').slideUp(500);
                        $('.field-validation-valid').slideDown(500);
					}
				}
			});
			}
		});
	</script>
    <script type="text/javascript">
        $('#customer-address-list-new-add-button').click(function () {
            $(this).data("added", "true");
        });
        $('.delete-address').click(function () {
            event.preventDefault();
            var data = {
                id: $(this).data('address-id')
            };
            $.ajax({
                url: '/Account/DeleteAddress',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                processData: false,
                success: function (result) {
                    window.location.reload();
                }
            });
        });
        function getAddressFromForm(prefix, id) {
            return {
                Id: id,
                FirstName: $('#' + prefix + '-firstname').val(),
                LastName: $('#' + prefix + '-lastname').val(),
                Address1: $('#' + prefix + '-address1').val(),
                Address2: $('#' + prefix + '-address2').val(),
                PostalCode: $('#' + prefix + '-postalcode').val(),
                City: $('#' + prefix + '-city').val(),
                State: $('#' + prefix + '-state').val(),
                Province: $('#' + prefix + '-province').val(),
                CountryCode: $('#' + prefix + '-country').val(),
                Telephone: $('#' + prefix + '-telephone').val(),
                IsPreferredBillingAddress: $('#' + prefix + '-billing-radio').prop('checked'),
                IsPreferredShippingAddress: $('#' + prefix + '-shipping-radio').prop('checked')

            };
        }

        function removeValidation() {
            $("#my-account-addresses .field-validation-error").text("");
            $("#my-account-addresses .field-validation-error").removeClass("field-validation-error").addClass("field-validation-valid");
            $("#my-account-addresses input.validation-error").removeClass("validation-error");
        }

        function setErrorMessage(id, text) {
            $(id).addClass("validation-error");
            $(id).next().text(text);
        }

        function countryUpdatedMyPage(obj) {
            $("#" + $(obj).data("id") + "-canadaProvince").hide();
            $("#" + $(obj).data("id") + "-usStates").hide();
            if ($(obj).val().toLowerCase() === "us") {
                $("#" + $(obj).data("id") + "-usStates").show();
            }
            if ($(obj).val().toLowerCase() === "ca") {
                $("#" + $(obj).data("id") + "-canadaProvince").show();
            }
        }

        $(document).ready(function() {
            $("#customer-address-list-new-country").val('US');
            countryUpdatedMyPage($("#customer-address-list-new-country"));

            $(".cancel-edit-address").click(function () {
				
                $(this).parent().children(".address-was-changed").prop('checked', false);;
            });
			
			$(".edit-address").click(function () {
				$('#customer-address-list-new-add-button').hide();
				$('#btn_add_address').hide();
				$('#edit-addresses').hide();
				
            });
        });

    </script>
</div>

        </li>
        <li id="account-content-orders">
            <div class="tabs__mobile-trigger" aria-expanded="true" aria-controls="mobile-tabs-id-2" tabindex="0">Orders</div>
<div class="tabs__content" id="mobile-tabs-id-2">
    <h2>My orders</h2>
    <ul class="order-history">
    </ul>
</div>

        </li>
    </ul>
</div>
<script type="text/javascript">
    function updateAccountTabs() {
        $('#account-tabs-account-info, #account-content-account-info').toggleClass('is-active', (window.location.hash == '#account-info' || window.location.hash == ''));
        $('#account-tabs-addresses, #account-content-addresses').toggleClass('is-active', (window.location.hash == '#addresses'));
        $('#account-tabs-orders, #account-content-orders').toggleClass('is-active', (window.location.hash == '#orders'));
        $('#account-tabs-returnorders, #account-content-returnorders').toggleClass('is-active', (window.location.hash == '#returnorders'));
    }

    updateAccountTabs();

    $('#account-tabs-account-info').click(function () {
        window.location.hash = "#account-info";
        updateAccountTabs();
    });
    $('#account-tabs-addresses').click(function () {
        window.location.hash = "#addresses";
        updateAccountTabs();
    });
    $('#account-tabs-orders').click(function () {
        window.location.hash = "#orders";
        updateAccountTabs();
    });
    $('#account-tabs-returnorders').click(function () {
        window.location.hash = "#returnorders";
        updateAccountTabs();
    });

$("#idprovince").change(function(){				
                var selectValues = $("#idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#city').html(msg);
                }else{
                    var idprovince = {idprovince:$("#idprovince").val()};
                    $('#idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/select_city')?>",
                            data: idprovince,
                            success: function(msg){
                                $('#city').html(msg);
                            }
                    });
                }
        });
		
$("#edit_idprovince").change(function(){				
                var selectValues = $("#edit_idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"edit_idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#edit_city').html(msg);
                }else{
                    var edit_idprovince = {edit_idprovince:$("#edit_idprovince").val()};
                    $('#edit_idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/edit_select_city')?>",
                            data: edit_idprovince,
                            success: function(msg){
                                $('#edit_city').html(msg);
                            }
                    });
                }
        });
</script>


</div>
