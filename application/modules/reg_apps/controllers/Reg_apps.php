<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reg_apps extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_regapps');
        }
		
    public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('reg_appsview',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
        	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>