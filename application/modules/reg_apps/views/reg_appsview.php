<div class="page" tabindex="-1">


<br/><br/><br/>
<div class="centered-content">
    <div class="heading">
        <h1>Registrasi</h1>
    </div>
    <div id="loginRegistration">
        <form action="<?php echo site_url('register/RegMobile'); ?>" class="form" id="register-form-page" method="post" novalidate="novalidate">    
		
		<div class="form-group">
        <label class="text-input-label" for="nama_lengkap">Nama Lengkap</label>
        <input class="text-input" data-val="true" data-val-required="The Email: field is required." id="nama_lengkap" name="nama_lengkap" type="text" value="">
        <span class="field-validation-valid" data-valmsg-for="nama_lengkap" id="nama_kosong" data-valmsg-replace="false"></span>
    </div>
	<div class="form-group">
        <label class="text-input-label" for="email">E-mail</label>
        <input class="text-input" data-val="true" data-val-required="The Email: field is required." id="email" name="email" type="text" value="">
        <span class="field-validation-valid" data-valmsg-for="email" id="email_kosong" data-valmsg-replace="false"></span>
    </div>
	<div class="form-group">
        <label class="text-input-label" for="telp">Telepon</label>
        <input class="text-input" data-val="true" data-val-required="The Email: field is required." id="telp" name="telp" type="text" value="">
        <span class="field-validation-valid" data-valmsg-for="telp" id="telp_kosong" data-valmsg-replace="false"></span>
    </div>
	
    <div class="form-group">
        <div class="password-wrapper password-is-shown"><label class="text-input-label" for="password1">Password</label>
		<input class="text-input" data-val="true" data-val-required="The Password: field is required." id="password1" name="password1" type="password">
		<button type="button" class="password-visibility-toggle" title="Show password"></button></div>
        <span class="field-validation-valid" data-valmsg-for="password1" id="password_kosong" data-valmsg-replace="false"></span>
    </div>
	<div class="form-group">
        <p class="confirmation-message" id="register-form-page-confirmation" style="display: none;"></p>
	</div>
			
    <div class="form-group">
		<input type="submit" value="Register" id="register-form-page-submit">
    </div>
    </form>
    </div>
	 <script type="text/javascript">
		$('#register-form-page-submit').submit(function (event) {
			event.preventDefault();
			var nama = $('#nama_lengkap').val();
			var email = $('#email').val();
			var telp = $('#telp').val();
			var pass = $('#password1').val();
			if(nama == '' || email == '' || telp == '' || pass == '') {
				$('#error').html('Isi semua data Anda.');
			}else{
            $.ajax({
                url: '<?php echo site_url('register/RegMobile'); ?>',
                type: 'POST',
                data: $('#register-form-page').serialize(),
				success: function (data) {
					if(data == 'OK') {
						$('#register-form-page-confirmation').html('Proses pendaftaran berhasil. Silahkan login menggunakan akun Anda.');
						window.location.reload('login_apps');
						
					}else{
						$('#error').hide();
						$('#register-form-confirmation').hide(); 
						$('#register-form-error').show(); 
						$('#register-form-error').html('Data email "<b>'+email+'</b>" sudah terdaftar.');
						
						
					}
				}
				
			});
			}
		});
    </script>
</div>
</div>

