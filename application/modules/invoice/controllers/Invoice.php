<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Invoice extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
//			$this->load->model('m_account');
			$this->load->model('m_invoice');
			if($this->session->userdata('loc_id') == ''){
			redirect('home');
		}
        }
		
     public function print_invoice() {
		$idses 	= $this->session->userdata('loc_id');
		$idinv 	= $this->uri->segment(3);
		$data['query_cart'] = $this->m_general->addCart();
		$data['query_add'] = $this->m_invoice->getAddressInvoice();
		$data['order'] = $this->db->query('select *, date_format(tanggal, "%d-%m-%Y") as tgl from tbl_order where id="'.$idinv.'" and user="'.$idses.'" and status !="Pending" order by id desc');
		$data['payment'] = $this->db->query('select *, date_format(tanggal, "%d-%m-%Y") as tgl from tbl_payments where id_order="'.$idinv.'" and user="'.$idses.'"');
		$data['user'] = $this->db->query('select * from tbl_user where id="'.$idses.'"');
		$sql = $this->db->query("select * from tbl_cart where user='$idses' and id_order='$idinv'");
			foreach($sql->result() as $cart) {
				$id_cart = $cart->id;
				$produk_cart = $cart->produk;
				$size_cart = $cart->size;
			}
		$query = $this->db->query("select * from tbl_products where id='$produk_cart'");
			foreach($query->result() as $pro) {
				$id = $pro->id;
				$nama = $pro->nama;
				$warna =$pro->warna;
			}
		$data['cart_item'] = $query;
		$data['content'] = $this->load->view('print_invoice',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function check_invoice() {
		$id 	= $this->session->userdata('loc_id');
		$idinv 	= $this->uri->segment(3);
		$data['query_cart'] = $this->m_general->addCart();
		$data['query_add'] = $this->m_invoice->getAddressInvoice();
		$data['province'] = $this->m_invoice->getFirstChoiceProvince();
		$data['city'] = $this->m_invoice->getCityChoice();
		$data['order'] = $this->db->query('select *, date_format(tanggal, "%d-%m-%Y") as tgl from tbl_order where id="'.$idinv.'" and user="'.$id.'" and status !="Pending" order by id desc');
		$data['payment'] = $this->db->query('select *, date_format(tanggal, "%d-%m-%Y") as tgl from tbl_payments where id_order="'.$idinv.'" and user="'.$id.'"');
		$data['user'] = $this->db->query('select * from tbl_user where id="'.$id.'"');
		$sql = $this->db->query("select * from tbl_cart where user='$id' and id_order='$idinv'");
			foreach($sql->result() as $cart) {
				$id_cart = $cart->id;
				$produk_cart = $cart->produk;
				$size_cart = $cart->size;
			}
		$query = $this->db->query("select * from tbl_products where id='$produk_cart'");
			foreach($query->result() as $pro) {
				$id = $pro->id;
				$nama = $pro->nama;
				$warna =$pro->warna;
			}
		$data['cart_item'] = $query;
		$data['content'] = $this->load->view('invoice_view',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	
	
}

?>