<body onload="javascript:window.print()">
<div class="page" tabindex="-1">
<div class="heading">
	<?php
		foreach($order->result() as $rsl) {
			$id		= $rsl->id;
			$tgl 	= $rsl->tgl;
			$stt 	= $rsl->status;
			$pay	= $rsl->st_payment;
			$resi	= $rsl->resi_kiriman;
			$kurir	= $rsl->kurir;
		}
		if(!isset($id)) {
			echo '';
		}else{
	?>
		<div class="global-header-logo"><img src="<?php echo base_url(); ?>assets/images/ampmlogo.png" alt="AMPM" title="AMPM" width="120px"></div>
		<p>We Are Equal</p>
		<hr>
        <h3>INVOICE : #<?php echo $id; ?></h3>
		<p><b><?php echo $tgl; ?></b></p>
		<p><b>- <?php echo $stt; ?> - </b></p>
	<?php } ?>
</div>

<div class="my-account-container">

    <ul class="order-history">
	<?php
		foreach($query_add->result() as $rsl) {
			$nama	= $rsl->nama;
			$alam	= $rsl->nama_alamat;
			$city	= $rsl->name_city;
			$prov	= $rsl->name_province;
			$neg	= $rsl->negara;
			$kode	= $rsl->kodepos;
			$tlp	= $rsl->telepon;
		}
	?>
	<p>
		<table border="1" height="100%" width="100%">
			<tr>
				<td><b>NAMA</b></td>
				<td><b>:</b></td>
				<td><b><?php echo $nama; ?></b></td>
				<td rowspan="3" align="center"><div style="text-align:center;vertical-align:middle;background:s#F0B856;" >
		<strong>STATUS</strong><br><?php if(!isset($pay)) { echo ''; }else{ echo $pay; }?><br>
		<strong>AIR WAY BILL NUMBER</strong><br>
		<?php if(!isset($resi)) { echo ''; }else{ echo $resi; }?><br>
		<strong>COURIER</strong><br>
		<?php if(!isset($kurir)) { echo ''; }else{ echo $kurir; } ?>
	</div></td>
			</tr>
			<tr>
				<td valign="top"><b>ALAMAT</b></td>
				<td valign="top"><b>:</b></td>
				<td valign="top"><b><?php echo $alam; ?><br/><?php echo $city.' - '.$prov; ?><br/><?php echo $neg.' '.$kode; ?></b></td>
			</tr>
				<td><b>TELEPON</b></td>
				<td><b>:</b></td>
				<td><b><?php echo $tlp; ?></b></td>
			</tr>
		</table>
		</p>
    </ul>
	
    
    <ul class="order-history">
	<?php
		$no = 1;
		$subtot = 0;
		foreach($cart_item->result() as $row) {
			$namax	  = $row->nama;
			$warna		= $row->warna;
			$hrg	  = $row->harga;
		}
		foreach($order->result() as $rsl) {
			$tot	= $rsl->jml_bayar;
			$subtot += $tot;
		}
	?>
	<p align="left">
		<table border="1" height="100%" width="100%">
			<tr>
				<td align="center"><b>NO</b></td>
				<td align="center"><b>ITEM(S)</b></td>
				<td align="center"><b>PRICE</b></td>
			</tr>
			<tr>
				<td align="center"><b><?php echo $no++; ?></b></td>
				<td><b><?php echo $namax.' - '.$warna; ?></b></td>
				<td align="right"><b>Rp. <?php echo number_format($tot,2,',','.').""; ?></b></td>
			</tr>
			<tr>
				<td colspan="3" align="right"><b>PAYMENT TOTAL : Rp. <?php echo number_format($subtot,2,',','.').""; ?></b></td>
			</tr>
		</table>
		</p>
    </ul>


	
    <ul class="order-history">
	<p>
		<table border="1" height="100%" width="100%">
			<tr>
				<td align="center"><b>TANGGAL</b></td>
				<td align="center"><b>DETAIL</b></td>
				<td align="center"><b>DEBIT</b></td>
				<td align="center"><b>CREDIT</b></td>
				<td align="center"><b>SALDO</b></td>
			</tr>
			<?php
		$no = 0;
		foreach($order->result() as $rsl) {
			$tgl	= $rsl->tgl;
			$bank	= $rsl->bank_tujuan;
			$byr	= $rsl->jml_bayar;
			}
		?>
			<tr>
				<td><b><?php echo $tgl; ?></b></td>
				<td><b>PLEASE TRANSFER TO : <?php echo $bank; ?></b></td>
				<td><b>0,00</b></td>
				<td><b><?php echo number_format($byr,2,',','.').""; ?></b></td>
				<td><b><?php echo number_format($byr,2,',','.').""; ?></b></td>
			</tr>
			<?php
				$no = 0;
				$saldo_akhir = 0;
				foreach($payment->result() as $rsl) {
					$tgl	= $rsl->tgl;
					$from	= $rsl->transfer_from;
					$to		= $rsl->transfer_to;
					$debit	= $rsl->debit;
					$credit = $rsl->credit;
					$saldo_payment = $byr+$saldo_akhir+$credit-$debit;
					$saldo_akhir = $saldo_payment;
			}
			?>
			<tr>
				<td><b><?php echo $tgl; ?></b></td>
				<td><b><?php echo $from.' | '.$to; ?></b></td>
				<td><b><?php echo "".number_format($debit,2,',','.').""; ?></b></td>
				<td><b><?php echo "".number_format($credit,2,',','.').""; ?></b></td>
				<td><b><?php echo "".number_format($saldo_payment,2,',','.').""; ?></b></td>
			</tr>
		</table>
		</p>
    </ul>

<br/>
<hr>
	<table border="0" width="100%">
			<tr>
				<td align="right" colspan="2"><b>Bandung, <?php echo $tgl; ?></b>
				<br/><br/><img src="<?php echo base_url(); ?>assets/images/ampmlogo.png" width="90px">
				<br/><br/><u>AMPM Clothing</u>
				</td>
			</tr>
		</table>
    
</div>
</body>

<script type="text/javascript">
    function updateAccountTabs() {
        $('#account-tabs-account-info, #account-content-account-info').toggleClass('is-active', (window.location.hash == '#account-info' || window.location.hash == ''));
        $('#account-tabs-addresses, #account-content-addresses').toggleClass('is-active', (window.location.hash == '#addresses'));
        $('#account-tabs-orders, #account-content-orders').toggleClass('is-active', (window.location.hash == '#orders'));
        $('#account-tabs-returnorders, #account-content-returnorders').toggleClass('is-active', (window.location.hash == '#returnorders'));
    }

    updateAccountTabs();

    $('#account-tabs-account-info').click(function () {
        window.location.hash = "#account-info";
        updateAccountTabs();
    });
    $('#account-tabs-addresses').click(function () {
        window.location.hash = "#addresses";
        updateAccountTabs();
    });
    $('#account-tabs-orders').click(function () {
        window.location.hash = "#orders";
        updateAccountTabs();
    });
    $('#account-tabs-returnorders').click(function () {
        window.location.hash = "#returnorders";
        updateAccountTabs();
    });

$("#idprovince").change(function(){				
                var selectValues = $("#idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#city').html(msg);
                }else{
                    var idprovince = {idprovince:$("#idprovince").val()};
                    $('#idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/select_city')?>",
                            data: idprovince,
                            success: function(msg){
                                $('#city').html(msg);
                            }
                    });
                }
        });
		
$("#edit_idprovince").change(function(){				
                var selectValues = $("#edit_idprovince").val();
                if (selectValues == 0){
                    var msg = "<select name=\"edit_idcity\" disabled><option value=\"Pilih Kota\">Pilih Propinsi Dulu</option></select>";
                    $('#edit_city').html(msg);
                }else{
                    var edit_idprovince = {edit_idprovince:$("#edit_idprovince").val()};
                    $('#edit_idcity').attr("disabled",true);
                    $.ajax({
                            type: "POST",
                            url : "<?php echo site_url('account/edit_select_city')?>",
                            data: edit_idprovince,
                            success: function(msg){
                                $('#edit_city').html(msg);
                            }
                    });
                }
        });
</script>


</div>
