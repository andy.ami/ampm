<div class="page">
<style type="text/css">
    .product-sidebar__product-original-price:before {
        text-decoration: line-through;
        /*position: relative;
        display: inline-block;
        margin-right: 10px;
        content: "";
        display: block;
        top: 50%;
        height: 1px;
        margin-top: -1px;
        width: 100%;
        background: #696969;
        text-decoration: line-through;*/
}
</style>
<br/><br/><br/><br/>
<?php 
		foreach($query->result() as $rsl) {
			$id	  = $rsl->id;
			$nama = $rsl->nama;
			$wrn  = $rsl->warna;
			$hrg  = $rsl->harga;
		}
	?>
<div class="social-media-image" data-img-src="<?php echo base_url(); ?>assets/images/footgrey1.jpg"></div>
<header class="product-header">
    <h1 class="product-header__heading"><?php echo $nama; ?></h1>
    <p class="product-header__description"><?php echo $wrn; ?></p>
	<input type="hidden" name="idp" id="idp" value="<?php echo $id; ?>">
	<input type="hidden" name="harga" id="harga" value="<?php echo $hrg; ?>">
</header>

<section class="product-section product-section--main">
<div class="product-image-carousel">
    <ul class="product-images-container">
	
	<li class='product-image-large is-active'>
	<?php 
	foreach($query_baju->result() as $rsl) {
			$a = 	$rsl->foto;
	?>
		<img src="" data-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $a; ?>?Width=1200&amp;transform=fit" data-small-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $a; ?>?Width=500&amp;transform=fit">
			
		</li>
		<li class='product-image-large  '>
			<img src="" data-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $a; ?>?Width=1200&amp;transform=fit" data-small-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $a; ?>?Width=500&amp;transform=fit">
		</li>
		<?php } ?>
	</ul>
	
		<ul class="product-thumbnails-container">
			<?php 
				foreach($query_thum->result() as $rsl) {
				//	$a = 	$rsl->foto;
				?>
				 <li class="product-thumbnail" tabindex="0">
					<a href="<?php echo site_url('mamboo/pilih/'.$rsl->warna.'/'.$rsl->id); ?>">
                    <img data-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=1200&amp;transform=fit" data-small-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=500&amp;transform=fit"></a>
                </li>
				
				<!--<li class="product-thumbnail" tabindex="0">
				

				 <a href="<?php echo site_url('mamboo/pilih/'.$rsl->warna.'/'.$rsl->id); ?>">
                   <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>" data-small-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>"></a>
              
				</li>-->
				  <?php } ?>
			</ul>
			
	</div>
    <div class="product-sidebar" ng-controller="BuyController" id="b464d5ad-972b-4707-900b-3a45a8006085">
        	<div class="product-sidebar__overlay"></div>
			<div class="product-sidebar__column" ng-init="init('en', 2 == 1 ? '11101-031-M': null,'Classic Galosh',0,'Olive','M', '11101-031', 'images/11101_031_1_gray1200.jpg', 'Classic Galosh', 'Please select size', 'USD', '95')">
                <p class="product-sidebar__product-price">IDR <?php echo number_format($hrg); ?></p>
            <div class="product-sidebar__description"><p>The foundation of SWIMS, this revitalized galosh was transformed from a purely practical utility product to a classy lifestyle item. It is now a dearly valued part of the wardrobe for style-conscious people around the world, looking good while protecting their favorite pair of shoes.</p></div>
                <a href="#other-colors" class="product-sidebar__other-colors-link">Other colours</a>
                            <div class="product-sidebar__size-select select-with-label">
                    <select id="sizes" name="sizes" class="" ng-model="selectedSize" ng-change="getStock(selectedSize)" >
                        <option value="" disabled="disabled" selected="selected" class="placeholder">&nbsp;</option>
                            <option value="M" size="M">M</option>
                            <option value="L" size="L">L</option>
                            <option value="XL" size="XL">XL</option>
                    </select>
                    <label for="sizes">Size</label>
                </div>
               
			   <div class="product-sidebar__quantity-select select-with-label">
                    <select id="quantity" name="quantity" class="product-quantity-select">
                        <option value="" disabled="disabled" selected="selected" class="placeholder">&nbsp;</option>
						<option value="1">1</option>
                        <option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
                        <option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
                        <option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
                    </select>
                    <label for="quantity">Quantity</label>
                </div>
                            <p class="product-sidebar__size-guide-link">
                 <!--   <a href="../../../../../../articles/sizeguidemensfootwear/index.html">Size guide</a>-->
                </p>
                            <button class="button--add-to-cart" onClick="addToCart()">
                    <span class="button-text">Add to the bag</span>
                    <span class="added-confirmation-text">Added to the bag</span>
                </button>
				 <ul class="social-links">
                    <li>
                        <a href="" class="social-links__item facebook"></a>
                        <span class="social-links__tooltip">Share this</span>
                    </li>
                    <li>
                        <a href="" class="social-links__item pinterest"></a>
                        <span class="social-links__tooltip">Share this</span>
                    </li>
                </ul>

        </div>

        <div class="product-sidebar__column">
            
        </div>
    </div>
</section>

    <section class="product-section" id="other-colors">
        <h2 class="product-carousel-heading">Other colours</h2>
        <div class="product-carousel">
		<?php
		foreach($query_other->result() as $rsl) {
		?>
		
		    <a class="product " href="<?php echo site_url('mamboo/detail_produk/'.$rsl->warna); ?>">
                <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=500&amp;transform=fit" alt="Classic Galosh" 
				src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=500&amp;transform=fit">
                <div class="product__hover-image">
                    <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>" alt="Classic Galosh" 
					src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>">
                </div>
				 <div class="product__description">
                <p class="product__name ng-binding"><?php echo $rsl->nama; ?></p>
                <p class="product__color ng-binding"><?php echo $rsl->warna; ?></p>
                
                <p class="product__price ng-binding" ng-hide="product.DiscountedPrice">
                    <?php echo number_format($rsl->harga); ?>
                </p>
            </div>
                </a>
		<?php } ?>

        </div>
    </section>
</div>

<script>
function addToCart() {
	    var idp = $("#idp").val();
		var sizes = $("#sizes").val();
		var hrg = $("#harga").val();
		var qty = $("#quantity").val();
        $.ajax({
            url: '<?php echo site_url('mamboo/cart'); ?>',
            type: "POST",
            data: {
                idp: idp, sizes: sizes, harga: hrg, quantity: qty 
            },
            success: function(data) {
				if(data == 'OK') {
				window.location.reload();
				}else{
					alert('Pilih size dan quantity.');
				}
            }
        });
    }
</script>