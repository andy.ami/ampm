
<div class="page">

</br></br></br></br>
<div class="heading">
    <h1>Bamboo</h1>
    <div class="heading__intro"><p>Don't ruin your best leather shoes. Seduce the rain in colorful galoshes!&nbsp;</p></div>
</div>

<div id="productAndSearchController">
    <div class="filters">
	
        <select class="select" id="warna" name="warna">
			<option value="" class="placeholder">Color</option>
		<?php
		foreach($query->result() as $row) {
		?>
			<option value="<?php echo $row->warna; ?>"><?php echo $row->warna; ?></option>
		<?php } ?>
        </select>
		<!--
        <select class="select " title="{{facetStyle.DisplayName}}" onchange="updateFacet(this)" ng-show="facetStyle.TermsList.length > 0" data-facettype="{{facetStyle.Name}}">
			<option value="" class="placeholder">Style</option>
            <option value="" disabled="disabled" class="placeholder">{{facetStyle.DisplayName}}</option>
            <option value="{{facet.Term}}" ng-selected="facet.Selected" ng-repeat="facet in facetStyle.TermsList">{{facet.TranslatedText}}</option>
        </select>-->
    </div>

</div>
<br/><br/><br/>
<div id="result">
        <?php echo $this->load->view('result'); ?><!-- end box_upk_des -->
</div>
</div>

<script type='text/javascript' language='javascript'>
$('#warna').on('change',function(){
	var warna = document.getElementById('warna').value;
	$.ajax({
    type:'POST',
    url:'<?php echo site_url('bamboo/find');?>',
	data:{warna:warna},
    success:function(html){
    $('#result').html(html);
    }
    });
});
</script>