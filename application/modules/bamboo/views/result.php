<div class="product-grid">
	<?php
		foreach($query->result() as $rsl) {
			//$ft = $rsl->fo
	?>
    	<a class="product" href="<?php echo site_url('bamboo/detail_produk/'.$rsl->warna); ?>">
            <div class="product__image">
                <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>" alt="Classic Galosh"/>
                <div class="product__hover-image">
                    <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>" alt="Classic Galosh"/> 
                </div>
            </div>
            <div class="product__description">
                <p class="product__name ng-binding"><?php echo $rsl->nama; ?></p>
                <p class="product__color ng-binding"><?php echo $rsl->warna; ?></p>
                
                <p class="product__price ng-binding" ng-hide="product.DiscountedPrice">
                    <?php echo number_format($rsl->harga); ?>
					
                </p>
				<br/><br/>
            </div>
        </a>
		<?php } ?>
</div>