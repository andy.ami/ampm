<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bamboo extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
        }
		
	public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['query'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and bahan ="Bamboo" group by warna asc');
		$data['content'] = $this->load->view('bamboo_view',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function detail_produk() {
		$col = $this->uri->segment(3);
		$pilih = $this->input->post('id');
		$data['query_cart']    = $this->m_general->addCart();
		$data['query'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and warna="'.$col.'" order by id desc');
		$data['query_baju'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and warna="'.$col.'" order by id asc');
		$data['query_thum'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and warna="'.$col.'" order by warna asc');
		$data['query_other'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and bahan="Bamboo" and warna !="'.$col.'" group by warna asc');
		$data['content'] = $this->load->view('detail_produk',$data,TRUE);
		$this->load->view('home_index',$data);

	}
	
	public function pilih() {
		
		$a = $this->uri->segment(3);
		$b = $this->uri->segment(4);
		$data['query_cart'] = $this->m_general->addCart();
		$data['query'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and warna="'.$a.'" order by id desc');
		$data['query_baju'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and id="'.$b.'" order by id asc');
		$data['query_thum'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and warna="'.$a.'"');
		$data['query_other'] = $this->db->query('select * from tbl_products where kategori ="tshirt" and bahan="Bamboo" and warna !="'.$a.'" group by warna asc');
		$data['content'] = $this->load->view('detail_produk',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function cart() {
		$id     	= $this->session->userdata('loc_id');
		$ip 		= $_SERVER['REMOTE_ADDR'];
		$idp		= $this->input->post('idp');
		$sizes		= $this->input->post('sizes');
		$jumlah	 	= $this->input->post('quantity');
		$hrg 		= $this->input->post('harga');
		$tanggal 	= date('Y-m-d');
		if($id != '') {
			$a = $this->session->userdata('loc_id');
		}else{
			$a = 0;
		}
		if ($sizes == 0 && $jumlah == 0) {
			echo 'Masukkan data dengan benar.';
		}else{
			echo 'OK';
		$data = array(
			'ip'			=> $ip,
			'produk'		=> $idp,
			'user'			=> $a,
			'size'			=> $sizes,
			'jumlah'		=> $jumlah,
			'harga'			=> $hrg,
			'tanggal'		=> $tanggal
		);
			$this->db->insert('tbl_cart',$data);
		}
	}
    
	public function find() {
		$warna = $this->input->post('warna');
		$data['query'] = $this->db->query('select * from tbl_products where warna ="'.$warna.'" group by warna asc');
		$this->load->view('result',$data);
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
        
    }
    

?>