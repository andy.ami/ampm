<div class="page">
	
<script type="text/javascript">
/*
    function trackGuest() {
        if (dataLayer) {
            dataLayer.push({
                "event": "checkoutOption",
                "ecommerce": {
                    "checkout_option": {
                        "actionField": { "step": 2, "option": "continueAsGuest" }
                    }
                }
            });
        }
    }

    function trackLogin() {
        if (dataLayer) {
            dataLayer.push({
                "event": "checkoutOption",
                "ecommerce": {
                    "checkout_option": {
                        "actionField": { "step": 2, "option": "login" }
                    }
                }
            });
        }
    }*/
</script>

<br/><br/><br/><br/>
<div class="heading">
    <h1>Checkout</h1>
</div>

<ul class="checkout-progress">
    <li class="
        is-completed ">
            <a href="#<?php //echo site_url('checkout/cart'); ?>">
        <div class="checkout-progress__step-icon">1</div>
        <p>Your shopping bag</p>
    </a>

    </li>
    <li class="is-current
         ">
    <a href="<?php echo site_url('checkout/billing'); ?>">
        <div class="checkout-progress__step-icon">2</div>
        <p>Your billing / shipping address</p>
    </a>
    </li>
    <li class=" ">
    <div class="checkout-progress__step-icon">3</div>
    <p>Payment</p>
    </li>
</ul>

<form action="<?php echo site_url('checkout/payment'); ?>" class="checkout-container ng-pristine ng-valid" method="post" novalidate="novalidate">    
<div class="checkout-column">
<?php 
	$a = $this->session->userdata('loc_id');
    if($a != TRUE) {
	?>
	

	<div class="form form--box" id="checkout-login">
            <h2>Silahkan Log in</h2>
            <p>Log in untuk checkout</p>
            <label class="text-input-label" for="login_email">E-mail</label>
            <input type="text" class="text-input" id="login_email" name="login_email">
            <label class="text-input-label" for="login_password">Password</label>
            <input type="password" class="text-input" id="login_password" name="login_password">
            <!--<a href="#" class="forgot-password-link">Forgot password?</a>-->
            <input class="checkbox" type="checkbox" id="checkout-login-remember-me">
            <label for="checkout-login-remember-me">Remember me</label>
			 <p class="error-message" id="login-form-error"></p>
            <!--<button type="submit" id="checkout-login-btn">Log in and continue</button>-->
			<input type="button" class="form-submit" value="Log in and continue" id="check-login-btn">
        </div>
		
		<script type="text/javascript">
            $('#check-login-btn').click(function () {
				var email = document.getElementById('login_email').value;
				var password = document.getElementById('login_password').value;
				if(email == '' || password == '') {
					$('#login-form-error').html("Email atau password salah.");
				}else{
                $.ajax({
					url: '<?php echo site_url('login_apps/login_proses'); ?>',
					type: 'POST',
					data: {'login_email':email, 'login_password':password},
                    success: function (data) {
						if(data != '') {
							window.location.reload('checkout/billing');
                        } else {
							$('#login-form-error').hide();
						    window.location.href('checkout/billing');
                        }
                    }
                });
				}
            });
	//	});
            $('#login-form-error').hide();
</script>
	<?php
	}else{
	?>
	
<?php
		foreach($query_add->result() as $row) {
				$nama = $row->nama;
				$alamat = $row->nama_alamat;
				$kode = $row->kodepos;
				$city = $row->name_city;
				$pro = $row->name_province;
				$telp = $row->telepon;
			}
			?>
		 
        <div id="addressdata" style="display: none">
                <p class="checkout-address" id="de56c759-21c0-4b93-b016-803df21da008">
                    <b><?php echo $nama; ?></b><br>
                    <?php echo $alamat; ?><br>
                    <?php echo $kode.' '.$city; ?><br>
                    <?php echo $pro; ?><br>
                    <?php echo $telp; ?>
                </p>
                
        </div>

        <div class="form ng-scope" id="billing-address-form" style="">
            <h2>Alamat Billing</h2>
			
            <input id="Email" name="Email" type="hidden" value="andylife6@gmail.com">
			
            <div class="form__content" tabindex="-1" style="display: block;">
                <select class="select ng-pristine ng-untouched ng-valid" id="id" name="id" style="display: none;">
				<!--<option value="">- Pilih Alamat Tujuan -</option>-->
				<?php
				
			foreach($query_add->result() as $row) {
				$nama = $row->nama;
				$alamat = $row->nama_alamat;
				$kode = $row->kodepos;
				$city = $row->name_city;
				$kec  = $row->name_kecamatan;
				$pro = $row->name_province;
				$telp = $row->telepon;
				
				
			?>
				<option selected="selected" value="<?php echo $row->id_alamat; ?>"><?php echo $row->nama.' ('.($row->name_city).')'; ?></option>
				<?php } ?>
			</select>
			<?php
			if(isset($nama) && ($alamat)) {
					
			?>
			<div id="hide">
			<p class="checkout-address">
                     <b><?php echo $nama; ?></b><br>
                    <?php echo $alamat; ?><br>
                    <?php echo $city.' - '.$kec; ?><br>
                    <?php echo $pro.' '.$kode; ?><br>
                    <?php echo $telp; ?>
					
                </p>
			</div>
			<?php }else{
				echo '';
			}
			?>
			<div id="show">
			</div>
			<a href="<?php echo site_url('account'.'#addresses'); ?>"><button type="button" class="checkout-edit-address">Add Address</button></a>
                <!--<button type="button" class="checkout-edit-address">Edit</button>-->
                
            </div>
        </div>
	<?php } ?>
	</div>
	
	

<div id="checkout-order" class="checkout-column ng-scope" ng-controller="HeaderCartController" ng-init="GetOrderItems()">
    <div class="checkout-receipt">
        <h2>Pesanan Anda</h2>
        <ul>
	<?php
	$subtot = 0;
foreach($query_cart->result() as $row) {
	$idprod   = $row->idcart;
	$nama = $row->nama;
	$wrn  = $row->warna;
	$hrg  = $row->harga;
	$foto = $row->foto;
	$size = $row->size;
	$jml  = $row->jumlah;
	$tot  = $hrg * $jml;
	$subtot += $tot;

	if(!isset($jml)) {
	echo '';
	}else{
?>
        <li ng-repeat="p in products">
        <button type="button" class="remove-from-cart" onclick="removeFromCart(<?php echo $idprod; ?>)" title="Remove product from shopping cart"></button>
                <h3><?php echo $nama; ?></h3>
                <p><?php echo $wrn; ?></p>
                    <p>Size : <?php echo $size; ?><br/> Qty : <span class="checkout-receipt__edit-qty"><?php echo $jml; ?></span></p>
					<!--<p>Size : <?php echo $size; ?>, Qty : <input type="text" class="checkout-receipt__edit-qty" ng-model="p.Quantity" ng-change="updateQuantity(p, '{{p.Quantity}}')" value="<?php //echo $jml; ?>"></p>-->
                <div class="checkout-receipt__price"> <span class="currency"><?php echo number_format($tot); ?></span></div>
            </li>
  <?php }} ?>
		<li>
             <h3 class="ng-binding">Standard Shipping</h3>
             <p class="ng-binding">2 - 4 business days</p>
             <div class="checkout-receipt__price ng-binding">0 </div>
        </li>
	<li class="checkout-receipt__total">
                <h3>Total</h3>
				<input type="hidden" name="harga_total" value="<?php echo $subtot; ?>">
                <div class="checkout-receipt__price"> <span class="currency" ><?php echo number_format($subtot); ?></span></div>
            </li>
		</ul>
    </div>
</div>

    <div class="terms-and-conditions">
		<?php 
			foreach($query_add->result() as $row) {
				$iduser = $row->user;
			}
			$a = $this->session->userdata('loc_login');
			//$b = $this->session->userdata('loc_id');
			if(!isset($iduser) || ($a != true)) { ?>
			
			<?php 
			}else{
				?>
						<input class="checkbox" id="terms-and-conditions" name="AcceptedTerms" type="checkbox" value="true">
						<input name="AcceptedTerms" type="hidden" value="false">
                        <label for="terms-and-conditions">I accept the <a href="#">terms and conditions</a></label>
			<?php } ?>
                    </div>
                    <div class="checkout-nav">
                        <a href="#<?php //echo site_url('checkout/cart'); ?>" class="checkout-nav__prev">Previous step</a>
                        <!-- ignorer onclicken på submit -->
                        <input type="submit" class="checkout-nav__next" value="Next - Payment" disabled="">
                        <!-- lenke uten submit: <a href="lenke" class="checkout-nav__next">lenke-tekst</a> -->
                    </div>
</form>
</div>


<script>
$('#id').on('change',function(){
	var id = document.getElementById('id').value;
	$.ajax({
    type:'POST',
    url:'<?php echo site_url('checkout/pilih_address');?>',
	data:{id:id},
    success:function(html){
	$('#hide').hide();
    $('#show').html(html);
    }
    });
});

function removeFromCart(idprod) {
	if (confirm("Anda yakin akan menghapus transaksi ini?")) {
        $.ajax({
            url: '<?php echo site_url('con_global/delete'); ?>',
            type: "POST",
            data: 
			{
                idprod: idprod
            },
            success: function(data) {
				window.location.reload();
            }
        });
		 $(this).parents(".record").animate("fast").animate({
                        opacity : "hide"
                    }, "slow");
		}
		return false;
    }
</script>


