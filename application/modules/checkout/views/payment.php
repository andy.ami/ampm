<div class="page" tabindex="-1">

<script type="text/javascript">
    function submitPaymentForm() {
        $('#formPayment').submit();
    }
    $(document).ready(function () {
        $("#language-selector-link").click(function () {
            $("#language-selector").hide();
        });
    });
</script>

<div class="heading">
    <h1>Checkout</h1>
</div>


<ul class="checkout-progress">
    <li class="
        is-completed ">
            <a href="<?php echo site_url('checkout/cart'); ?>">
        <div class="checkout-progress__step-icon">1</div>
        <p>Your shopping bag</p>
    </a>

    </li>
    <li class="
        is-completed ">
    <a href="<?php echo site_url('checkout/billing'); ?>">
        <div class="checkout-progress__step-icon">2</div>
        <p>Your billing / shipping address</p>
    </a>
    </li>
    <li class="is-current ">
    <a href="<?php echo site_url('checkout/payment'); ?>">
        <div class="checkout-progress__step-icon">3</div>
        <p>Payment</p>
    </a>
    </li>
</ul>

<div class="checkout-container">
    <div class="checkout-column">
        <div>
            <h2>Click to choose payment method:</h2>
            <ul ng-controller="PaymentController" class="payment__options ng-scope">
				<?php 
				//foreach($bank->result() as $row) {
					
			//	}
				?>
                    <li>
                        <input type="radio" name="atom-radio" id="bank1" value="BCA" name="1" class="ng-pristine ng-untouched ng-valid">
                        <label for="bank1">
                            <img src="<?php echo base_url(); ?>assets/images/payment/visa-logo.png" alt="Visa" title="Visa">
                            <span>Pay with Visa</span>
                        </label>
                    </li>
                    <li>
                        <input type="radio" name="atom-radio" id="bank2" value="BNI" name="2" class="ng-pristine ng-untouched ng-valid">
                        <label for="bank2">
                            <img src="<?php echo base_url(); ?>assets/images/payment/mastercard-logo.png" alt="Mastercard" title="Mastercard">
                            <span>Pay with Mastercard</span>
                        </label>
                    </li>
                    <li>
                        <input type="radio" name="atom-radio" id="bank3" value="776165ec-0023-4657-bcab-f77c7ea44fa6" class="ng-pristine ng-untouched ng-valid">
                        <label for="bank3">
                            <img src="<?php echo base_url(); ?>assets/images/payment/paypal-logo.png" alt="Paypal" title="Paypal">
                            <span>Pay with Paypal</span>
                        </label>
                    </li>
            </ul>
        </div>
			
			<div id="bayar">
            </div>
    </div>
    
<div id="checkout-order" class="checkout-column ng-scope" ng-controller="HeaderCartController" ng-init="GetOrderItems()">
    <div class="checkout-receipt">
        <h2>Your order</h2>
		<?php
foreach($query_cart->result() as $row) {
	$nama = $row->nama;
	$wrn  = $row->warna;
	$hrg  = $row->harga;
	$foto = $row->foto;
	$size = $row->size;
	$jml  = $row->jumlah;
}
?>
        <ul>
            <!-- ngRepeat: p in products --><li ng-repeat="p in products" class="ng-scope">
                <h3 class="ng-binding"><?php echo $nama; ?></h3>
                <p class="ng-binding"><?php echo $wrn; ?></p>
                    <p class="ng-binding">Size : <?php echo $size; ?> <br/> Qty : <?php echo $jml; ?></p>
                <div class="checkout-receipt__price ng-binding"><?php echo number_format($hrg); ?></div>
            </li><!-- end ngRepeat: p in products -->
            <!-- ngRepeat: discount in discountCodes -->
         
            <li class="checkout-receipt__total">
                <h3>Total</h3>
                <div class="checkout-receipt__price ng-binding"><?php echo number_format($hrg); ?> </div>
            </li>
        </ul>
    </div>
</div>
    <div class="checkout-nav">
        <a href="https://www.swims.com/checkout/address/" class="checkout-nav__prev">Previous step</a>
        <!-- ignorer onclicken på submit -->
        <!-- lenke uten submit: <a href="lenke" class="checkout-nav__next">lenke-tekst</a> -->
    </div>
</div>
</div>
<script>
$('#bank1').on('change',function(){
	var bank1 = document.getElementById('bank1').value;
	$.ajax({
    type:'POST',
    url:'<?php echo site_url('checkout/bank_payment');?>',
	data:{'id':bank1},
    success:function(html){
    $('#bayar').html(html);
    }
    });
});
</script>

