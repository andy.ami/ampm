<div class="page">
<style type="text/css">
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<br/><br/><br/>
<div class="heading">
    <h1>Checkout</h1>
</div>

<ul class="checkout-progress">
    <li class="is-current
         ">
            <a href="<?php echo site_url('checkout/addCart'); ?>">
        <div class="checkout-progress__step-icon">1</div>
        <p>Your shopping bag</p>
    </a>

    </li>
    <li class="
         ">
    <div class="checkout-progress__step-icon">2</div>
    <p>Your billing / shipping address</p>
    </li>
    <li class=" ">
    <div class="checkout-progress__step-icon">3</div>
    <p>Payment</p>
    </li>
</ul>

<div class="cart-swims-loader"></div>
<form class="shopping-cart--page">
    
<?php
$subtot = 0;
foreach($query_cart->result() as $row) {
	$idprod   = $row->idcart;
	$nama = $row->nama;
	$wrn  = $row->warna;
	$hrg  = $row->harga;
	$foto = $row->foto;
	$size = $row->size;
	$jml  = $row->jumlah;
	$tot  = $hrg * $jml;
	$subtot += $tot;
?>
<?php
	if(!isset($jml)) {
	echo '';
	}else{
?>
<ul class="shopping-cart__products">

    <li class="shopping-cart__product" ng-repeat="p in products">
        <button class="remove-from-cart" title="Remove from cart" onclick="removeFromCart(<?php echo $idprod;?>)"></button>
        <a class="product " ng-href="{{p.Url}}">
            <div class="product__image">
                <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $foto; ?>" alt="<?php echo $nama; ?>"/>
                <div class="product__hover-image" ng-show="p.SecondaryImageUrl">
                    <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $foto; ?>" alt="<?php echo $nama; ?>" />
                </div>
            </div>
            <div class="product__description">
                <p class="product__name"><?php echo $nama; ?></p>
                <p class="product__color"><?php echo $wrn; ?></p>
                <p class="product__price">
                    <?php echo number_format($hrg); ?>
                </p>

            </div>

        </a>

        <ul class="shopping-cart__product-details">
            <li class="shopping-cart__product-size">
                <div class="select-with-label">
                    <select id="sizes{{p.Code}}" class="" data-productcode="{{p.Code}}" ng-model="p.Code" ng-change="updateProduct('{{p.Code}}', p.Code)">
                        <option value="" disabled="disabled" class="placeholder">&nbsp;</option>
                        <option ng-repeat="variant in p.Variants"
                                
                                value="{{variant.Code}}"
                                ng-class="(variant.Stock==0) ? 'out-of-stock' : ''"
                                ng-disabled="variant.Stock == 0">
                            <?php echo $size; ?>
                        </option>
                    </select>
                    <label for="sizes{{p.Code}}">Size</label>
                </div>
            </li>
            <li class="shopping-cart__product-qty">
                <div class="select-with-label">
                    <select id="quantity{{p.Code}}" class="product-quantity-select" ng-model="p.Quantity" ng-options="stockItem as stockItem for stockItem in p.Stock track by stockItem" ng-change="updateQuantity('{{p.Code}}', '{{p.Quantity}}', p.Quantity)">
                        <option value="" disabled="disabled" class="placeholder">&nbsp;</option>
						<option value="1" class="placeholder"><?php echo $jml; ?></option>
                    </select>
                    <label for="quantity{{p.Code}}">Quantity</label>
                </div>
            </li>
            <li class="shopping-cart__product-price">
                <p><span class="currency"><?php echo number_format($tot); ?></span></p>
            </li>
        </ul>
    </li>
</ul>
<? }} ?>

  

    <ul class="shopping-cart__sum">
        <li>
            <input id="FreeDelivery" name="FreeDelivery" type="hidden" value="Castle.Proxies.TextBlockProxy" />
                
            Sub total <span class="amount"> <span class="currency"><?php echo number_format($subtot); ?></span></span>
        </li>
       
                
        <li class="final-total">
            Total
            <span class="amount"> <span class="currency"><?php echo number_format($subtot); ?></span></span>
        </li>

      
    </ul>
    <div class="checkout-nav">
        <a href="#" class="checkout-nav__prev">Continue shopping</a>
        <a href="<?php echo site_url('checkout/billing');?>" ng-click="gotoPayment($event)" class="checkout-nav__next">Next - Address</a>
    </div>
</form>
</div>
<script>
function removeFromCart(idprod) {
	if (confirm("Anda yakin akan menghapus transaksi ini?")) {
        $.ajax({
            url: '<?php echo site_url('con_global/delete'); ?>',
            type: "POST",
            data: 
			{
                idprod: idprod
            },
            success: function(data) {
				window.location.reload();
            }
        });
		 $(this).parents(".record").animate("fast").animate({
                        opacity : "hide"
                    }, "slow");
		}
		return false;
    }
</script>