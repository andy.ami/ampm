<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Checkout extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_checkout');
        }
		
    public function cart() {
		
		$ip 		= $_SERVER['REMOTE_ADDR'];
		$idp		= $this->input->post('idp');
		$sizes		= $this->input->post('sizes');
		$jumlah	 	= $this->input->post('quantity');
		$hrg 		= $this->input->post('harga');
		$tanggal 	= date('Y-m-d');
		
		$data = array(
			'ip'			=> $ip,
			'produk'		=> $idp,
			'size'			=> $sizes,
			'jumlah'		=> $jumlah,
			'harga'			=> $hrg,
			'tanggal'		=> $tanggal
		);
		if ($data != '0') {
			$this->db->insert('tbl_cart',$data);
		}
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('detail_produk',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function addCart() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('checkout',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function billing() {
		$idses = $this->session->userdata('loc_id');
		$id = $this->input->post('SelectedBillingAddressId');
		$data['pilih_alamat'] = $this->db->query('select a.*, b.*, c.* from tbl_alamat a JOIN tbl_province b ON b.idprovince=a.provinsi 
												JOIN tbl_city c ON c.idcity=a.kota where a.id="'.$id.'" and user="'.$idses.'"') ;
		$data['query_add'] = $this->m_general->getAddress();
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('cekbill',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function pilih_address() {
		$idses = $this->session->userdata('loc_id');
		$id = $this->input->post('id');
		if(isset($id)) {
		$data['pilih_alamat'] = $this->db->query('select a.*, b.*, c.*, d.* from tbl_alamat a JOIN tbl_province b ON b.idprovince=a.provinsi 
												JOIN tbl_city c ON c.idcity=a.kota 
												JOIN tbl_kecamatan d ON d.idkecamatan=a.kecamatan where a.id="'.$id.'" and user="'.$idses.'"') ;
		$this->load->view('result_alamat',$data);
		}
	}
	
	public function payment() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$log   = $this->session->userdata('loc_login');
		if($log = TRUE) {
		$idses = $this->session->userdata('loc_id');
		$sql = $this->db->query('select * from tbl_order order by id desc limit 1');
			foreach($sql->result() as $row) {
			$hasil_id = $row->id;
			if($hasil_id <=0){
				$next_id = 1000;
			}else{
				$next_id=$hasil_id+1;
			}
		}
		$tgl = date('Y-m-d');
		$alam = $this->input->post('id'); 
		$hrg  = $this->input->post('harga_total');
		$data = array('id'  => $next_id,
						'user'=> $idses,
						'ip' => $ip,
						'jml_bayar' => $hrg,
						'ongkir' => 0,
						'bank_transfer' => 0,
						'bank_tujuan' => 0,
						'alamat' => $alam,
						'kurir' => 0,
						'resi_kiriman' => 0,
						'tanggal' => $tgl,
						'agree' => 'agree',
						'status' => 'Pending',
						'st_payment' => 'UNPAID');
		$tabel = 'tbl_order';
		$this->m_checkout->insert_data($tabel, $data);
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('payment',$data,TRUE);
		$this->load->view('home_index',$data);
		redirect('order');
		}else{
			redirect('home');
		}
	}
	        	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>