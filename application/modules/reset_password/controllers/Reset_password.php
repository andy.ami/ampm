<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reset_password extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_reset');
        }
		
    public function index() {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('reset_appsview',$data,TRUE);
		$this->load->view('home_index',$data);
	}
	
	public function reset() {
		$data_user 		= $this->m_reset->get_by_email($this->input->post('reset'))->row();
		if(count($data_user) > 0) {
			echo 'OK';
			$reset		= $this->generate_kode();
			$data 		= array('password' => md5($reset), 'code' => $reset);
		$this->m_reset->edit($data_user->id, $data);
		
			$to   = $data_user->email;
		  	$from = "clothingampm@gmail.com";
			$this->load->library('email');
		   $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
		  	$this->email->subject('AMPM Official | Reset Password');
		  	$message  = "<html><body>";
		  	$message .= "<p>Password Anda berhasil di reset. <br />";
			$message .= "Password Baru  : <strong>".$reset."</strong></p><br/>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
		redirect('login_apps');	
		}else{
			redirect('reset_password');
			echo 'Nama email tidak terdaftar';
		}
	}
	
	# Mengenerate kode verifikasi (5 karakter)
	public function generate_kode()
	{
		$seed 			= "123456789123456789123456789";
		$seed_length	= 5;
		$str 			= ''; 

		for ($i=0; $i<=$seed_length; $i++) 
		{ 
			$str .= substr ($seed, rand() % 27, 1); 
		} 

		return $str; 
	}
        	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>