<div class="page" tabindex="-1">
<br/>
<div class="centered-content">
   
	<h3 class="login-drawer__heading">Forgot password?</h3>
    <p>Forgot your password? Don't worry! Fill in your e-mail address and we'll send you a new one.</p>
    <div id="loginRegistration">
    <style>
    .field-validation-valid {
        display: none;
    }
</style>

<form action="<?php echo site_url('reset_password/reset'); ?>" class="form ng-pristine ng-valid" id="resetForm" method="post">
<div class="form-group">
        <label class="text-input-label" for="reset">E-mail</label>
        <input class="text-input" data-val="true" data-val-required="The Email: field is required." id="reset" name="reset" type="text" value="">
        <span class="field-validation-valid" data-valmsg-for="reset" id="reset" data-valmsg-replace="false">Masukkan nama email.</span>
    </div>
	<div class="form-group">
        <input type="submit" class="form-submit" id="reset-password" value="Reset Password">
	</div>
</form>

    </div>
</div>
</div>

<script type="text/javascript">
		$(document).ready(function() {
			$('#password-recovery-form-error').hide();
			$('#password-recovery-form-confirmation').hide();
            $('#reset-password').click(function () {
				//event.preventDefault();
				var reset = document.getElementById('reset').value;
				if(reset == '') {
					window.location.replace('reset_password');
					$('#password-recovery-form-error').html('Masukkan nama email Anda.');
				}else{
				$.ajax({
                    url: '<?php echo site_url('reset_password/reset'); ?>',
					type: 'POST',
					data: $('#resetForm').serialize(),
                    success: function (data) {
						if(data == 'OK') {
							$('#password-recovery-form-error').hide();
							$('#password-recovery-form-confirmation').show();
							$('#password-recovery-form-confirmation').html('Password berhasil di reset.');
                        }else{
							$('#password-recovery-form-confirmation').hide();
							$('#password-recovery-form-error').show();
							$('#password-recovery-form-error').html('Nama email <b>'+reset_password+'</b> tidak terdaftar.');
						}
                    }
				});
                }
				});
		});
    </script>