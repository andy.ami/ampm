<div>
            <ul class="payment__content">
                  <li id="iframeContent" style="display: list-item;">
                    <div id="frameintro"><p>Anda memilih transfer ke Bank <b>BNI</b></p></div>
                    <!--<iframe id="framepayment" src="<?php //echo site_url('order/linkbank_payment'); ?>" height="400px" width="100%" frameborder="0"></iframe>-->
                </li>
				
				<script type="text/javascript" src="./jquery-1.7.1.min.js.download">
    </script>
    <script type="text/javascript" src="./highslide-full.js.download">
    </script>
    <script type="text/javascript" src="./highslide.config.js.download" charset="utf-8">
    </script>
    <link rel="stylesheet" type="text/css" href="./highslide.css">
    <script type="text/javascript" src="./payssl.info.sites.js.download">
    </script>
    <style type="text/css">
        .container {
          width: 100%;
          text-align: center;
          line-height: 12px;
          overflow: hidden;
        }
        .content {
          display: inline-block;
          vertical-align: middle;
          margin:0px 2px 0px 2px;
        }
        .content {
	        *display: inline;
        }
      </style>
  <style type="text/css">.highslide img {cursor: url(images/zoomin.cur), pointer !important;}.highslide-viewport-size {position: fixed; width: 100%; height: 100%; left: 0; top: 0}</style></head>
  <body onload="firstField();" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
    <form style="margin:0px;" name="SSLForm" id="SSLForm" method="POST" language="JavaScript" onclick="return SSLForm_onsubmit();" autocomplete="off" action="<?php echo site_url('order/transfer2'); ?>">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
        <tbody><tr>
          <td valign="center">
            <table border="1" cellpadding="0" cellspacing="0" align="center" bgcolor="" width="100%" height="360">
              <tbody>
			  <?php 
				$subtot = 0;
				foreach($query_cart->result() as $row) {
					$idprod   = $row->idcart;
					$inv   = $row->id_order;
					$hrg  = $row->harga;
					$jml  = $row->jumlah;
					$tot  = $hrg * $jml;
					$subtot += $tot;
				}
				if(!isset($idprod)) {
				echo '';
				}else{
				
			  ?>
			  <tr>
                <td style="padding:20px 18px 20px 20px">
                  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    
                    <tr>
                      <td style="height:49px; padding:10px; background:#FFDCDC;" valign="middle" colspan="3">
                        <font size="-1" color="696969" face="futura,tahoma,sans serif"><b>Pastikan sebelum mengklik tombol Konfirmasi, Anda sudah mentransfer ke rekening tujuan dengan benar.</b></font>
                      </td>
                    </tr>
                    <tr>
                      <td valign="middle" colspan="4">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tbody><tr>
                            <td width="100%">
                              <font size="-1" color="696969" face="futura,tahoma,sans serif">Nama Bank</font>
                            </td>
                           <td align="right" style="padding-top:5px">
						   <input type="hidden" name="delcart" id="delcart" value="<?php echo $idprod; ?>">
							<input style="width:186px" type="hidden" value="<?php echo $inv; ?>" id="invoice" name="invoice">
                              <input style="width:186px" type="text" value="" id="nama_bank" name="nama_bank" maxlength="19">
                            </td>
                          </tr>
                          <tr>
                            <td style="padding-top:5px">
                              <font size="-1" color="696969" face="futura,tahoma,sans serif">Nama Pemilik Rekening</font>
                            </td>
                            <td align="right" style="padding-top:5px">
                              <input style="width:186px" type="text" value="" id="akun_bank" name="akun_bank" maxlength="39">
                            </td>
                          </tr>
						  <tr>
                            <td style="padding-top:5px">
                              <font size="-1" color="696969" face="futura,tahoma,sans serif">Nomor Rekening</font>
                            </td>
                            <td align="right" style="padding-top:5px">
                              <input style="width:186px" type="text" value="" id="no_rek" name="no_rek" maxlength="19">
                            </td>
                          </tr>
						  <tr>
                            <td style="padding-top:5px">
                              <font size="-1" color="696969" face="futura,tahoma,sans serif">Transfer Nomor Rekening</font>
                            </td>
                            <td align="right" style="padding-top:5px">
                              <input style="width:186px" type="text" id="trans_norek" name="trans_norek" value="BNI 0482712995 (GALIH AGUSTIAN)" readonly="readonly">
                            </td>
                          </tr>
						  <tr>
                            <td style="padding-top:5px">
                              <font size="-1" color="696969" face="futura,tahoma,sans serif">Total Bayar</font>
                            </td>
                            <td align="right" style="padding-top:5px">
                              <input style="width:186px" type="text" id="total_bayar" name="total_bayar" value="<?php echo $subtot; ?>" readonly="readonly">
                            </td>
                          </tr>
                          
                        </tbody></table>
                      </td>
                    </tr>
				
                    <tr>
                      <td style="height:50px" valign="middle" colspan="4">
                        <span id="payStatus" style="display:none; font-style:italic; font-weight:bold">
                          <font size="-1" color="696969" face="futura,tahoma,sans serif">Please wait, payment is proceeding.</font>
                        </span>
                        <span id="errorDescription" style="font-style:italic; font-weight:bold; color:red">
                        </span>
                      </td>
                    </tr>
                    <tr style="height:23px">
                      <td colspan="4">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                          <tbody><tr>
                            <td style="width:15%">
                              <input type="button" name="button1" visible="true" style="visibility:visible" value="Cancel" onclick="
                                    javascript:window.location=&#39;https://www.swims.com/checkout/payment/Back&#39;
                                  ">
                            </td>
                            <td style="width:70%">
                              <div class="container">
                                <div class="content">
                                  <!--<img style="cursor:pointer" src="./vbv_neu_klein.png" border="0" alt="Verified by Visa" onclick="return openInfoLightBox(this, &#39;lb_vbv&#39;)">-->
                                </div>
                                <div class="content">
                                  <!--<img style="cursor:pointer" src="./mcsc_neu_klein.png" border="0" alt="MasterCard SecureCode" onclick="return openInfoLightBox(this, &#39;lb_mcsc&#39;)">-->
                                </div>
                              </div>
                            </td>
                            <td style="width:70%">
                              <input type="button" id="submit1" name="submit1" style="visibility:visible" value="Confirm">
                            </td>
							<?php } ?>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      </tbody></table>
    </form>
            </ul>
        </div>
<script>
$('#submit1').on('click',function(){
	var inv = document.getElementById('invoice').value;
	var nama = document.getElementById('nama_bank').value;
	var akun = document.getElementById('akun_bank').value;
	var norek = document.getElementById('no_rek').value;
	if(nama == '') {
		alert("Silahkan masukkan nama bank.");
		document.SSLForm.nama_bank.focus();
		return false;
	}
	if(akun == '') {
		alert("Silahkan masukkan nama akun bank.");
		document.SSLForm.akun_bank.focus();
		return false;
	}
	if(norek == '') {
		alert("Silahkan masukkan nomor rekening bank.");
		document.SSLForm.no_rek.focus();
		return false;
	}
	$.ajax({
    type:'POST',
    url:'<?php echo site_url('order/transfer2');?>',
	data: $('#SSLForm').serialize(),
    success:function(html){
		$('#payStatus').show();
		window.location = '<?php echo site_url('myorder'); ?>';
    }
    });
});
</script>