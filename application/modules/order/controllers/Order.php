<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Order extends CI_Controller{
        
        function __construct()
        {
            parent::__construct();
			$this->load->model('m_general');
			$this->load->model('m_order');
        }
		
	public function index() {
		$idses = $this->session->userdata('loc_id');
		$log   = $this->session->userdata('loc_login');
		if($log = TRUE) {
		$data['query_cart'] = $this->m_general->addCart();
		$data['content'] = $this->load->view('order_view',$data,TRUE);
		$this->load->view('home_index',$data);
		}else{
			redirect('home');
		}
	}
	
	public function transfer1() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$log   = $this->session->userdata('loc_login');
		if($log = TRUE) {
		$idses = $this->session->userdata('loc_id');
		$sql = $this->db->query('select id as idcart from tbl_cart where id_order="0"');
			foreach($sql->result() as $a) {
			$hsl_id = $a->idcart;
		}
		
		$sql = $this->db->query('select * from tbl_order where bank_transfer ="0" and bank_tujuan="0" order by id desc limit 1');
			foreach($sql->result() as $row) {
			$next_id = $row->id;
		}
		
		$bank = $this->input->post('nama_bank');
		$akun = $this->input->post('akun_bank');
		$norek = $this->input->post('no_rek');
		$tgl = date('Y-m-d');
		$tf_to = $this->input->post('trans_norek');
		$deb  = $this->input->post('total_bayar');
		$tf = $bank.' '.$norek.' '.'('.$akun.')';
		$idcart = $this->input->post('delcart');
		$data = array('id_order'  => $next_id,
						'user'=> $idses,
						'transfer_from' => $tf,
						'transfer_to' => $tf_to,
						'debit' => $deb,
						'credit' => 0,
						'tanggal' => $tgl,
						'status' => 'Pending');
		$tabel = 'tbl_payments';
		$this->m_order->insert_data($tabel, $data);
		$this->db->query('UPDATE tbl_order SET bank_transfer="'.$tf.'", bank_tujuan="'.$tf_to.'", status="In Checking" where user="'.$idses.'"');
		$data['query_cart'] = $this->m_general->addCart();
		$this->db->query('UPDATE tbl_cart SET id_order="'.$next_id.'", ip="'.$ip.'" where id="'.$hsl_id.'"');
		$data['invoice'] = $this->db->query('select * from tbl_order where user="'.$idses.'" and ip="'.$ip.'"');
		$this->load->view('proses_complete',$data);
		
		$mailuser = $this->db->query("select * from tbl_user where id='".$idses."'");
		foreach($mailuser->result() as $cek) {
			$email_user = $cek->email;
			$nama_user = $cek->nama;
		}
		
		$s_cart = $this->db->query("select * from tbl_cart where id_order='".$next_id."'");
			$num_x=1;
			foreach($s_cart->result() as $a) {
				$id_cart = $a->id;
				$produk_cart = $a->produk;
				$size_cart = $a->size;
			
		$s_pr = $this->db->query("select * from tbl_products where id='".$produk_cart."'");
			foreach($s_pr->result() as $b) {
				$nama_pr = $b->nama;
				$bahan_pr = $b->bahan;
				$warna_pr = $b->warna;
			}
			$to   = $email_user;
			$cc   = "andylife6@gmail.com";
		  	$from = "clothingampm@gmail.com";
		  	$this->load->library('email');
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
			$this->email->cc($cc);
		  	$this->email->subject('AMPM Official | Pembayaran');
		  	$message  = "<html><body>";
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=120></div></p>";
			$message .= "<p>Halo <strong>".$nama_user.",</strong> <br /></p>";
			$message .= "<p>Terima kasih telah berbelanja di <b>AMPM Clothing.</b>";
		  	$message .= "<p>Data Order (Invoice) : <br /></p>";
		  	$message .= "<p>Invoice : <strong>#".$next_id."</strong></p><br/>";
			$message .= "<p>Total   : IDR Rp. <strong>".number_format($deb,2,',','.')."</strong></p><br/>";
			$message .= "<p>Order Detail : <br /></p>";
			$message .=	"<span style='padding-left:20px;'><b>$num_x. $nama_pr $bahan_pr $warna_pr ($size_cart)</b></span><br>";
							$num_x++;
						}
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=100></div></p>";
			$message .= "<p><u>AMPM WeAreEqual</u></p>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
		redirect('myorder');
		}else{
			redirect('home');
		}
	}
	
	public function transfer2() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$log   = $this->session->userdata('loc_login');
		if($log = TRUE) {
		$idses = $this->session->userdata('loc_id');
		$sql = $this->db->query('select id as idcart from tbl_cart where id_order="0"');
			foreach($sql->result() as $a) {
			$hsl_id = $a->idcart;
		}
		
		$sql = $this->db->query('select * from tbl_order where bank_transfer ="0" and bank_tujuan="0" order by id desc limit 1');
			foreach($sql->result() as $row) {
			$next_id = $row->id;
		}
		
		$bank = $this->input->post('nama_bank');
		$akun = $this->input->post('akun_bank');
		$norek = $this->input->post('no_rek');
		$tgl = date('Y-m-d');
		$tf_to = $this->input->post('trans_norek');
		$deb  = $this->input->post('total_bayar');
		$tf = $bank.' '.$norek.' '.'('.$akun.')';
		$idcart = $this->input->post('delcart');
		$data = array('id_order'  => $next_id,
						'user'=> $idses,
						'transfer_from' => $tf,
						'transfer_to' => $tf_to,
						'debit' => $deb,
						'credit' => 0,
						'tanggal' => $tgl,
						'status' => 'Pending');
		$tabel = 'tbl_payments';
		$this->m_order->insert_data($tabel, $data);
		$this->db->query('UPDATE tbl_order SET bank_transfer="'.$tf.'", bank_tujuan="'.$tf_to.'", status="In Checking" where user="'.$idses.'"');
		$this->db->query('UPDATE tbl_cart SET id_order="'.$next_id.'", ip="'.$ip.'" where id="'.$hsl_id.'"');
		$data['query_cart'] = $this->m_general->addCart();
		$data['invoice'] = $this->db->query('select * from tbl_order where user="'.$idses.'" and ip="'.$ip.'"');
		$this->load->view('proses_complete',$data);
		
		$s_cart = $this->db->query("select * from tbl_cart where id_order='".$next_id."'");
			$num_x=1;
			foreach($s_cart->result() as $a) {
				$id_cart = $a->id;
				$produk_cart = $a->produk;
				$size_cart = $a->size;
			
		$s_pr = $this->db->query("select * from tbl_products where id='".$produk_cart."'");
			foreach($s_pr->result() as $b) {
				$nama_pr = $b->nama;
				$bahan_pr = $b->bahan;
				$warna_pr = $b->warna;
			}
			$to   = $email_user;
			$cc   = "andylife6@gmail.com";
		  	$from = "clothingampm@gmail.com";
		  	$this->load->library('email');
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
			$this->email->cc($cc);
		  	$this->email->subject('AMPM Official | Pembayaran');
		  	$message  = "<html><body>";
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=120></div></p>";
			$message .= "<p>Halo <strong>".$nama_user.",</strong> <br /></p>";
			$message .= "<p>Terima kasih telah berbelanja di <b>AMPM Clothing.</b>";
		  	$message .= "<p>Data Order (Invoice) : <br /></p>";
		  	$message .= "<p>Invoice : <strong>#".$next_id."</strong></p><br/>";
			$message .= "<p>Total   : IDR Rp. <strong>".number_format($deb,2,',','.')."</strong></p><br/>";
			$message .= "<p>Order Detail : <br /></p>";
			$message .=	"<span style='padding-left:20px;'><b>$num_x. $nama_pr $bahan_pr $warna_pr ($size_cart)</b></span><br>";
							$num_x++;
						}
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=100></div></p>";
			$message .= "<p><u>AMPM WeAreEqual</u></p>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
		redirect('myorder');
		}else{
			redirect('home');
		}
	}
	
	public function transfer3() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$log   = $this->session->userdata('loc_login');
		if($log = TRUE) {
		$idses = $this->session->userdata('loc_id');
		$sql = $this->db->query('select id as idcart from tbl_cart where id_order="0"');
			foreach($sql->result() as $a) {
			$hsl_id = $a->idcart;
		}
		
		$sql = $this->db->query('select * from tbl_order where bank_transfer ="0" and bank_tujuan="0" order by id desc limit 1');
			foreach($sql->result() as $row) {
			$next_id = $row->id;
		}
		$bank = $this->input->post('nama_bank');
		$akun = $this->input->post('akun_bank');
		$norek = $this->input->post('no_rek');
		$tgl = date('Y-m-d');
		$tf_to = $this->input->post('trans_norek');
		$deb  = $this->input->post('total_bayar');
		$tf = $bank.' '.$norek.' '.'('.$akun.')';
		$idcart = $this->input->post('delcart');
		$data = array('id_order'  => $next_id,
						'user'=> $idses,
						'transfer_from' => $tf,
						'transfer_to' => $tf_to,
						'debit' => $deb,
						'credit' => 0,
						'tanggal' => $tgl,
						'status' => 'Pending');
		$tabel = 'tbl_payments';
		$this->m_order->insert_data($tabel, $data);
		$this->db->query('UPDATE tbl_order SET bank_transfer="'.$tf.'", bank_tujuan="'.$tf_to.'", status="In Checking" where user="'.$idses.'"');
		$this->db->query('UPDATE tbl_cart SET id_order="'.$next_id.'", ip="'.$ip.'" where id="'.$hsl_id.'"');
		$data['query_cart'] = $this->m_general->addCart();
		$data['invoice'] = $this->db->query('select * from tbl_order where user="'.$idses.'" and ip="'.$ip.'"');
		$this->load->view('proses_complete',$data);
		
		$s_cart = $this->db->query("select * from tbl_cart where id_order='".$next_id."'");
			$num_x=1;
			foreach($s_cart->result() as $a) {
				$id_cart = $a->id;
				$produk_cart = $a->produk;
				$size_cart = $a->size;
			
		$s_pr = $this->db->query("select * from tbl_products where id='".$produk_cart."'");
			foreach($s_pr->result() as $b) {
				$nama_pr = $b->nama;
				$bahan_pr = $b->bahan;
				$warna_pr = $b->warna;
			}
			$to   = $email_user;
			$cc   = "andylife6@gmail.com";
		  	$from = "clothingampm@gmail.com";
		  	$this->load->library('email');
		    $config['protocol']     = 'mail';
			$config['charset'] 		= 'iso-8859-1';
			$config['wordwrap'] 	= TRUE;
			$config['mailtype'] 	= 'html';
		    $this->email->initialize($config);
		  	$this->email->from($from, 'AMPM Clothing');
		  	$this->email->to($to);
			$this->email->cc($cc);
		  	$this->email->subject('AMPM Official | Pembayaran');
		  	$message  = "<html><body>";
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=120></div></p>";
			$message .= "<p>Halo <strong>".$nama_user.",</strong> <br /></p>";
			$message .= "<p>Terima kasih telah berbelanja di <b>AMPM Clothing.</b>";
		  	$message .= "<p>Data Order (Invoice) : <br /></p>";
		  	$message .= "<p>Invoice : <strong>#".$next_id."</strong></p><br/>";
			$message .= "<p>Total   : IDR Rp. <strong>".number_format($deb,2,',','.')."</strong></p><br/>";
			$message .= "<p>Order Detail : <br /></p>";
			$message .=	"<span style='padding-left:20px;'><b>$num_x. $nama_pr $bahan_pr $warna_pr ($size_cart)</b></span><br>";
							$num_x++;
						}
			$message .= "<p><div align=left><img src=http://ampmstore.com/ampm/assets/images/ampmlogo.png width=100></div></p>";
			$message .= "<p><u>AMPM WeAreEqual</u></p>";
		  	$message .= "</body></html>";
		  	$this->email->message($message);
			$this->email->send();
		redirect('myorder');
		}else{
			redirect('home');
		}
	}
	
	public function bank_payment() {
		$idses = $this->session->userdata('loc_id');
		$data['query_cart'] = $this->m_general->addCart();
		$id = $this->input->post('1');
		$data['bank'] = $this->db->query('select * from tbl_bank where id="'.$id.'" order by id desc');
		$this->load->view('link_bank',$data);
	}
	
	public function bank_payment2() {
		$idses = $this->session->userdata('loc_id');
		$data['query_cart'] = $this->m_general->addCart();
		$id = $this->input->post('2');
		$data['bank'] = $this->db->query('select * from tbl_bank where id="'.$id.'" order by id desc');
		$this->load->view('link_bank2',$data);
	}
	
	public function bank_payment3() {
		$idses = $this->session->userdata('loc_id');
		$data['query_cart'] = $this->m_general->addCart();
		$id = $this->input->post('3');
		$data['bank'] = $this->db->query('select * from tbl_bank where id="'.$id.'" order by id desc');
		$this->load->view('link_bank3',$data);
	}
	
	public function linkbank_payment() {
		$data['query_cart'] = $this->m_general->addCart();
		$this->load->view('form_bank',$data);
	}
        	
	function logout(){
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}	
}

?>