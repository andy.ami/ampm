<!DOCTYPE html>
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="keywords">
    <meta name="author" content="">
    <title>AMPM WeAreEqual</title>
	
	<script src="<?php echo base_url();?>assets/libraries/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery-2.1.0.min.js"></script>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.min.css">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
	
</head>
<body class="home">
 <script>
  /*  dataLayer = [{
        'pageType': 'HomePage',
        'market': 'US',
        'currencyCode': 'USD',
        'userId': ''
    }];*/

</script>
	<!--<noscript>
        <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-W59RMR&amp;gtm_auth=JIaymc1oaiuK8a8PywEVTA&amp;gtm_preview=env-1&amp;gtm_cookies_win=x"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>-->
    <script>
    /*    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '../www.googletagmanager.com/gtm5445.html?id=' + i + dl + '&gtm_auth=JIaymc1oaiuK8a8PywEVTA&gtm_preview=env-1&gtm_cookies_win=x'; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W59RMR'); */
    </script>
    
    <!-- Both Google Tag Manager -->
    <nav class="mobile-header">
        <ul class="cf">
            <li>
                <button class="mobile-header-toggle" title="Toggle main menu visiblity">
                    <svg class="inline-icon-hamburger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <line x1="0" x2="24" y1="5.5" y2="5.5"></line>
                        <line x1="0" x2="24" y1="12.5" y2="12.5"></line>
                        <line x1="0" x2="24" y1="19.5" y2="19.5"></line>
                    </svg>
                </button>
            </li>
            <li>
                <a href="/" class="mobile-header-logo">
                    <img src="<?php echo base_url(); ?>assets/images/ampmlogo.png" alt="AMPM" title="AMPM">
                </a>
            </li>
			
            <?php
				foreach($query_cart->result() as $row) {
				$qty = $row->hit_cart;
			}
			?>
			
			<li><button class="mobile-shopping-cart-toggle" title="Toggle shopping cart visibility">
                    <svg class="inline-icon-basket" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16,3.9 C16,2.3,14.7,1,13.1,1h-2.4C9,1,7.7,2.3,7.7,3.9v2.9L2,6.9v13.6c0,1.3,1.1,2.3,2.3,2.3h15.3c1.3,0,2.3-1.1,2.3-2.3V9.6 c0-1.3-1.1-2.3-2.3-2.3h-3.8l-0.1,3.6c0,1.6-1.3,2.9-2.9,2.9h-1.9c-1.6,0-2.9-1.3-2.9-2.9"></path>
                    </svg>
                    <span class="cart-number-of-items">
					<?php 
			if(!isset($qty)) {
			echo '0';
			}else{
			?>		
		<?php echo $qty; ?></span></button></li>
		<? } ?>
					
        </ul>
    </nav>
    
<header class="global-header">
        
<a href="<?php echo site_url('home'); ?>" class="global-header-logo">
    <img src="<?php echo base_url(); ?>assets/images/ampmlogo.png" alt="AMPM" title="AMPM">
</a>


<form action="#" class="mobile-search">
    <!--<input type="search" name="q" id="mobile-search-input">
    <label for="mobile-search-input">Search</label>
    <button type="submit" title="Search"></button>-->
</form>

    <nav class="main-nav">
        <ul class="main-nav__first-level">
                <li>
    <a href="<?php echo site_url('home'); ?>" class=""><span class="link-text">AMPM WeAreEqual</span></a>
    </li>
    <li>
        <!--    <a href="" class="has-sub-nav "><span class="link-text">T-SHIRTS</span></a>
            <ul class="main-nav__second-level "> -->
		<!--	<li>    <a href="<?php echo site_url('combed'); ?>" class=""><span class="link-text">Galoshes</span></a>
			</li>
        <li><a href="#" class=""><span class="link-text">Outerwear</span></a>
		</li>-->

        <li>
            <a href="" class="has-sub-nav"><span class="link-text">Appareal</span></a>
            <ul class="main-nav__third-level">
                <li><a href="<?php echo site_url('combed'); ?>" class=""><span class="link-text">T-SHIRT</span></a></li>
				<!--<li><a href="<?php echo site_url('bamboo'); ?>" class=""><span class="link-text">Bamboo</span></a></li>
				<li><a href="<?php echo site_url('mamboo'); ?>" class=""><span class="link-text">Mamboo</span></a></li>-->
				<!--<li><a href="#" class=""><span class="link-text">Boots</span></a></li>
				<li><a href="#" class=""><span class="link-text">Waterproof</span></a></li>-->
            </ul>
        </li>
         <li>
            <a href="<?php echo site_url('how_to_order'); ?>" class=""><span class="link-text">Cara Belanja</span></a>
        </li>
		<li>
            <a href="<?php echo site_url('size_guide'); ?>" class=""><span class="link-text">Panduan Ukuran</span></a>
        </li>
       <li><br></li>
        <li><a href="" class="has-sub-nav "><span class="link-text">TENTANG AMPM</span></a>
        	<ul class="main-nav__second-level">                        
                <!--<li><a href="http://ampmstore.com/story" class=""><span class="link-text">AMPM Story</span></a></li>-->
                <li><a href="<?php echo site_url('contact'); ?>" class=""><span class="link-text">KONTAK</span></a></li>
            </ul>
        </li>

            </ul>
    </li>
    </ul>
    </nav>

	<nav class="secondary-nav">
    <ul>
	<?php 
		$a = $this->session->userdata('loc_login');
		if($a != true) {
	?>
            <li class="login-link"><a href="<?php echo site_url('login_apps'); ?>" id="login-register-link"><span class="link-text">Log in | Register</span></a></li>
		<?php }else{ ?>
			<li><a href="<?php echo site_url('account'); ?>"><span class="link-text">My Account</span></a></li>
			<li><a href="<?php echo site_url('myorder'); ?>"><span class="link-text">My Order</span></a></li>
	
	</ul>
		<?php } ?>
</nav>
<script>
    $("#mobile-selector-country").change(function () {
        var id = $("#mobile-selector-country option:selected").attr("value");
        var data = {
            market: id.split(",")[0],
            language: id.split(",")[1]
        };
        $.ajax({
            url: "/Language/SetLanguageAndMarket",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            processData: false,
            success: function (result) {
                if (!result) {
                    $("#mobile-language-selector-country-error").slideDown(500);
                } else {
                    $("#mobile-language-selector-country-error").hide();

                    var url = window.location.href.split('?')[0];
                    if (url.indexOf('?') > -1) {
                        url += "&showCountries=true";
                    } else {
                        url += "?showCountries=true";
                    }
                    window.location.href = url;
                }
            }
        });
    });
    $("#mobile-language-selector-country-error").hide();
</script>
    </header>

<nav class="right-nav">
    <ul>
        <!--<li><button class="search-trigger">Search</button></li>-->
		<?php
foreach($query_cart->result() as $row) {
	$qty = $row->hit_cart;
}
		
?>
        <li><button class="shopping-cart-trigger" onclick="updateShoppingCart('en');">The Bag<span class="cart-number-of-items">
		<?php 
		if(!isset($qty)) {
		echo '0';
	}else{
?>		
		<?php echo $qty; ?></span></button></li>
	<? } ?>
    </ul>
</nav>
 
    <div class="login-drawer">
	<button class="login-drawer__close"><span class="visually-hidden">Close login window</span></button>
<div class="login-drawer__login-form">
    <h3 class="login-drawer__heading">Log in to your account</h3>
    <form action="#" id="login-form" method="post" data-redirect="<?php echo site_url('account'); ?>">
        <label class="text-input-label" for="nama_mail">E-mail</label>
        <input type="text" class="text-input" id="nama_email" name="nama_email">
        <label class="text-input-label" for="nama_password">Password</label>
        <input type="password" class="text-input" id="nama_password" name="nama_password">
        <a href class="forgot-password-link">Forgot password?</a>
        <input class="checkbox" type="checkbox" id="login-form-remember-me">
        <label for="login-form-remember-me">Remember me</label>
        <p class="error-message" id="login-form-error"></p>
        <input type="submit" class="form-submit" value="Log in" id="login-form-submit">
    </form>
    <script type="text/javascript">
            $('#login-form-submit').click(function (event) {
				event.preventDefault();
				$('#login-form-error').show();
				var email = document.getElementById('nama_email').value;
				var password = document.getElementById('nama_password').value;
                $.ajax({
                    url: '<?php echo site_url('login'); ?>',
					type: 'POST',
					//data: {'nama_email':email, 'nama_password':password},
					data: $('#login-form').serialize(),
                    success: function (data) {
						if(data != '') {
							$('#login-form-error').html("Email atau password salah.");
                            $('#login-form-error').slideDown(500);
                        } else {
							$('#login-form-error').hide();
						   var redirect = $('#login-form').data('redirect');
                            window.location.replace(redirect);
                        }
                    }
                });
            });
		//}
            $('#login-form-error').hide();
    </script>
    <h3 class="login-drawer__heading">Don't have an account yet?</h3>
    <button class="register-form-open">Register an account</button>
</div>
    <div class="login-drawer__password-recovery-form">
    <a href="" class="password-recovery-form-close">Back to Log in</a>
    <h3 class="login-drawer__heading">Forgot password?</h3>
    <p>Forgot your password? Don't worry! Fill in your e-mail address and we'll send you a new one.</p>
    <form action="/Account/RecoverPassword" id="password-recovery-form" method="post">
        <label class="text-input-label" for="reset_password">E-mail</label>
        <input type="text" class="text-input" id="reset_password" name="reset_password">
        <p class="error-message" id="password-recovery-form-error"></p>
        <p class="confirmation-message" id="password-recovery-form-confirmation"></p>
        <input type="submit" class="form-submit" value="Reset password" id="password-recovery-submit">
    </form>
    <script type="text/javascript">
		$(document).ready(function() {
			$('#password-recovery-form-error').hide();
				$('#password-recovery-form-confirmation').hide();
            $('#password-recovery-submit').click(function (event) {
                event.preventDefault();
				$('#password-recovery-form-error').show();
				$('#password-recovery-form-confirmation').show();
                var reset_password = document.getElementById('reset_password').value;
				if(reset_password == '') {
					$('#password-recovery-form-error').html('Masukkan nama email Anda.');
				}else{
                $.ajax({
                    url: '<?php echo site_url('home/reset_password'); ?>',
					type: 'POST',
					data: $('#password-recovery-form').serialize(),
                    success: function (data) {
						if(data == 'OK') {
							$('#password-recovery-form-error').hide();
							$('#password-recovery-form-confirmation').show();
							$('#password-recovery-form-confirmation').html('Password berhasil di reset.');
                        }else{
							$('#password-recovery-form-confirmation').hide();
							$('#password-recovery-form-error').show();
							$('#password-recovery-form-error').html('Nama email <b>'+reset_password+'</b> tidak terdaftar.');
						}
					}
                });
				}
            });
		});
    </script>
</div>
    


<div class="login-drawer__register-form">
    <a href="" class="register-form-close">Back to Log in</a>

	
    <h3 class="login-drawer__heading">Register an account</h3>
    <form action="#" method="post" id="register-form1">
		<label class="text-input-label" for="nama_lengkap">Nama Lengkap</label>
        <input type="text" class="text-input" id="nama_lengkap" name="nama_lengkap">
		
        <label class="text-input-label" for="email">E-mail</label>
        <input type="text" class="text-input" id="email" name="email">
        
		<label class="text-input-label" for="telp">Telepon</label>
        <input type="text" class="text-input" id="telp" name="telp">
		
        <label class="text-input-label" for="register-form-password">Password</label>
        <input type="password" class="text-input" id="password1" name="password1">
		
	<!--	<label class="text-input-label" for="register-form-password">Re-Password</label>
        <input type="password" class="text-input" id="password2" name="password2">
		
        <input class="checkbox" type="checkbox" id="register-form-terms">
        <label for="register-form-terms"><b>Yes</b>, I accept the <a href="/help--info/terms-conditions/" target="_blank">terms and conditions</a></label>-->
		
		<p class="error-message" id="error"></p>
		<p class="error-message" id="register-form-error"></p>
        <p class="confirmation-message" id="register-form-confirmation"></p>
        <input type="submit" class="form-submit" value="Register" id="register-form-submit">
    </form>

    <script type="text/javascript">
		$(document).ready(function() {
			$('#register-form-error').hide();
			$('#register-form-confirmation').hide();
			$('#error').hide();
        $('#register-form-submit').click(function(event) {
			event.preventDefault();
			var nama = $('#nama_lengkap').val();
			var email = $('#email').val();
			var telp = $('#telp').val();
			var pass = $('#password1').val();
			if(nama == '' || email == '' || telp == '' || pass == '') {
				$('#error').show();
				$('#error').html('Silahkan input data Anda dengan benar.');
			}else{
			$.ajax({
                url: '<?php echo site_url('register/addReg'); ?>',
                type: 'POST',
                data: $('#register-form1').serialize(),
				success: function (data) {
					if(data == 'OK'){
						$('#error').hide(); 
						$('#register-form-error').hide(); 
						$('#register-form-confirmation').show(); 
						$('#register-form-confirmation').html('Proses pendaftaran berhasil. Silahkan login menggunakan akun Anda.');
					}else{
						$('#error').hide();
						$('#register-form-confirmation').hide(); 
						$('#register-form-error').show(); 
						$('#register-form-error').html('Data email "<b>'+email+'</b>" sudah terdaftar.');
					}
				}
			});
			}
		});
	});
    </script>
</div>
</div>


<script>
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    $(document).ready(function () {
        var showCountries = getUrlVars()["showCountries"];
        if (showCountries === "true") {
            $("#language-selector").show();
        }
    });
</script>

<div class="language-selector" id="language-selector">
    <h3 class="language-selector__heading">Choose your shipping destination</h3>
    <label for="language-selector-country" class="select-label">Country</label>
    <select id="language-selector-country" class="select">
        <option value="ID,IND" selected=selected>Indonesia (Rp)</option>
		<option value="US,USA">United States (USD)</option>
    </select>
    <button type="button" class="language-selector__close"><span class="visually-hidden">Close language selection window</span></button>
    <p class="error-message" id="language-selector-country-error">Could not change language</p>
</div>

<script>
    $("#language-selector-country").change(function () {
        var id = $("#language-selector-country option:selected").attr("value");
        var data = {
            market: id.split(",")[0],
            language: id.split(",")[1]
        };
        $.ajax({
            url: "/Language/SetLanguageAndMarket",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            processData: false,
            success: function (result) {
                if (!result) {
                    $("#language-selector-country-error").slideDown(500);
                } else {
                    $("#language-selector-country-error").hide();
                    var newText = $("#language-selector-country option:selected").text();
                    $("#language-selector-link > .link-text").text(newText);

                    var url = window.location.href.split('?')[0];
                    if (url.indexOf('?') > -1) {
                        url += "&showCountries=true";
                    } else {
                        url += "?showCountries=true";
                    }
                    window.location.href = url;
                }
            }
        });
    });
    $("#language-selector-country-error").hide();
</script>
<?php
foreach($query_cart->result() as $row) {
	$qty = $row->hit_cart;
}
?>

<div class="shopping-cart" id="shopping-cart" ng-controller="HeaderCartController">
 <header class="shopping-cart__header">
	<h3 class="shopping-cart__heading">The Bag <span class="cart-number-of-items">
<?php 
	if(!isset($qty)) {
		echo '0';
	}else{
?>				
	<?php echo $qty; ?>
	<? } ?>	
	</span></h3>
        <a href="<?php echo site_url('checkout/addCart'); ?>" class="shopping-cart__checkout">Proceed to checkout</a>
    </header>
	<?php
	$subtot = 0;
	//$hrg_tot = 0;
foreach($query_cart->result() as $row) {
	$idprod   = $row->idcart;
	$nama = $row->nama;
	$wrn  = $row->warna;
	$hrg  = $row->harga;
	$foto = $row->foto;
	$size = $row->size;
	$jml  = $row->jumlah;
	$tot  = $hrg * $jml;
	if($hrg) {
		$diskon_pr = (($hrg*20)/100);
		$hrg_diskon = $hrg - $diskon_pr;
	}

	if($tot) {
		$diskon = (($tot*20)/100);
		$hrg_tot = $tot - $diskon;
	}
	$subtot += $hrg_tot;

?>
<?php
	if(!isset($jml)) {
	echo '';
	}else{
?>
    <div class="swims-loader"></div>
    <ul class="shopping-cart__products">
        <li class="shopping-cart__product" ng-repeat="p in products" ng-cloack>
			
            <button type="button" class="remove-from-cart" onClick="removeFromCart(<?php echo $idprod;?>)" title="Remove product from shopping cart"></button>
            
			<a class="product">
                <div class="product__image">
                    <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $foto; ?>" ng-alt="<?php echo $foto; ?>" ng-title="<?php echo $foto; ?>" />
                    <div class="product__hover-image" ng-show="p.SecondaryImageUrl">
                        <img src="<?php echo base_url(); ?>assets/images/produk/<?php echo $foto; ?>" ng-alt="<?php echo $foto; ?>" ng-title="<?php echo $foto; ?>"/>
                    </div>
                </div>
                <div class="product__description">
                    <p class="product__name"><?php echo $nama; ?></p>
                    <p class="product__color"><?php echo $wrn; ?></p>
					
                    <p class="product__price" ng-show="{{p.LineItemDiscount == 0}}">
                        <?php echo number_format($hrg_diskon); ?>
                    </p>
                    <!--<p class="product__price" ng-show="{{p.LineItemDiscount != 0}}">
                        <span class="product__original-price">
                            {{p.PlacedPrice}} {{currency}}
                            <span class="currency"></span>
                        </span>

                    </p>-->
                </div>
            </a>
	
            <ul class="shopping-cart__product-details">
                <li class="shopping-cart__product-size">Size : <?php echo $size; ?>, </li><br/>
                <li class="shopping-cart__product-qty">Qty : <?php echo $jml; ?></li>
                <li class="shopping-cart__product-price">
                    <p> <span class="currency"><?php echo number_format($hrg_tot); ?></span></p>
                </li>
            </ul>
        </li>
    
<? }} ?>
</ul>

    <ul class="shopping-cart__subtotal">
        <li>Sub total <span class="amount"> <span class="currency"><?php echo number_format($subtot); ?></span></span></li>
        <!--<li ng-repeat="discount in discountCodes" ng-cloack>
            {{discount.Code}} <span class="amount">- {{discount.Amount}} <span class="currency">{{currency}}</span></span>
        </li>-->
        
    </ul>

    <ul class="shopping-cart__total">
        <li class="final-total">Total <span class="amount"> <span class="currency"><?php echo number_format($subtot); ?></span></span></li>
    </ul>


    <a href="<?php echo site_url('checkout/addCart'); ?>" class="shopping-cart__checkout">Proceed to checkout</a>
    <button class="close-shopping-cart"><span class="visually-hidden">Close shopping cart</span></button>
</div>

<div class="search" ng-controller="AutoCompleteController">
    <header class="search__header">
        <h3 class="search__heading">Search</h3>
    </header>

    <form class="search__input-form" ng-init="init('en', '/system/search/', 'USD', 'US')">
        <input type="search" id="search" placeholder="Type here" onChange="populate()" />
        

        <button type="submit" ng-click="gotoSearchPage()"><span class="visually-hidden">Search</span></button>
    </form>

    <h5 class="search__results-heading" style="display: none">Products</h5>
    <ul class="search__results-list">
        <li class="search__result" ng-repeat="product in products">
            <a class="product" ng-href="{{product.Url}}" ng-click="productClick(product)">
                <div class="product__image">
                    <img ng-src="{{product.Image}}" ng-alt="{{product.DefaultImageAltText}}" ng-title="{{product.DefaultImageTitle}}" />
                    <div class="product__hover-image" ng-show="product.SecondaryImage">
                        <img ng-src="{{product.SecondaryImage}}" ng-alt="{{product.SecondImageAltText}}" ng-title="{{product.SecondImageTitle}}" />
                    </div>
                </div>
                <div class="product__description">
                    <p class="product__name">{{product.Name}}</p>
                    <p class="product__color">{{product.Color}}</p>
                    <p class="product__price">
                        {{product.Price}}
                        <span class="currency">{{currency}}</span>
                    </p>
                </div>
            </a>
        </li>
    </ul>
    <a href="#" class="search__all-results-link" style="display: none">See all products</a>
    <h5 class="search__results-heading" style="display: none">Articles</h5>
    <ul class="search__results-list">
        <li class="search__result" ng-repeat="article in articles">
            <a ng-href="{{article.Url}}" class="media-block__link">
                <div class="media-block">
                    <div class="media-block__image">
                        <button type="button" class="media-block__image-toggler" title="Show full image"></button>
                        <img ng-if="{{article.Image}}" ng-src="{{article.Image}}" ng-alt="{{article.Name}}" ng-title="{{article.Name}}">
                    </div>
                    <div class="media-block__text">
                        <p>{{article.Name}}</p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <a href="#" class="search__all-results-link" style="display: none">See all articles</a>
    <button class="close-search"><span class="visually-hidden">Close search</span></button>
</div>

<!-- Ini Body Page -->
	
	<?php
		echo $content;
    ?>

	
<!------------------------------->


    <div id="video-lightbox">
        <button class="video-lightbox-close" title="Close video player"></button>
        <iframe src="" frameborder="0" title="Video player"></iframe>
    </div>
	
	
    <button type="button" id="scroll-to-top" title="Back to top"></button>
    <!-- JS and analytics only. -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>

  <!--  <script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.validate.mind91e.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.validate.unobtrusive.mind91e.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ObjectUtilsd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/AjaxUtilsd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/starterkitd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/logind91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Productd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ProductDialogd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/SizeGuideDialogd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.autocomplete.mind91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/HelpDialogd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/angular.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/angular-resource.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/app/ProductAndSearchController.js"></script>
    <script type="text/javascript">
        //Oxx.AjaxUtils.sitePath = '/';
        //Oxx.AjaxUtils.language = 'ind';
    </script> -->
    
<!-- <script type="text/javascript" src="https://dl.episerver.net/12.2.2/epi-util/native.history.js"></script>
<script type="text/javascript" src="https://dl.episerver.net/12.2.2/epi-util/find.js"></script>
<script type="text/javascript">
if(FindApi){var api = new FindApi();api.setApplicationUrl('/');api.setServiceApiBaseUrl('/find_v2/');api.processEventFromCurrentUri();api.bindWindowEvents();api.bindAClickEvent();api.sendBufferedEvents();}
</script>-->

<script>
function populate() {
//$(function(){
	alert('gggg');
  var nama = $("#search").val();
  $.ajax({
    url: "<?php echo site_url('home/get_data'); ?>", // path to the get_birds method
	minLength: 2, //search after two characters
	data :{nama},
	select: function(event,ui){
    //do something
    }
  });
}
</script>
<script>
function removeFromCart(idprod) {
	if (confirm("Anda yakin akan menghapus transaksi ini?")) {
        $.ajax({
            url: '<?php echo site_url('con_global/delete'); ?>',
            type: "POST",
            data: 
			{
                idprod: idprod
            },
            success: function(data) {
				window.location.reload();
            }
        });
		 $(this).parents(".record").animate("fast").animate({
                        opacity : "hide"
                    }, "slow");
		}
		return false;
    }
</script>

</body>
</html>
