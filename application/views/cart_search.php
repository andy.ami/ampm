<div class="shopping-cart" id="shopping-cart" ng-controller="HeaderCartController">
    <header class="shopping-cart__header">
        <h3 class="shopping-cart__heading">The Bag <span class="cart-number-of-items"><?php echo $hit_cart; ?></span></h3>
        <a href="<?php echo site_url('checkout/cart'); ?>" class="shopping-cart__checkout">Proceed to checkout</a>
    </header>
    <div class="swims-loader"></div>
    <ul class="shopping-cart__products">
        <li class="shopping-cart__product" ng-repeat="p in products" ng-cloack>
            <button type="button" class="remove-from-cart" ng-click="removeFromCart('en', p, $event)" title="Remove product from shopping cart"></button>
            <a class="product" ng-href="{{p.Url}}">
                <div class="product__image">
                    <img ng-src="{{p.ImageUrl}}" ng-alt="{{p.Name}}" ng-title="{{p.Name}}" />
                    <div class="product__hover-image" ng-show="p.SecondaryImageUrl">
                        <img ng-src="{{p.SecondaryImageUrl}}" ng-alt="{{p.Name}}" ng-title="{{p.Name}}"/>
                    </div>
                </div>
                <div class="product__description">
                    <p class="product__name">{{p.Name}}</p>
                    <p class="product__color">{{p.Color}}</p>
                    <p class="product__price" ng-show="{{p.LineItemDiscount == 0}}">
                        {{p.PlacedPrice}} {{currency}}
                    </p>
                    <p class="product__price" ng-show="{{p.LineItemDiscount != 0}}">
                        <span class="product__original-price">
                            {{p.PlacedPrice}} {{currency}}
                            <span class="currency"></span>
                        </span>
                        {{p.PlacedPrice - (p.LineItemDiscount / p.Quantity)}} {{currency}}
                    </p>
                </div>
            </a>
            <ul class="shopping-cart__product-details">
                <li class="shopping-cart__product-size">Size {{p.Size}}</li>
                <li class="shopping-cart__product-qty">Qty {{p.Quantity}}</li>
                <li class="shopping-cart__product-price">
                    <p>{{p.LineItemTotal - p.LineItemDiscount}} <span class="currency">{{currency}}</span></p>
                </li>
            </ul>
        </li>
    </ul>

    <ul class="shopping-cart__subtotal">
        <li>Sub total <span class="amount">{{cartTotalLineItemsAmount}} <span class="currency">{{currency}}</span></span></li>
        <li ng-repeat="discount in discountCodes" ng-cloack>
            {{discount.Code}} <span class="amount">- {{discount.Amount}} <span class="currency">{{currency}}</span></span>
        </li>
        <li>Estimated shipping <span class="amount">{{cartShipping.Total}} <span class="currency">{{currency}}</span></span></li>
    </ul>

    <ul class="shopping-cart__total">
        <li class="final-total">Total <span class="amount">{{cartTotalAmount}} <span class="currency">{{currency}}</span></span></li>
    </ul>

        <p class="shopping-cart__shipping-information">
            Free shipping on orders over USD180 
        </p>

    <a href="/checkout/cart/" class="shopping-cart__checkout">Proceed to checkout</a>
    <button class="close-shopping-cart"><span class="visually-hidden">Close shopping cart</span></button>
</div>

<div class="search" ng-controller="AutoCompleteController">
    <header class="search__header">
        <h3 class="search__heading">Search</h3>
    </header>

    <form class="search__input-form" ng-init="init('en', '/system/search/', 'USD', 'US')">
        <input type="search" placeholder="Type here" ng-model="searchTerm" ng-change="populate()" />
        

        <button type="submit" ng-click="gotoSearchPage()"><span class="visually-hidden">Search</span></button>
    </form>

    <h5 class="search__results-heading" style="display: none">Products</h5>
    <ul class="search__results-list">
        <li class="search__result" ng-repeat="product in products">
            <a class="product" ng-href="{{product.Url}}" ng-click="productClick(product)">
                <div class="product__image">
                    <img ng-src="{{product.Image}}" ng-alt="{{product.DefaultImageAltText}}" ng-title="{{product.DefaultImageTitle}}" />
                    <div class="product__hover-image" ng-show="product.SecondaryImage">
                        <img ng-src="{{product.SecondaryImage}}" ng-alt="{{product.SecondImageAltText}}" ng-title="{{product.SecondImageTitle}}" />
                    </div>
                </div>
                <div class="product__description">
                    <p class="product__name">{{product.Name}}</p>
                    <p class="product__color">{{product.Color}}</p>
                    <p class="product__price">
                        {{product.Price}}
                        <span class="currency">{{currency}}</span>
                    </p>
                </div>
            </a>
        </li>
    </ul>
    <a href="#" class="search__all-results-link" style="display: none">See all products</a>
    <h5 class="search__results-heading" style="display: none">Articles</h5>
    <ul class="search__results-list">
        <li class="search__result" ng-repeat="article in articles">
            <a ng-href="{{article.Url}}" class="media-block__link">
                <div class="media-block">
                    <div class="media-block__image">
                        <button type="button" class="media-block__image-toggler" title="Show full image"></button>
                        <img ng-if="{{article.Image}}" ng-src="{{article.Image}}" ng-alt="{{article.Name}}" ng-title="{{article.Name}}">
                    </div>
                    <div class="media-block__text">
                        <p>{{article.Name}}</p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <a href="#" class="search__all-results-link" style="display: none">See all articles</a>
    <button class="close-search"><span class="visually-hidden">Close search</span></button>
</div>