
<header class="global-header">
        
<a href="/" class="global-header-logo">
    <img src="//swimscdnprod.azureedge.net/assets/images/swims-logo.svg" alt="SWIMS" title="SWIMS">
</a>


<form action="/system/search/" class="mobile-search">
    <input type="search" name="q" id="mobile-search-input">
    <label for="mobile-search-input">Search</label>
    <button type="submit" title="Search"></button>
</form>

    <nav class="main-nav">
        <ul class="main-nav__first-level">
                <li>
    <a href="#" class=""><span class="link-text">Bradleys Footwear</span></a>
    </li>
    <li>
            <a href="" class="has-sub-nav "><span class="link-text">Men&#39;s</span></a>
            <ul class="main-nav__second-level ">
                        <li>
            <a href="" class="has-sub-nav "><span class="link-text">Footwear</span></a>
            <ul class="main-nav__third-level ">
                        <li>    <a href="#" class=""><span class="link-text">Loafers</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Sneakers</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Lace-ups</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Boots</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Waterproof</span></a>
</li>

            </ul>
        </li>
        <li>    <a href="#" class=""><span class="link-text">Galoshes</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Outerwear</span></a>
</li>
       

            </ul>
    </li>
    </ul>
    </nav>

	<nav class="secondary-nav">
    <ul>
            <li class="login-link"><a href="/system/login/" id="login-register-link"><span class="link-text">Log in | Register</span></a></li>
			<li class="language-selector-link"><a href="#" id="language-selector-link"><span class="link-text">Indonesia (Rp)</span></a></li>
		<li class="mobile-language-selector">
            <div class="select">
                <select id="mobile-selector-country" title="select country">
					<option value="ID,IND" selected=selected>Indonesia (Rp)</option>
                    <option value="US,USA" >United States (USD)</option>
                </select>
            </div>
            <div class="error-message" id="mobile-language-selector-country-error">Could not change language</div>
        </li>
	</ul>
</nav>
<script>
    $("#mobile-selector-country").change(function () {
        var id = $("#mobile-selector-country option:selected").attr("value");
        var data = {
            market: id.split(",")[0],
            language: id.split(",")[1]
        };
        $.ajax({
            url: "/Language/SetLanguageAndMarket",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            processData: false,
            success: function (result) {
                if (!result) {
                    $("#mobile-language-selector-country-error").slideDown(500);
                } else {
                    $("#mobile-language-selector-country-error").hide();

                    var url = window.location.href.split('?')[0];
                    if (url.indexOf('?') > -1) {
                        url += "&showCountries=true";
                    } else {
                        url += "?showCountries=true";
                    }
                    window.location.href = url;
                }
            }
        });
    });
    $("#mobile-language-selector-country-error").hide();
</script>
    </header>