<footer class="global-footer">
    <div class="centered-content">
        <div><div class="block instagramblock narrow ">

<h2 class="global-footer__heading">World of SWIMS</h2>
<div class="global-footer__hashtags"><p>Tag us on Instagram @SWIMSofficial #SWIMSofficial&nbsp;</p></div>
<ul class="global-footer__instagram-images">
    <li><img src="../../../../../../../swimscdnprod.azureedge.net/images/insta1.jpg" alt="SWIMS"></li>
    <li><img src="../../../../../../../swimscdnprod.azureedge.net/images/insta2.jpg" alt="SWIMS"></li>
    <li><img src="../../../../../../../swimscdnprod.azureedge.net/images/insta3.jpg" alt="SWIMS"></li>
    <li><img src="../../../../../../../swimscdnprod.azureedge.net/images/insta4.jpg" alt="SWIMS"></li>
</ul></div><div class="block titleandtextblock narrow ">

<h2 class="global-footer__heading">How may we help you today?</h2>
<div class="global-footer__sub-title"></div></div></div>

        <ul class="global-footer__content"><li class="global-footer__content__item">


<h5>About SWIMS</h5>
<ul class="global-footer__link-list">
        <li><a href="../../../../../../about-swims/swims-story/index.html">SWIMS Story</a></li>
        <li><a href="../../../../../../about-swims/behind-the-scenes/index.html">Behind the Scenes</a></li>
        <li><a href="../../../../../../about-swims/store-locator/index.html">Store Locator</a></li>
        <li><a href="../../../../../../about-swims/product-care/index.html">Product Care</a></li>
</ul>
</li><li class="global-footer__content__item">


<h5>Help &amp; Info</h5>
<ul class="global-footer__link-list">
        <li><a href="../../../../../../help--info/contact-us/index.html">Contact US</a></li>
        <li><a href="../../../../../../help--info/F-A-Q/index.html">F.A.Q.</a></li>
        <li><a href="../../../../../../help--info/return-policy/index.html">Return Policy</a></li>
        <li><a href="../../../../../../help--info/shipping-information/index.html">Shipping Information</a></li>
        <li><a href="../../../../../../old-pages/size-guide/index.html">Size Guide</a></li>
        <li><a href="../../../../../../help--info/terms-conditions/index.html">Terms &amp; Conditions</a></li>
        <li><a href="../../../../../../help--info/privacy--cookies/index.html">Privacy &amp; Cookies</a></li>
</ul>
</li><li class="global-footer__content__item">

<h5>Follow us</h5>
<ul class="social-links">
    <li>
        <a href="https://www.facebook.com/Swimsofficial" class="social-links__item facebook"></a>
        <span class="social-links__tooltip">Follow us</span>
    </li>
    <li>
        <a href="https://www.instagram.com/swimsofficial/" class="social-links__item instagram"></a>
        <span class="social-links__tooltip">Follow us</span>
    </li>
</ul></li><li class="global-footer__content__item">

<h5>Subscribe to our newsletter</h5>
<p>Get updates on our latest offers, news and arrivals!</p>
<div class="mini-form" ng-controller="NewsLetterController" ng-init="init('en')">
    <input type="text" class="mini-form__input" placeholder="Your e-mail here" ng-model="email"/>
    <button class="mini-form__submit" ng-click="addEmailToList()">Send</button>
    
</div>
</li></ul>
    </div>

    <div class="global-footer__footer">
        <div class="centered-content">
            <ul class="shipping-information"><li>
Free shipping on orders over USD180 
</li><li>
Free Returns
</li><li>
Secure Checkout
</li></ul>

                <div class="global-footer__cookie-info">
                    <div class="article-page">
<p>We use cookies &amp; hope that's ok with you.</p>
<p><a title="Privacy &amp; Cookies" href="../../../../../../help--info/privacy--cookies/index.html">More about cookies</a></p>
</div>
                </div>
        </div>
    </div>
</footer>