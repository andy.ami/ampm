<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="keywords">
    <meta name="author" content="">
    <title>Bradleys Footwear</title>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery-2.1.0.mind91e.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery-2.1.0.min.js"></script>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.mind91e.css">
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pluginsd91e.js"></script>
    <link rel="shortcut icon" href="//swimscdnprod.azureedge.net/assets/images/swims.ico">
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/appd91e.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.validate.mind91e.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.validate.unobtrusive.mind91e.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ObjectUtilsd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Oxx/AjaxUtilsd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/starterkitd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/components/logind91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/components/Productd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/components/ProductDialogd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/components/SizeGuideDialogd91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/jquery.autocomplete.mind91e.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/HelpDialogd91e.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/angular.mind91e.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/angular-resource.mind91e.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/appd91e.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/native.history.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app/find.js"></script>
    <script type="text/javascript">
        Oxx.AjaxUtils.sitePath = '/';
        Oxx.AjaxUtils.language = 'en';
    </script>
    
    <script type="text/javascript" src="https://dl.episerver.net/12.2.2/epi-util/native.history.js"></script>
<script type="text/javascript" src="https://dl.episerver.net/12.2.2/epi-util/find.js"></script>
<script type="text/javascript">
if(FindApi){var api = new FindApi();api.setApplicationUrl('/');api.setServiceApiBaseUrl('/find_v2/');api.processEventFromCurrentUri();api.bindWindowEvents();api.bindAClickEvent();api.sendBufferedEvents();}
</script>
 
	
</head>
<body class="home">
	<script>
    dataLayer = [{
        'pageType': 'home',
        'market': 'US',
        'currencyCode': 'USD',
        'userId': ''
    }];

</script>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-W59RMR&gtm_auth=JIaymc1oaiuK8a8PywEVTA&gtm_preview=env-1&gtm_cookies_win=x"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl + '&gtm_auth=JIaymc1oaiuK8a8PywEVTA&gtm_preview=env-1&gtm_cookies_win=x'; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W59RMR');
    </script>
    <!-- End Google Tag Manager -->
	
	<nav class="mobile-header">
        <ul class="cf">
            <li>
                <button class="mobile-header-toggle" title="Toggle main menu visiblity">
                    <svg class="inline-icon-hamburger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <line x1="0" x2="24" y1="5.5" y2="5.5"></line>
                        <line x1="0" x2="24" y1="12.5" y2="12.5"></line>
                        <line x1="0" x2="24" y1="19.5" y2="19.5"></line>
                    </svg>
                </button>
            </li>
            <li>
                <a href="index.html" class="mobile-header-logo">
                    <img src="http://swimscdnprod.azureedge.net/assets/images/swims-logo.svg" alt="SWIMS logo">
                </a>
            </li>
            <li>
                <button class="mobile-shopping-cart-toggle" onclick="updateShoppingCart();" title="Toggle shopping cart visibility">
                    <svg class="inline-icon-basket" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M16,3.9 C16,2.3,14.7,1,13.1,1h-2.4C9,1,7.7,2.3,7.7,3.9v2.9L2,6.9v13.6c0,1.3,1.1,2.3,2.3,2.3h15.3c1.3,0,2.3-1.1,2.3-2.3V9.6 c0-1.3-1.1-2.3-2.3-2.3h-3.8l-0.1,3.6c0,1.6-1.3,2.9-2.9,2.9h-1.9c-1.6,0-2.9-1.3-2.9-2.9"></path>
                    </svg>
                    <span class="cart-number-of-items"></span>
                </button>
            </li>
        </ul>
    </nav>
	
	
<header class="global-header">
        
<a href="/" class="global-header-logo">
    <img src="//swimscdnprod.azureedge.net/assets/images/swims-logo.svg" alt="SWIMS" title="SWIMS">
</a>


<form action="/system/search/" class="mobile-search">
    <input type="search" name="q" id="mobile-search-input">
    <label for="mobile-search-input">Search</label>
    <button type="submit" title="Search"></button>
</form>

    <nav class="main-nav">
        <ul class="main-nav__first-level">
                <li>
    <a href="#" class=""><span class="link-text">Bradleys Footwear</span></a>
    </li>
    <li>
            <a href="" class="has-sub-nav "><span class="link-text">Men&#39;s</span></a>
            <ul class="main-nav__second-level ">
                        <li>
            <a href="" class="has-sub-nav "><span class="link-text">Footwear</span></a>
            <ul class="main-nav__third-level ">
                        <li>    <a href="#" class=""><span class="link-text">Loafers</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Sneakers</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Lace-ups</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Boots</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Waterproof</span></a>
</li>

            </ul>
        </li>
        <li>    <a href="<?php echo site_url('galoshes'); ?>" class=""><span class="link-text">Galoshes</span></a>
</li>
        <li>    <a href="#" class=""><span class="link-text">Outerwear</span></a>
</li>
       

            </ul>
    </li>
    </ul>
    </nav>

	<nav class="secondary-nav">
    <ul>
            <li class="login-link"><a href="/system/login/" id="login-register-link"><span class="link-text">Log in | Register</span></a></li>
			<li class="language-selector-link"><a href="#" id="language-selector-link"><span class="link-text">Indonesia (Rp)</span></a></li>
		<li class="mobile-language-selector">
            <div class="select">
                <select id="mobile-selector-country" title="select country">
					<option value="ID,IND" selected=selected>Indonesia (Rp)</option>
                    <option value="US,USA" >United States (USD)</option>
                </select>
            </div>
            <div class="error-message" id="mobile-language-selector-country-error">Could not change language</div>
        </li>
	</ul>
</nav>
<script>
    $("#mobile-selector-country").change(function () {
        var id = $("#mobile-selector-country option:selected").attr("value");
        var data = {
            market: id.split(",")[0],
            language: id.split(",")[1]
        };
        $.ajax({
            url: "/Language/SetLanguageAndMarket",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            processData: false,
            success: function (result) {
                if (!result) {
                    $("#mobile-language-selector-country-error").slideDown(500);
                } else {
                    $("#mobile-language-selector-country-error").hide();

                    var url = window.location.href.split('?')[0];
                    if (url.indexOf('?') > -1) {
                        url += "&showCountries=true";
                    } else {
                        url += "?showCountries=true";
                    }
                    window.location.href = url;
                }
            }
        });
    });
    $("#mobile-language-selector-country-error").hide();
</script>
    </header>
	
	<nav class="right-nav">
    <ul>
        <li><button class="search-trigger">Search</button></li>
        <li><button class="shopping-cart-trigger" onclick="updateShoppingCart('en');">The Bag<span class="cart-number-of-items"></span></button></li>
    </ul>
</nav>




    <div class="login-drawer">
    


<button class="login-drawer__close"><span class="visually-hidden">Close login window</span></button>
<div class="login-drawer__login-form">
    <h3 class="login-drawer__heading">Log in to your account</h3>
    <form action="https://www.swims.com/Account/Login" id="login-form" method="post" data-redirect="/my-page/">
        <label class="text-input-label" for="login-form-email">E-mail</label>
        <input type="text" class="text-input" id="login-form-email">
        <label class="text-input-label" for="login-form-password">Password</label>
        <input type="password" class="text-input" id="login-form-password">
        <a href class="forgot-password-link">Forgot password?</a>
        <input class="checkbox" type="checkbox" id="login-form-remember-me">
        <label for="login-form-remember-me">Remember me</label>
        <p class="error-message" id="login-form-error"></p>
        <input type="submit" class="form-submit" value="Log in" id="login-form-submit">
    </form>
    <script type="text/javascript">
            $('#login-form').submit(function (event) {
                event.preventDefault();
                var data = {
                    username: $('#login-form-email').val(),
                    password: $('#login-form-password').val(),
                    rememberMe: $('#login-form-remember-me').prop('checked')
                };
                $.ajax({
                    url: this.action,
                    type: this.method,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                    processData: false,
                    success: function (result) {
                        if (result) {
                            var redirect = $('#login-form').data('redirect');
                            window.location.replace(redirect);
                        } else {
                            $('#login-form-error').html("Wrong username or password");
                            $('#login-form-error').slideDown(500);
                        }
                    }
                });
            });
            $('#login-form-error').hide();
    </script>
    <h3 class="login-drawer__heading">Don't have an account yet?</h3>
    <button class="register-form-open">Register an account</button>
</div>
    <div class="login-drawer__password-recovery-form">
    <a href="#" class="password-recovery-form-close">Back to Log in</a>
    <h3 class="login-drawer__heading">Forgot password?</h3>
    <p>Forgot your password? Don't worry! Fill in your e-mail address and we'll send you a new one.</p>
    <form action="https://www.swims.com/Account/RecoverPassword" id="password-recovery-form" method="post">
        <label class="text-input-label" for="password-recovery-form-email">E-mail</label>
        <input type="text" class="text-input" id="password-recovery-form-email">
        <p class="error-message" id="password-recovery-form-error"></p>
        <p class="confirmation-message" id="password-recovery-form-confirmation"></p>
        <input type="submit" class="form-submit" value="Reset password" id="password-recovery-submit">
    </form>
    <script type="text/javascript">
            $('#password-recovery-form').submit(function (event) {
                event.preventDefault();
                var data = { email: $('#password-recovery-form-email').val() };
                $.ajax({
                    url: this.action,
                    type: this.method,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                    processData: false,
                    success: function (result) {
                        if (result) {
                            $('#password-recovery-form-error').hide();
                            $('#password-recovery-form-confirmation').html("If your e-mail is registered, then we have sent you an e-mail with instructions on how to recover your password.");
                            $('#password-recovery-form-confirmation').slideDown(500);
                        } else {
                            $('#password-recovery-form-confirmation').hide();
                            $('#password-recovery-form-error').html("Could not recover password");
                            $('#password-recovery-form-error').slideDown(500);
                        }
                    }
                });
            });
            $('#password-recovery-form-confirmation').hide();
            $('#password-recovery-form-error').hide();
    </script>
</div>
    


<div class="login-drawer__register-form">
    <a href="#" class="register-form-close">Back to Log in</a>
    <h3 class="login-drawer__heading">Register an account</h3>
    <form action="https://www.swims.com/Account/Register" method="post" id="register-form">
        <label class="text-input-label" for="register-form-email">E-mail</label>
        <input type="text" class="text-input" id="register-form-email">
        <label class="text-input-label" for="register-form-confirm-email">Repeat e-mail</label>
        <input type="text" class="text-input confirm-email" id="register-form-confirm-email">
        <label class="text-input-label" for="register-form-password">Password</label>
        <input type="password" class="text-input" id="register-form-password">
        <input class="checkbox" type="checkbox" id="register-form-newsletter">
        <label for="register-form-newsletter"><b>Yes</b>, sign me up for the SWIMS newsletter.</label>
        <input class="checkbox" type="checkbox" id="register-form-terms">
        <label for="register-form-terms"><b>Yes</b>, I accept the <a href="help--info/terms-conditions/index.html" target="_blank">terms and conditions</a></label>
        <p class="error-message" id="register-form-error"></p>
        <p class="confirmation-message" id="register-form-confirmation"></p>
        <input type="submit" class="form-submit" value="Register" id="register-form-submit">
    </form>

    <script type="text/javascript">
        $('#register-form').submit(function (event) {
            event.preventDefault();
            var data = {
                email: $('#register-form-email').val(),
                repeatemail: $('#register-form-confirm-email').val(),
                password: $('#register-form-password').val(),
                repeatPassword: $('#register-form-confirm-password').val(),
                subscribeToNewsletter: $('#register-form-newsletter').prop('checked'),
                acceptTerms: $('#register-form-terms').prop('checked')
            };
            $.ajax({
                url: this.action,
                type: this.method,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                processData: false,
                success: function(result) {
                    if (result === true) {
                        $('#register-form-error').hide();
                        $('#register-form-confirmation').html(" The account was registered");
                        $('#register-form-confirmation').slideDown(500);
                    } else {
                        if (result === 20)
                            $('#register-form-error').html("The e-mail is not valid");
                        else if (result === 21)
                            $('#register-form-error').html("E-mail and repeat e-mail are not the same");
                        else if (result === 23)
                            $('#register-form-error').html("Password is too weak");
                        else if (result === 27)
                            $('#register-form-error').html("The Terms and Conditions are not accepted");
                        else if (result === 28)
                            $('#register-form-error').html("Something went wrong! If you already have an account we have sent you an e-mail with instructions on how to reset your password.");
                        else
                            $('#register-form-error').html("Could not register this account. Is the form filled in correctly?");
                        $('#register-form-confirmation').hide();
                        $('#register-form-error').slideDown(500);
                    }

                }
            });
        });
        $('#register-form-error').hide();
        $('#register-form-confirmation').hide();
    </script>
</div>
</div>

<script>
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    $(document).ready(function () {
        var showCountries = getUrlVars()["showCountries"];
        if (showCountries === "true") {
            $("#language-selector").show();
        }
    });
</script>

<div class="language-selector" id="language-selector">
    <h3 class="language-selector__heading">Choose your shipping destination</h3>
    <label for="language-selector-country" class="select-label">Country</label>
    <select id="language-selector-country" class="select">
			<option value="US,USA" >United States (USD)</option>
            <option value="ID,IND" selected=selected>Indonesia (Rp)</option>
    </select>
    <button type="button" class="language-selector__close"><span class="visually-hidden">Close language selection window</span></button>
    <p class="error-message" id="language-selector-country-error">Could not change language</p>
</div>

<script>
    $("#language-selector-country").change(function () {
        var id = $("#language-selector-country option:selected").attr("value");
        var data = {
            market: id.split(",")[0],
            language: id.split(",")[1]
        };
        $.ajax({
            url: "/Language/SetLanguageAndMarket",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            processData: false,
            success: function (result) {
                if (!result) {
                    $("#language-selector-country-error").slideDown(500);
                } else {
                    $("#language-selector-country-error").hide();
                    var newText = $("#language-selector-country option:selected").text();
                    $("#language-selector-link > .link-text").text(newText);

                    var url = window.location.href.split('?')[0];
                    if (url.indexOf('?') > -1) {
                        url += "&showCountries=true";
                    } else {
                        url += "?showCountries=true";
                    }
                    window.location.href = url;
                }
            }
        });
    });
    $("#language-selector-country-error").hide();
</script>
	<?php
    //    $this->load->view('header');
    ?>
	<?php
       //$this->load->view('home_page');
    ?>
	
	<div id="content">
		<?php
        if($content != ''):
            $this->load->view($content);
        endif;
    ?>
	</div>
	
	<?php
        $this->load->view('footer');
    ?>
</body>
</html>