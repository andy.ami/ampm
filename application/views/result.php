<div style="position:relative; width:80%; height:400;">
	<?php
		foreach($query->result() as $rsl) {
			//$ft = $rsl->fo
	?>
	<div class="test" style="width:30%; float:left; margin:0 5%;">
    	<a class="product  ng-scope product--vertical-image product-grid-fx-0 is-on-screen" ng-class="{true:'', false:'product--vertical-image'}[product.HorizontalImages]" ng-href="/swimscatalog/man/footwear/Galoshes/classic/classic-brown/" ng-repeat="product in products" ng-click="productClick(product)" href="<?php echo site_url('galoshes/detail_produk/'.$rsl->warna); ?>">
            <div class="product__image">
                <img ng-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=500&amp;transform=fit" alt="Classic Galosh" 
				src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>?Width=500&amp;transform=fit">
                <div class="product__hover-image">
                    <img ng-src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>" alt="Classic Galosh" 
					src="<?php echo base_url(); ?>assets/images/produk/<?php echo $rsl->foto; ?>">
                </div>
            </div>
            <div class="product__description">
                <p class="product__name ng-binding"><?php echo $rsl->nama; ?></p>
                <p class="product__color ng-binding"><?php echo $rsl->warna; ?></p>
                
                <p class="product__price ng-binding" ng-hide="product.DiscountedPrice">
                    <?php echo number_format($rsl->harga); ?>
                </p>
            </div>
        </a>
    </div>
		<?php } ?>
</div>