﻿/* jshint -W099 */
/* global jQuery:false */
/* global angular:false */
(function ($, productApp) {

    "use strict";

    productApp.controller('ProductAndSearchController', ['$scope', '$location', '$window', 'productService', 'articleService', 'gtmService',
    function ($scope, $location, $window, productService, articleService, gtmService) {
        $scope.selectedProductCategories = [];
        $scope.selectedProducts = [];
        $scope.SelectedTerms = [];
        $scope.page = 1;
        $scope.articlePageNumber = 1;
        $scope.showMore = false;
        $scope.showMoreArticles = false;
        $scope.nrOfColumns = 3;
        $scope.pageSizeAtFiveColumns = 100;
        $scope.ShowCategoriesAsCheckboxes = false;
        $scope.ShowMainCategoryFacets = false;
        $scope.SearchDone = false;
        $scope.SearchPage = false;
        $scope.SelectedTerm = '';

        $scope.init = function (preSelectedCategory, preSelectedProduct, language, pageSize, currency) {
            $scope.language = language;
            $scope.selectedProductCategories = preSelectedCategory ? preSelectedCategory.split(",") : $scope.selectedProductCategories = [];
            $scope.selectedProducts = preSelectedProduct ? preSelectedProduct.split(",") : $scope.selectedProducts = [];
            $scope.productPageSize = pageSize;
            $scope.pageSizeAtThreeColumns = pageSize;
            $scope.currency = currency;
            var list = "";
            var url = window.location.pathname;
            if (url) {
                var urlSplit = url.split("combed");
                for (var i = 0; i < urlSplit.length; i++) {
                    if (urlSplit[i].length > 2) {
                        list = list + urlSplit[i] + " ";
                    }
                }
                $scope.list = list.trim();
            }
            updateWithData();
        };

        $scope.initSearch = function (language, productPageSize, articlePageSize, searchedQuery, currency) {
            $scope.SearchPage = true;
            $scope.language = language;
            $scope.productPageSize = productPageSize;
            $scope.articlePageSize = articlePageSize;
            $scope.pageSizeAtThreeColumns = productPageSize;
            $scope.currency = currency;
            $scope.list = "regular search";
            if (searchedQuery) {
                $scope.queryTerm = searchedQuery;
                $scope.search();
            }
        };

        $scope.search = function () {
            setProductdata();
            $scope.loadProductData();
            $scope.loadArticles();
            $scope.SearchDone = true;
        };

        $scope.loadArticles = function () {
            $scope.showLoader();
            articleService.get($scope.queryTerm, $scope.language, $scope.articlePageNumber, $scope.articlePageSize).then(
                function (data) {
                    $scope.hideLoader();
                    if ($scope.articlePageNumber === 1) {
                        $scope.articles = data.articles;
                    } else {
                        $scope.articles.push.apply($scope.articles, data.articles);
                    }
                    $scope.articlesTotalResult = data.totalResult;
                    $scope.showMoreArticles = data.totalResult > $scope.articles.length;
                }
           );
        };

        $scope.loadProductData = function () {
            $scope.showLoader();
            productService.get($scope.productData, $scope.language, $scope.page, $scope.productPageSize).then(
			  function (data) {
			      $scope.hideLoader();
			      if ($scope.page === 1) {
			          $scope.products = data.products;
			      } else {
			          $scope.products.push.apply($scope.products, data.products);
			      }
			      $scope.totalResult = data.totalResult;
			      $scope.showMore = data.totalResult > $scope.products.length;
			      $scope.productCategories = data.productCategoryFacets;
			      $scope.facetColor = data.facetColor;
			      $scope.facetStyle = data.facetStyle;
			      
			      gtmService.trackList($scope.currency, $scope.products, $scope.list);
			  },
			  function (statusCode) {
			      $scope.hideLoader();
			      $scope.status = statusCode;
			  }
		  );
        };

        $scope.showLoader = function () {
            $scope.loaderVisible = true;
        };

        $scope.hideLoader = function () {
            $scope.loaderVisible = false;
        };

        function updateWithData() {
            $scope.page = 1;
            setProductdata();
            $scope.loadProductData();
        }

        function setNewProductDataAndUpdate() {
            $scope.page = 1;
            setProductdata();
            $scope.loadProductData();
        }

        function setProductdata() {
            $scope.productData = {
                SearchTerm: $scope.queryTerm,
                SelectedProductCategories: $scope.selectedProductCategories,
                SelectedProducts: $scope.selectedProducts,
                FacetColor: $scope.facetColor,
                FacetStyle: $scope.facetStyle,
            };
        }

        $scope.LoadMore = function () {
            $scope.page++;
            $scope.loadProductData();
        };

        $scope.LoadMoreArticles = function () {
            $scope.articlePageNumber++;
            $scope.loadArticles();
        };

        $scope.updateStringFacetsSelections = function (facet, facetType) {
            if ($scope.facetColor.Name === facetType) {
                $scope.facetColor.SelectedTerms = [];
                $scope.facetColor.SelectedTerms.push(facet);
            };
            if ($scope.facetStyle.Name === facetType) {
                $scope.facetStyle.SelectedTerms = [];
                $scope.facetStyle.SelectedTerms.push(facet);
            };
            setNewProductDataAndUpdate();
        };

        $scope.updateViewNrOfColumns = function ($event) {
            var $selector = $($event.currentTarget),
				mode = $selector.data('mode');
            if (mode !== $scope.nrOfColumns) {
                $scope.nrOfColumns = mode;
                $scope.productPageSize = mode === 5 ? $scope.pageSizeAtFiveColumns : $scope.pageSizeAtThreeColumns;
                if ($scope.totalResult > $scope.productPageSize) {
                    $scope.page = 1;
                    $scope.loadProductData();
                }
                else if ($scope.products.length < $scope.productPageSize) {
                    $scope.page = 1;
                    $scope.loadProductData();
                }
            }
        };

        $scope.checkAll = function (facetListToUpdate) {
            facetListToUpdate.length = 0;
            setNewProductDataAndUpdate();
        };

        $scope.productClick = function (clickedProduct) {
            var index = 0;
            //var index = $scope.products.findIndex(x => x.Code === clickedProduct.Code &&
            //    x.Color === clickedProduct.Color);
            //if (!index) {
            //    index = 0;
            //}
            gtmService.trackClickProduct(clickedProduct, $scope.list, index+1);
        };
    }]);
})(jQuery, window.productApp);

function updateFacet(element) {
    var selectedTerm = $(element).val();
    var facetType = $(element).data("facettype");
    if (selectedTerm) {
        var scope = angular.element(
            document.getElementById("productAndSearchController")).scope();
        scope.updateStringFacetsSelections(selectedTerm, facetType);
    }
}