
// require([
// 	"jquery",
// 	"scrollMagic",
// 	"scrollMagicGsap",
// 	"matchMedia",
// 	"chosen",
// 	"gsap",
// 	"gsapScrollTo",
// 	"scrollLock",
// 	"slick"
// ], function($, ScrollMagic){
(function() {

var	isDesktop = window.matchMedia("(min-width: 992px)").matches,
	isTouchDevice = $("html").hasClass("touchevents"),
	supportsMutationObservers = (typeof(window.MutationObserver||window.WebKitMutationObserver)!=="undefined"),
	initScrollPos = $(window).scrollTop(),
	$body = $("body"),
	$header = $(".global-header"),
	$footer = $(".global-footer"),
	$cart = $(".shopping-cart"),
	$search = $(".search"),
	$page = $(".page"),
	$languageSelector = $(".language-selector"),
	$loginDrawer = $(".login-drawer"),
	$loginForm = $(".login-drawer__login-form"),
	$registerForm = $(".login-drawer__register-form"),
	$pwForm = $(".login-drawer__password-recovery-form"),
	allDialogs = [],
	allAccordions = [],
	historyCount = history.length;






// Check for input type and set event handlers accordingly
var touchOrClick = "touchstart";
if (!isTouchDevice) { touchOrClick = "click"; }

var scrollOrTouch = "touchmove";
if (!isTouchDevice) { scrollOrTouch = "scroll"; }
if (navigator.userAgent.match('CriOS')) { scrollOrTouch = "scroll"; } // set to scroll for iOS chrome 

var touchOrHoverIn = "touchstart";
if (!isTouchDevice) { touchOrHoverIn = "mouseenter"; }

var touchOrHoverOut = "touchend";
if (!isTouchDevice) { touchOrHoverOut = "mouseleave"; }






// Throttle function to limit scroll and resize event handler function call rate
function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250); // jshint ignore:line
  var last,
      deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date(),
        args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}










// Debounce function to limit scroll and resize event handler function call rate
function debounce(func, wait, immediate) {
	wait || (wait = 250); //jshint ignore:line
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}












// jQuery extend stuff
jQuery.extend( jQuery.easing,
{
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	}
});

// Overwrite jQuery slideToggle
jQuery.fn.slideToggle = function() {
	var $el = $(this),
		dur = 0.8;

	if ($el.is(":visible")) {
		// Slide up (hide)
		$el.slideUp();
	} else if ($el.is(":hidden") || TweenMax.isTweening($el)) {
		// Slide down (show)
		$el.slideDown();
	}

	return;
};

// Overwrite jQuery slideUp
// callback, callback function to run after animation
jQuery.fn.slideUp = function(callback) {
	var $el = $(this),
		dur = 0.8;

	$el.css({"overflow": "hidden", "box-sizing": "content-box"});
	TweenLite.to($el, dur, {height: 0, paddingTop: 0, paddingBottom: 0, ease: Power4.easeOut, onComplete: function(){
		$el.attr("style", "").hide();
		if (callback !== undefined) { callback(); }
	}});

	return;
};

// Overwrite jQuery slideDown
// options, object of options
// options.callback, callback function to run after animation
// options.durationType, string, set this to "relative" in order to set animation duration accordin to element height
// options.duration, float, animation duration in seconds
jQuery.fn.slideDown = function(options) {
	var callback, duration, durationType;

	if (typeof options != "undefined") {
		if (typeof options.callback != "undefined") 	{ callback = options.callback; }
		if (typeof options.durationType != "undefined") { durationType = options.durationType; }
		if (typeof options.duration != "undefined") 	{ duration = options.duration; }
	} else {
		duration = 0.8;
		durationType = "";
	}

	var $el = $(this),
		visible = $(this).is(":visible"),
		height = 0; // initial height

	// Set duration relative to element height
	if (durationType === "relative") {
		var finalH = $el.show().height();
		if (!visible) { $el.hide(); }
		duration = Math.max(finalH / 500, 0.8);
	}

	// If element is already animating, set initial height to its current height
	if (TweenMax.isTweening($el)) { height = $el.height(); }

	// Don't do animation if already visible, to prevent staggering
	if (!visible || TweenMax.isTweening($el)) {
		$el.css({"overflow": "hidden", "box-sizing": "content-box"}).show();
		TweenLite.set($el, {height: "auto"});
		TweenLite.from($el, duration, {height: height, paddingTop: 0, paddingBottom: 0, ease: Power4.easeOut, onComplete: function(){
			$el.attr("style", "").show();
			if (callback !== undefined) { callback(); }
		}});
	}

	return;
};


// Removes elements from selection if they are below the current viewport (including scroll position)
// inverted: boolean, if set to true will return the opposite selection
jQuery.fn.filterVisible = function(inverted) {
	inverted = typeof inverted !== 'undefined' ?  inverted : false;
	var screenTop = $(window).innerHeight() + $(window).scrollTop();

	var $filtered = $(this).filter(function(index){
		if ($(this).offset().top > screenTop) {
			if (!inverted) { return true; }
		} else {
			if (inverted) { return true; }
		}
	});

	return $filtered;
};


// Utility method to run the scrollReveal function on a jQuery object.
// Options documented on the scrollReveal function.
jQuery.fn.scrollReveal = function(options) {
	scrollReveal($(this), options);
};


// Ajax loader indicator. Apply to container
// off, string, set this to "off" to remove the loader
// Example: $(".my-container").loader(); will show the indicator
// $(".my-container").loader("off"); will remove the indicator from the container
jQuery.fn.loader = function(off) {
	var imgUrl = $(".global-header-logo img").first().attr("src"),
		disable = false,
		isList = $(this).prop("tagName").toUpperCase() === "UL",
		markup = '<div class="swims-loader" style="display: none;"><img src="' + imgUrl + '" alt=""></div>',
		$loader;

	if (typeof off !== "undefined") { disable = off === "off"; }

	if (disable) {
		// Find loader
		if (isList) { $loader = $(this).siblings(".swims-loader"); }
		else 		{ $loader = $(this).find(".swims-loader"); }

		// Remove loader
		$loader.remove();
	} else {
		// Insert loader
		if (isList) {
			$(this).before(markup);
			$loader = $(this).siblings(".swims-loader");
		}
		else {
			$(this).prepend(markup);
			$loader = $(this).find(".swims-loader");
		}

		// Delay showing the loader, to avoid showing it when the content loads very fast
		setTimeout(function(){
			$loader.show();
		}, 300);
	}
};

// Returns css transition duration for the first match in the selection
jQuery.fn.transitionDur = function() {
	return parseFloat($(this).first().css("transition-duration").replace("s", ""));
};











// Toggles visibility of two blocks within the same container, with animation
// $hideBlock, jQuery object to hide
// $showBlock, jQuery object to show
// $container, jQuery object of container element
function toggleContent($hideBlock, $showBlock, $container) {
	// Get final height
	$showBlock.show();
	$hideBlock.hide();
	var height = $container.height();
	$showBlock.hide();
	$hideBlock.show();

	$container.css("overflow", "hidden");

	// Animate
	$hideBlock.fadeOut("fast");
	TweenLite.to($container, 0.4, {height: height, ease: Power3.easeOut, onComplete: function(){
		$showBlock.attr("tabindex", -1).fadeIn("fast").focus();
		$container.attr("style", "");
	}});
}











// Init scrollMagic controller if not a touch device
if (!isTouchDevice) {
	var controller = new ScrollMagic.Controller({refreshInterval: 50});
	controller.enabled(true);
}


// Scroll reveal effect
var srCount = 0; // used to sed ID's on scroll reveal elements


// Adds a fade in animation for elements as they enter the screen.
// Can also be used with jquery directly ($(".my-element").scrollReveal();)
// $selection, jquery object of elements to fade in on scroll
// options, object of options to override default settings.
// 		options.y, integer, number of pixels to slide the element vertically
//		options.duration, number, animation duration in seconds
//		options.offset, number, multiple of screen height, set to make elements fade in sooner or later, depending on scroll positoin
//		options.delay, number, seconds to delay the animation by
//		options.trigger, string formatted as jquery selector, used to provide a different scrollmagic target than the element itself. 
//		options.stagger, boolean, if set to true the elements in the selection will animate one by one instead of simultaneously
//		options.staggerDelay, number, number of seconds of delay to use as delay between each animation of the selection
function scrollReveal($selection, options) {
	if (!isTouchDevice) {
		// Default values
		var y 		 = 50,	// Number of pixels to slide the element vertically
			duration = 2,   // Animation duration (seconds)
			offset 	 = 0,   // Multiple of screen height
			delay 	 = 0,   // Animation delay (seconds)
			selector = $selection.selector,
			hasTrigger = false, // indicates whether a custom trigger has been added
			stagger = false,	// set delay between animations
			staggerDelay = 0.1; // amount to delay each animation by

		// Parameter values
		if (options) {
			if (options.y) 		 	{ y = options.y; }
			if (options.duration) 	{ duration = options.duration; }
			if (options.offset) 	{ offset = options.offset; }
			if (options.delay) 	 	{ delay = options.delay; }
			if (options.trigger)  	{ hasTrigger = true; }
			if (options.stagger) 	{ stagger = true; }
			if (options.staggerDelay) { staggerDelay = options.staggerDelay; }
		}

		var $filteredSelection = $selection.filterVisible();

		$filteredSelection.addClass("scroll-reveal");

		for (var i=0, len=$filteredSelection.length; i<len; i++) {
			var id = "scroll-reveal-" + srCount, // create unique class name
				trigger,
				animEl,
				$this = $filteredSelection.eq(i);

			srCount++;
			
			// Set trigger element and animation target selector strings
			if (hasTrigger) {
				$this.closest(options.trigger).addClass(id);
				animEl = "." + id + selector;
			} else {
				$this.addClass(id);
				animEl = "." + id;
			}
			
			// Set trigger selector string
			trigger = "." + id;

			// Add initial style
			TweenLite.set(animEl, {y: y, opacity: 0});

			// Add animation delay if stagger is selected
			if (stagger) { delay = delay + staggerDelay*i; }

			// Animate
			new ScrollMagic.Scene({triggerElement: trigger, triggerHook: "onEnter", duration: 0, offset: offset * $(window).innerHeight()})
				.setTween(animEl, duration, {y: "0", opacity: 1, delay: delay, ease: Power4.easeOut})
				.addTo(controller)
				.reverse(false);
		}
	}
}













var ariaTogglerCount = 0;

// Sets the correct ARIA state for one or more triggers that are used to show/hide content. This function does NOT show or hide the content itself.
// $triggers, jquery object of one or more triggers that hide/show the content
// $content, jquery object of the content to show/hide
function ariaToggler($triggers, $content) {
	var thisToggler = this;

	this.expanded = $content.is(":visible");
	this.toggle = function(){
					if (this.expanded) {
						this.off();
					} else {
						this.on();
					}
				};
	this.on 	= function(){
					this.expanded = true;
					$triggers.attr("aria-expanded", true);
				};
	this.off 	= function(){
					this.expanded = false;
					$triggers.attr("aria-expanded", false);
				};

	ariaTogglerCount++;

	// Get or set ID
	var id = $content.attr("id");
	if (typeof id == "undefined") {
		id = "aria-toggle-" + ariaTogglerCount;
		$content.attr("id", id);
	}

	// Init
	$triggers.attr({"aria-controls": id, "aria-expanded": this.expanded});
}
















// Create a dialog object. The dialog object manages closed / opened state of the dialog, open/close animations and accessibility.
// $dialog, jquery object of the dialog
// $trigger, jquery object of the trigger (the element that opens the dialog)
// $closeBtn, jquery object of the close button inside of the dialog
// openFunc, function that shows / animates the dialog in
// closeFunc, function that hides / animates the dialog out

function dialog($dialog, $trigger, $closeBtn, openFunc, closeFunc) {
	// Create object
	var thisDialog = this;
	this.element   = $dialog;
	this.trigger   = $trigger;
	this.closeBtn  = $closeBtn;
	this.isOpen    = false;
	this.open      = function(){
						// Close other, already open dialogs
						for (var i=0, len=allDialogs.length; i < len; i++) {
							if (allDialogs[i].isOpen) {
								allDialogs[i].close();
							}
						}
						// Open this dialog and set trigger states
						thisDialog.isOpen = true;
						openFunc();
						$trigger.add($closeBtn).attr("aria-expanded", "true");
						setTimeout(function() { $dialog.focus(); }, 600);
						$tabbable.first().on("blur", {tabbable: $tabbable, container: $dialog}, trapFirst);
						$tabbable.last().on("blur", {tabbable: $tabbable, container: $dialog}, trapLast);
					};
	this.close     = function(){
						thisDialog.isOpen = false;
						closeFunc();
						$trigger.add($closeBtn).attr("aria-expanded", "false");
						$tabbable.first().off("blur", trapFirst);
						$tabbable.last().off("blur", trapLast);
					};

	// Set ID on dialog if undefined
	if (typeof $dialog.attr("id") === "undefined") {
		$dialog.attr("id", "dialog-" + allDialogs.length);
	}

	// Add ARIA-expanded attribute
	$trigger.add($closeBtn).attr({"aria-expanded": "false", "aria-controls": $dialog.attr("id")});

	// Make dialog focusable
	$dialog.attr("tabindex", -1);

	// Stop click propagation
	$dialog.on("click", function(e){
		e.stopPropagation();
	});

	// Get first and last tabbable element in the dialog. Used to lock keyboard focus within dialog
	$dialog.show();
	var $tabbable = $dialog.find("a, button, input").filter(function(){
		// Filter out any un-tabbable elements
		if ($(this).attr("tabindex") !== "-1") { return true; }
	}).filter(":visible").filter(":first, :last");
	$dialog.hide();

	// Dialog open on trigger click
	$trigger.on("click", function(e){
		if (thisDialog.isOpen) {
			thisDialog.close();
		} else {
			thisDialog.open();
		}
	});

	// Dialog close on body or header click
	$body.add($header).on("click", function(){
		if (thisDialog.isOpen) {
			thisDialog.close();
		}
	});

	// Dialog close on close button click or escape key press
	$closeBtn.on("click keyup", function(event){
		if (event.type == "click" || event.keyCode == 27) {
			thisDialog.close();
		}
	});

	// Focus to triger on close button click
	$closeBtn.on("click", function(){
		$trigger.focus();
	});

	// Focus to trigger on escape press
	$dialog.on("keyup", function(){
		if (event.keyCode == 27) {
			$trigger.focus();
		}
	});
}

// Set focus to last tabbable element if focus is removed from container via the first tabbable element. Parameters are passed as jquery event data
// container, jquery object of the container to keep keyboard focus within
// tabbable, jquery object of the first and last tabbable elements within the container
function trapFirst(event) {
	var $container = event.data.container,
		$tabbable = event.data.tabbable;

	setTimeout(function() {
		if ($container.has($(document.activeElement)).length <= 0 && !$body.is(".mouse-input")) {
			$tabbable[1].focus();
		}
	}, 20);
}

// Set focus to first tabbable element if focus is removed from container via the last tabbable element. Parameters are passed as jquery event data
// container, jquery object of the container to keep keyboard focus within
// tabbable, jquery object of the first and last tabbable elements within the container
function trapLast(event) {
	var $container = event.data.container,
		$tabbable = event.data.tabbable;

	setTimeout(function() {
		if ($container.has($(document.activeElement)).length <= 0 && !$body.is(".mouse-input")) {
			$tabbable[0].focus();
		}
	}, 20);
}









// Create warning popup. Accepts two strings, used as heading and main text of the popup
warningBox = function(text, heading) {
	heading = typeof  heading !== "undefined" ? heading : "Info";
	$body.prepend(
		'<div class="warning-box"><h4>' + heading + '</h4> <p>'  + text + '</p> <button class="warning-box__close"><span class="visually-hidden">Close message</span></button> </div>'
	);

	$box = $(".warning-box").first();
	TweenMax.from($box, 1, {y: -200, ease: Power4.easeOut, onComplete: function(){
		$box.attr("style", "");
	}});
};

$("body").on("click", ".warning-box__close", function(){
	var $box = $(this).parents(".warning-box");

	TweenMax.to($box, 0.3, {y: -200, ease: Power3.easeIn, onComplete: function(){
		$box.remove();
	}});
});














// Read the rgb value of the top left pixel of an image
// $img, jquery object of ONE IMAGE to get the pixel data from
function getBgColor($img) {
	if ($("#bg-color-canvas").length === 0) {
		$("body").append("<canvas id='bg-color-canvas' style='display: none;'></canvas>");
	}

	var canvas = $("#bg-color-canvas")[0],
		context = canvas.getContext("2d"),
		img = new Image(),
		pixelVal,
		bgColor;

	img.src = $img.attr("src");

	context.drawImage(img, 0, 0);
	pixelVal = context.getImageData(0, 0, 1, 1).data;

	bgColor = "rgb(" + pixelVal[0] + ", " + pixelVal[1] + ", " +  pixelVal[2] + ")";
	return bgColor;
}













// Window resize stuff
var winPrevW = $(window).innerWidth();

// Throttled resize:
$(window).resize(throttle(function(){
	// Calculate resize amount
	var winW = $(window).innerWidth(),
		deltaX = winW - winPrevW;
	winPrevW = winW;

	// Reset isDesktop variable
	isDesktop = window.matchMedia("(min-width: 992px)").matches;
	
	// Only on horizontal resize (needed because iOS safari fires resize events on scroll)
	if (Math.abs(deltaX) > 0) {
		// reset all elements that have a scroll reveal effect applied
		$(".scroll-reveal").attr("style", "");

		// reset global header style
		$(".global-header").attr("style", "");

		// show media slideshow on launch article page
		$(".launch-article-page .media-slideshow").css("opacity", 1);


		// Breakpoint: 992px
		if (isDesktop) {

		} else {
			// close all dialogs
			$.each(allDialogs, function(index) { allDialogs[index].close(); });

			// Show all scroll reveal elements on launch article page
			$(".launch-article-page .media-block").find("h2, li, div").css({"opacity": 1, "transform": "none"});
		}

		// Breakpoint: 768px
		if (window.matchMedia("(min-width: 768px)").matches) {
			$(".tabs__content").attr("style", ""); // reset mobile tabs
			$.each(tabsObj.mobileActive, function(index) { tabsObj.mobileActive[index] = true; });
		} else {
			$(".product-images-container").attr("style", ""); // reset product image carousel
			$.each(tabsObj.mobileActive, function(index) { tabsObj.mobileActive[index] = false; });
		}
	}

	tabsObj.update();
}, 300));


// Debounced resize
var winPrevWDebounce = $(window).innerWidth();
$(window).resize(debounce(function(){
	// Calculate resize amount
	var winW = $(window).innerWidth(),
		deltaX = winW - winPrevWDebounce;

	winPrevWDebounce = winW;

	// Only on horizontal resize (needed because iOS safari fires resize events on scroll)
	if (Math.abs(deltaX) > 0) {
		// Reset media slideshow on resize
		if ($("html").hasClass("flexbox") && $(".media-slideshow").length) {
			mediaSlideshowInit();
			mediaSlideshowFX();
		}
	}
}, 300));









// Scroll stuff
var scrollEventPrev = $(window).scrollTop();

$(window).on(scrollOrTouch, throttle(function(){
	var scrollY = $(window).scrollTop(),
		delta = scrollY - scrollEventPrev;
	
	scrollEventPrev = scrollY;

	if (!isDesktop) {
		// hide / show mobile header
		if (scrollY > 100) {
			if (delta >  50) { hideMobileHeaderBar(); }
			if (delta < -50) { showMobileHeaderBar(); }
		} else {
			showMobileHeaderBar();
		}
	}

}, 300));











// Hide / show focus outline according to user input method
$body.on("mousedown", function(){
	$body.addClass("mouse-input");
});

$body.on("keyup", function(e){
	if (e.keyCode == 9) {
		$body.removeClass("mouse-input");
	}
});













//  a88888b.                                                                    d8                
// d8'   `88                                                                    88              d8
// 88        .d8888b. 88d8b.d8b. 88d888b. .d8888b. 88d888b. .d8888b. 88d888b. d8888P .d8888b.   88
// 88        88'  `88 88'`88'`88 88'  `88 88'  `88 88'  `88 88ooood8 88'  `88   88   Y8ooooo.     
// Y8.   .88 88.  .88 88  88  88 88.  .88 88.  .88 88    88 88.  ... 88    88   88         88   d8
//  Y88888P' `88888P' 8P  8P  8P 88Y888P' `88888P' 8P    8P `88888P' 8P    8P   8P   `88888P'   88
//                               88                                                               
//                               8P                                                               




// Skip-link

$page.attr("tabindex", -1);
$body.prepend("<button id='skip-to-main-content'>Skip to main content</button>");
$("#skip-to-main-content").on("click", function(){
	$page.focus();
});










// Print page button
$(".print-page-button").on("click", function(e){
	e.preventDefault();
	window.print();
});












// Add scroll animation to the "other colors" link on product pages
$(".product-sidebar__other-colors-link").on("click", function(e){
	e.preventDefault();

	var id = $(this).attr("href"),
		st = $(id).offset().top;

	TweenMax.to($(window), 1, {scrollTo: {y: st}, ease: Power2.easeInOut});
});












// Scroll to top button

// Add element if not present in markup
if ($("#scroll-to-top").length === 0) {
	$body.append("<button id='scroll-to-top'></button>");
}

var scrollHeightRatio = $(".page").height() / $(window).innerHeight(),
	$scrollToTop = $("#scroll-to-top"),
	scrollToTopShown = false;

// Show or hide the scroll to top button. The button is not shown on pages where the entire page is less than 3 times the height of the viewport.
if (scrollHeightRatio > 3 && !isDesktop) {
	$(window).on("touchmove scroll", debounce(function(){
		if ($(window).scrollTop() > 3 * $(window).innerHeight()) {
			if (!scrollToTopShown) {
				// Show button
				scrollToTopShown = true;
				TweenLite.set($scrollToTop, {scale: 0, display: "block"});
				TweenLite.to($scrollToTop, 1, {scale: 1, ease: Elastic.easeOut});
			}
		} else {
			// Hide button
			scrollToTopShown = false;
			TweenLite.to($scrollToTop, 1, {scale: 0, ease: Power3.easeOut});
		}
	}, 500));
}

$("#scroll-to-top").on("click", function(){
	TweenMax.to($(window), 1, {scrollTo: {y: 0}, ease: Power2.easeInOut, onComplete: function(){
		$page.focus();
	}});
});






// Scroll hint. Displays an element that indicates to the user that there is more content below

// Add scroll hint element if not present
if (! $(".scroll-hint").length) {
	$body.append("<div class='scroll-hint'>Scroll</div>");
}

var $scrollHint = $(".scroll-hint"),
	scrollHintTimer;

// Select pages to show scroll hint
var scrollHintPages = [
	".product-footwear",
	".product-clothing",
	".section-page",
	".launch-article-page",
	".category"
].join(", ");

// Show scroll hint on selected pages
if ($(scrollHintPages).length) {
	showScrollHint();

	$(window).on("scroll", cancelScrollHint);
}

// Show scroll hint after 3 seconds
function showScrollHint() {
	scrollHintTimer = setTimeout(function() {
		TweenLite.set($scrollHint, {display: "block"});
		TweenLite.to($scrollHint, 1, {scale: 1, ease: Elastic.easeOut});
	}, 3000);
}

// Hide scroll hint
function hideScrollHint() {
	TweenLite.to($scrollHint, 0.7, {scale: 0, ease: Power3.easeOut});
}

// Cancel scroll hint if the user has scrolled before it is shown, or hide if already visible
function cancelScrollHint() {
	if ( Math.abs($(window).scrollTop() - initScrollPos) > 0.5 * $(window).innerHeight()) {
		clearTimeout(scrollHintTimer);
		$(window).off("scroll", cancelScrollHint);
		hideScrollHint();
	}
}








// CHECKOUT

$("#checkout-login-button").on("click", function(e){
	e.preventDefault();
});

// Terms and conditions checkbox. This controls the disabled state of the submit button
$("#terms-and-conditions").on("change", function(){
	var checked = $(this).prop("checked");

	if (checked) { $(".checkout-nav__next").prop("disabled", false); }
	else 		 { $(".checkout-nav__next").prop("disabled", true); }
});

// Trigger change event on page load in case the clicked state was remembered
$("#terms-and-conditions").trigger("change");



// Checkout address

// get the data for the selected customer address from the #addressdata element, and display it to the user
function setSelectedValue(id) {
	var addressToDisplay = $("#addressdata p[id='" + $("#" + id).val() + "']").first();
	$("#" + id).parent().find(".checkout-address").html($(addressToDisplay).html());
}

// Update the displayed address when the customer selects an address from the address dropdown list
$('#billing-address-select, #shipping-address-select').on("change", function () {
	setSelectedValue($(this).attr("id"));
});

// Show the default selected address on page load
setSelectedValue("billing-address-select");
setSelectedValue("shipping-address-select");















// Add to cart button (product pages)
// Uses sessionStorage to determine whether the button has been clicked before. If not, the shopping cart sidebar is shown.
$(".button--add-to-cart").on("click", function(e){
	e.stopPropagation();
	var $button = $(this);
	
	if ($("#sizes").val()) {
		$button.addClass("is-checked");
		setTimeout(function() {
			$button.removeClass("is-checked");
		}, 2000);

		try {
			// Check if button has been clicked before
			if (sessionStorage.cartHintShown === undefined) {
				sessionStorage.cartHintShown = true;
				// Show shopping cart sidebar
				cartHintAnim();
			}
		} catch(err) {}
	}
});

// Show shopping cart sidebar if the user clicks the "add to cart" button for the first time
function cartHintAnim() {
	cartDialog.open();
	$cart.find(".shopping-cart__products").hide();

	setTimeout(function() {
		// Get cart content markup from API
		if (typeof updateShoppingCart !== "undefined") { updateShoppingCart(); }

		if ($(".shopping-cart__products li").length) {
			// Animate if product markup is present
			$cart.find(".shopping-cart__products").slideDown({duration: 1.2});
		} else if (supportsMutationObservers) {
			// Animate when new markup is injected
			var cartProductObserver = new MutationObserver(function(){
				if ($(".shopping-cart__products li").length) {
					$cart.find(".shopping-cart__products").hide();
					$cart.find(".shopping-cart__products").slideDown({duration: 1.2});

					cartProductObserver.disconnect();
				}
			});
			cartProductObserver.observe($(".shopping-cart__products").get(0), { childList: true });
		}
	}, 400);
}













// Add checkmark to wishlist button when clicked
$(".button--add-to-wishlist").on("click", function(){
	var $button = $(this);
	$button.addClass("is-checked");
});






// Add is-active class to label on focus
$(".text-input").on("focus", function(){
	$(this).prev("label").addClass("is-active");
});

// Toggle is-active state on label on blur and change
$(".text-input").on("blur change", function(){
	if ($(this).val() === "") {
		$(this).prev("label").removeClass("is-active");
	} else {
		$(this).prev("label").addClass("is-active");
	}

});

// Loop through all text inputs, set IDs if not set and add is-active class if the input has content
for (var i=0, len = $(".text-input").length; i < len; i++) {
	var $label = $(".text-input").eq(i).prev("label"),
		$input = $(".text-input").eq(i);

	// Set id if not set
	if (typeof $input.attr("id") === "undefined") {
		$input.attr("id", "text-input-" + i);
		$label.attr("for", "text-input-" + i);
	} else if (typeof $label.attr("id") === "undefined") {
		$label.attr("for", $input.attr("id"));
	}

	// Set active state if input has content
	if ($input.val() !== "") {
		$input.prev("label").addClass("is-active");
	}
}

// Check for input elements that are filled by browser autofill (like username / password) and add class to label
$(window).load(function() {
	var $autofilled;

	try {
		$autofilled = $(":autofill");
	} catch (error) { // jshint ignore: line
		try {
			$autofilled = $(":-webkit-autofill");
		} catch (error) { // jshint ignore: line
			try {
				$autofilled = $(":-moz-autofill");
			} catch (error) {} // jshint ignore: line
		}
	}

	if (typeof $autofilled !== "undefined") {
		$autofilled.prev("label").addClass("is-active");
	}
});



// Add password visibility toggle to all password input fields
$("input[type='password']").each(function(){
	$(this).add($(this).prev()).wrapAll("<div class='password-wrapper'></div>");
	$(this).after("<button type='button' class='password-visibility-toggle' title='Show password'></button>");
});

// Toggle password visibility state
$(".password-visibility-toggle").on("click", function(e){
	e.preventDefault();

	var title = $(this).attr("title") === "Show password" ? "Hide password" : "Show password",
		type  = $(this).siblings("input").attr("type") === "password" ? "text" : "password";

	$(this).closest(".password-wrapper").toggleClass("password-is-shown");
	$(this).attr("title", title);
	$(this).siblings("input").attr("type", type);
});

// Disable pasting in email verification fields
$(".confirm-email").on("paste", function(e){
	e.preventDefault();
});









// Create a tabs object that manages accessibility and active tabs on desktop and mobile.
// This component has separate sets of triggers and content wrappers for desktop and mobile.
// $triggers, jquery object of the tab triggers/buttons on desktop
// $mobileTriggers, jquery object of the triggers shown on mobile
// $content, jquery object of the content wrappers on desktop
// $mobileContent, jquery object of the content wrappers on mobile
function tabs($triggers, $mobileTriggers, $content, $mobileContent) {
	var thisTabs = this;
	
	thisTabs.activeID = 0; // index of the currently selected tab
	thisTabs.mobileActive = []; // on mobile, several tabs can be open at the same time, managed by this list
	thisTabs.update = function() {
		if (window.matchMedia("(min-width: 768px)").matches) {
			// Update desktop state
			$triggers.removeClass("is-active").attr("aria-expanded", false);
			$triggers.eq(thisTabs.activeID).addClass("is-active").attr("aria-expanded", true);
			
			$content.removeClass("is-active");
			$content.eq(thisTabs.activeID).addClass("is-active");

			// Set mobile trigger state
			$mobileTriggers.attr("aria-expanded", true);


		} else {
			// Set desktop trigger state
			$triggers.attr("aria-expanded", true);

			// Update mobile trigger and content state
			$.each(thisTabs.mobileActive, function(index){
				if (thisTabs.mobileActive[index]) {
					$mobileTriggers.eq(index).attr("aria-expanded", true).addClass("is-expanded");
					$mobileContent.eq(index).slideDown({durationType: "relative"});
				} else {
					$mobileTriggers.eq(index).attr("aria-expanded", false).removeClass("is-expanded");
					$mobileContent.eq(index).slideUp();
				}
			});
		}
	};


	// Init
	var $triggerContainer = $triggers.closest("ul"),
		$contentContainer = $content.closest("ul");

	// Trigger ARIA stuff
	$triggers.each(function(index){
		var id = $content.eq(index).attr("id"); // get ID from content container, if any
		if (id === undefined) {
			id = "tabs-id-" + index;
			$content.eq(index).attr("id", id);
		}

		$(this).attr({"aria-expanded": false, "aria-controls": id, "tabindex": 0});
	});

	// Mobile trigger ARIA stuff
	$mobileTriggers.each(function(index){
		var id = $mobileContent.eq(index).attr("id"); // get ID from content container, if any
		if (id === undefined) {
			id = "mobile-tabs-id-" + index;
			$mobileContent.eq(index).attr("id", id);
		}

		$(this).attr({"aria-expanded": false, "aria-controls": id, "tabindex": 0});
		thisTabs.mobileActive.push(false);
	});

	// Set initial ARIA state
	if (window.matchMedia("(min-width: 768px)").matches) {
		// Set all mobile triggers to expanded true on desktop
		$.each(thisTabs.mobileActive, function(index){ thisTabs.mobileActive[index] = true; });
		$mobileTriggers.attr("aria-expanded", true);

		// Set first desktop trigger to expanded true on desktop
		$triggers.first().attr("aria-expanded", true);
	} else {
		// Set all desktop triggers to expanded true on mobile
		$triggers.attr("aria-expanded", true);
	}


	// Events 
	$triggers.on("click", function(){
		thisTabs.activeID = $(this).index();
		thisTabs.update();

		try {
			sessionStorage.accountLastOpenTab = $(this).index();
		} catch(err) {}
	});

	$triggers.on("keypress", function(e){
		if (e.keyCode === 13) {
			thisTabs.activeID = $(this).index();
			thisTabs.update();
		}
	});

	$mobileTriggers.on("click", function(){
		var index = $(this).closest("li").index();

		thisTabs.mobileActive[index] = !thisTabs.mobileActive[index]; // toggle bool value
		thisTabs.update();
	});
}

// Init tabs
var tabsObj = new tabs($(".tabs__triggers li"), $(".tabs__mobile-trigger"), $(".tabs__content-container > li"), $(".tabs__content"));

// Open the last tab on the My account page on mobile
if (window.matchMedia("(max-width: 767px)").matches && $body.is(".account")) {
	tabsObj.mobileActive[2] = true;
	tabsObj.update();
} else {
	// Open the last open tab if defined in session storage
	var id = sessionStorage.accountLastOpenTab;
	if (typeof id !== "undefined" && id < $(".tabs__triggers > li").length) {
		tabsObj.activeID = id;
		tabsObj.update();
	}
}










// Init accordion. Shows / hides content with a sliding animation and sets correct aria state on trigger element.
// $trigger, jquery object for the button that shows/hides the content
// $content, jquery object of the content to show/hide
function accordion($trigger, $content) {
	// Create object
	this.expanded = $content.is(":visible");
	this.trigger  = $trigger;
	this.content  = $content;
	this.openTxt  = $trigger.text();
	this.closeTxt = $trigger.data("close-text");
	this.collapse = function () {
		$trigger.attr("aria-expanded", "false").removeClass("is-expanded");
		$content.removeClass("is-expanded").slideUp();

		if (typeof this.closeTxt !== "undefined") {
			$trigger.text(this.openTxt);
		}
	};
	this.expand = function () {
		$trigger.attr("aria-expanded", "true").addClass("is-expanded");
		$content.addClass("is-expanded").slideDown();

		if (typeof this.closeTxt !== "undefined") {
			$trigger.text(this.closeTxt);
		}
	};

	// Add to array of all accordions
	var thisAccordion = this;
	allAccordions.push(thisAccordion);

	// Set accessibility stuff
	var	id = "accordion-content-" + allAccordions.length;

	// Get ID from content container, if any
	if ($content.attr("id") !== undefined) {
		id = $content.attr("id");
	} else {
		// Set generated ID on content
		$content.attr("id", id);
	}

	// Set initial aria state
	$trigger.attr({"aria-expanded": thisAccordion.expanded, "aria-controls": id});


	// Event handling
	$trigger.on("click", function(e){
		e.preventDefault();
		
		if (thisAccordion.expanded) {
			thisAccordion.collapse();
		} else {
			thisAccordion.expand();
		}

		thisAccordion.expanded = !thisAccordion.expanded;
	});
}


// Init accordion instances

$(".accordion-trigger").not(".category .accordion-trigger").each(function(){
	new accordion($(this), $(this).siblings(".accordion-content"));
});


// Init category page accordion
var categoryAccordion;
$(".category .accordion-trigger").each(function(){
	categoryAccordion = new accordion($(this), $(this).siblings(".accordion-content"));
});

// Make category page intro text slide up and down together with the categoryAccordion, and scroll window to top when toggling the accordion
$(".category .accordion-trigger").on("click", function(){
	TweenLite.to($(window), 0.8, {scrollTo: {y: 0}, ease:Power3.easeOut});

	if (!isDesktop) {
		if (categoryAccordion.expanded) {
			$(".heading__intro").slideDown();
		} else {
			$(".heading__intro").slideUp();
		}
	} else {
		$(".heading__intro").show();
	}
});








// FORMS

// Stop click propagation of links within labels, to avoid setting focus to the corresponding input element
$("label a").on("click", function(e){
	e.stopPropagation();
});

// Show / hide the form group for shipping address depending on the checked state a checkbox input
$("#custom-shipping-address-trigger").on("change", function(){
	if ($(this).is(":checked")) {
		$("#shipping-address-form .form__content").slideUp();
		if ($("#shipping-address-form .form__edit-content").is(":visible")) {
			$("#shipping-address-form .cancel-edit-address").trigger("click");
		}
	} else {
		$("#shipping-address-form .form__content").slideDown();
	}
});


// add class to text inputs if input validation fails to provide visual styling
$(".field-validation-error").each(function(){
	$(this).prev().filter("input, select").addClass("validation-error");
});

// Show / hide the edit address form
$(".checkout-address-registered .checkout-edit-address").on("click", function(e){
	e.preventDefault();
	toggleContent(
		$(this).closest(".form__content"),
		$(this).closest(".form__content").siblings(".form__edit-content"),
		$(this).closest(".form")
	);
});

// Hide the edit address form
$(".checkout-address-registered .cancel-edit-address").on("click", function(e){
	e.preventDefault();
	toggleContent(
		$(this).closest(".form__edit-content"),
		$(this).closest(".form__edit-content").siblings(".form__content"),
		$(this).closest(".form")
	);
});



// Show the add address form
$(".checkout-address-registered .checkout-add-address").on("click", function(e){
	e.preventDefault();
	$("#checkout-add-address-form").slideDown();
	TweenLite.to($(window), 1, {scrollTo: {y: $("#checkout-add-address-form").offset().top}, ease: Power2.easeInOut, onComplete: function(){
		$("#checkout-add-address-form").attr("tabindex", -1).focus();
	}});
});

// Hide the add address form
$(".checkout-address-registered .cancel-add-address").on("click", function(e){
	e.preventDefault();
	$("#checkout-add-address-form").slideUp();
	TweenLite.to($(window), 1, {scrollTo: {y: 0}, ease: Power2.easeInOut, onComplete: function(){
		$(".checkout-add-address").focus();
	}});
});




// Trigger submit button click on enter press in forms
$("#billing-address-form .form__edit-content")
.add("#shipping-address-form .form__edit-content")
.add("#checkout-add-address-form")
.add("#checkout-login").each(function(e){
	$(this).children("input").on("keypress", function(e){
		var code = e.keyCode || e.which;

		if (code === 13) {
			e.preventDefault();
			e.stopPropagation();
			$(this).siblings(".edit-address-submit, .add-address-submit, #checkout-login-button").trigger("click");
		}
	});
});










// MOLECULE - Product image carousel (on top of product page)

$(".product-image-carousel__close").on("click", function(e){
	e.preventDefault();

	$(this).siblings(".product-images-container").trigger("click");
});


// Push history state when product image is zoomed
$(".product-images-container").on("click", function(){
	if (window.matchMedia("(min-width: 768px)").matches) {
		var $el = $(this);
		if (!$el.hasClass("is-animating")) {
			if ($el.hasClass("is-zoomed")) {
				zoomProductOut();
				$el.removeClass("is-zoomed");


				// Reset browser history to previous state because of pushState below
				if (history.length > historyCount) {
					history.back();
				}
			} else {
				zoomProductIn();
				$el.addClass("is-zoomed");


				// Add history state to allow users to close the zoom by clicking back in the browser
				history.pushState({}, "");

				// Zoom out on browser back click and remove popstate event handler
				$(window).on("popstate", function(){
					zoomProductOut();
					$el.removeClass("is-zoomed");
					$(window).off("popstate");
				});
			}
		}
	}
});

// Responsive images
$(".product-image-carousel").find(".product-image-large img, .product-thumbnail img").each(function(){
	if (typeof $(this).data("src") !== "undefined") {
		$(this).removeAttr("src").hide();

		if (window.matchMedia("(min-width: 768px)").matches || typeof $(this).data("small-src") == "undefined") {
			$(this).attr("src", $(this).data("src")).fadeIn("slow");
		} else {
			$(this).attr("src", $(this).data("small-src")).fadeIn("slow");
		}
	}
});

// Set container height after zooming animation is complete
function zoomProductInCallback() {
	$(".product-images-container, .product-sidebar__overlay").removeClass("is-animating");
	$(".global-header, .global-footer, .product-sidebar, .product-section:not(.product-section--main)").hide();

	// Calculate new height and set inline
	var $section = $(".product-section--main"),
		sectionTop = $section.offset().top,
		winHeight = $(window).innerHeight();

	$page.css("overflow", "visible");
	$section.css("height", winHeight - sectionTop + 100);
}

// Product zoom in animation
function zoomProductIn() {
	var $img = $(".product-images-container"),
		winWidth = $(window).innerWidth(),
		winCenter = $(window).innerWidth() / 2,
		imgLeft = $img.offset().left,
		imgHeight = $img.height(),
		imgWidth = $img.width(),
		imgCenter = imgLeft + (imgWidth / 2),
		imgOffset = winCenter - imgCenter,
		imgScaleFactor = 0.75, // percent of window width
		thumbsLeft = $(".product-thumbnails-container").offset().left,
		maxWidth = 1400,
		maxHeight = 1400,
		imgScale;

	$img.add(".product-sidebar__overlay").addClass("is-animating");

	if ($body.is(".product-footwear")) {
		// calculate zoom
		imgScale = imgScaleFactor * winWidth / imgWidth;
		if (imgScale * imgWidth > maxWidth) { imgScale = maxWidth / imgWidth; }

		// Zoom product
		TweenLite.set(".product-thumbnails-container", {y: 30});
		TweenLite.set(".product-image-carousel", {scale: imgScale, x: imgOffset});
		setTimeout(function(){
			zoomProductInCallback();
		}, $(".product-image-carousel").transitionDur()*1000);


	} else if ($body.is(".product-clothing")) {
		imgScaleFactor = 0.9;

		// calculate zoom
		imgScale = imgScaleFactor * winWidth / imgWidth;
		if (imgScale * imgHeight > maxHeight) { imgScale = maxHeight / imgHeight; }

		TweenLite.set(".product-thumbnails-container", {x: -thumbsLeft + 0.02*winWidth});
		TweenLite.set(".product-images-container", {scale: imgScale, x: imgOffset});
		setTimeout(function(){
			zoomProductInCallback();
		}, $(".product-images-container").transitionDur()*1000);
	}

	// Scroll window to remove staggering after animation
	TweenMax.to($(window), 0.6, {scrollTo: {y: 0}, ease: Power3.easeOut});

	// Hide other elements
	TweenMax.to(".product-sidebar", 			 0.6, {opacity: 0, x: 50, ease: Power3.easeOut});
	TweenMax.to(".product-sidebar__overlay", 	 0.3, {x: "0%", ease: Power3.easeOut});
	TweenMax.to(".product-header", 				 0.4, {opacity: 0});
	TweenMax.to(".product-section:not(.product-section--main)", 0.6, {opacity: 0, y: 100, ease: Power3.easeOut});
	if (isDesktop) { TweenMax.to($header,	 	 0.6, {opacity: 0, x: -50, ease: Power3.easeOut}); }

}

// Product zoom out animation
function zoomProductOut() {
	$page.attr("style", "");
	$(".product-section--main").attr("style", "");
	$(".product-images-container").addClass("is-animating");
	$(".global-header, .global-footer, .product-sidebar, .product-section").show();

	if ($body.is(".product-footwear")) {
		// Zoom out product
		TweenLite.set(".product-image-carousel", {scale: 1, x: 0});
		setTimeout(function(){
			$(".product-images-container, .product-sidebar__overlay").removeClass("is-animating");
		}, $(".product-image-carousel").transitionDur()*1000);


	} else if ($body.is(".product-clothing")) {
		// Zoom out product
		TweenLite.set(".product-thumbnails-container", {y: 0, x: 0});
		TweenLite.set(".product-images-container", {scale: 1, x: 0});
		setTimeout(function(){
			$(".product-images-container, .product-sidebar__overlay").removeClass("is-animating");
		}, $(".product-images-container").transitionDur()*1000);
	}

	// Show other stuff
	TweenMax.to(".product-sidebar", 			 0.6, {opacity: 1, x: 0, ease: Power3.easeOut});
	TweenMax.to(".product-sidebar__overlay", 	 0.6, {x: "-100%", ease: Power3.easeOut});
	TweenMax.to(".product-header", 				 0.6, {opacity: 1});
	TweenMax.to(".product-section:not(.product-section--main)", 0.6, {opacity: 1, y: 0, ease: Power3.easeOut});
	if (isDesktop) { TweenMax.to($header, 		 0.6, {opacity: 1, x: 0, ease: Power3.easeOut}); }
}


// Init desktop slideshow

// Thumbnail mouse events
$(".product-thumbnails-container").on("click", ".product-thumbnail", function(e){
	if (window.matchMedia("(min-width: 768px)").matches) {
		switchProductImage($(this).index());
	}
});

// Thumbnail keyboard event
$(".product-thumbnails-container .product-thumbnail").keypress(function(e){
	if (e.keyCode == 13) {
		switchProductImage($(this).index());
	}
});

// Show an image in the product image carousel
// index, integer, the index of the image to show
function switchProductImage(index) {
	var $images = $(".product-image-large", ".product-image-carousel");

	$images.removeClass("is-active");
	$images.eq(index).addClass("is-active");
}









// Set background image on video container
$(".video").not("html").each(function(){
	var url = $(this).find(".video__poster").attr("src");
	if (url) { $(this).css("background-image", "url(" + url + ")"); }
});

// Get youtube embed code from play button and show embed iframe
$(".video").on("click", ".video__play", function(){
	var	baseUrl = window.location.protocol + "//www.youtube.com/embed/", 
		url = $(this).data("video-url"),
		attributes = "?rel=0&showinfo=0&autoplay=1&allowfullscreen",
		$button = $(this),
		$lightbox = $("#video-lightbox"),
		$player = $("#video-lightbox iframe");

	// remove sharing URL code
	url = url.split("/");
	url = url[url.length - 1];

	// add embed code and settings to url string
	url = baseUrl + url + attributes;

	// set url in iframe and show
	$player.attr("src", url);
	$lightbox.insertAfter($(this).parents(".video")).fadeIn("slow");
});

$(".video-lightbox-close").on("click", function(){
	$("#video-lightbox").hide().find("iframe").attr("src", "");
});












// Init background video
if (!isTouchDevice) {
	$(".background-video").each(function(){
		var vidBase 	= window.location.protocol + "//www.youtube.com/embed/",
			vidUrl 		= $(this).data("youtube-url"),
			vidStart 	= $(this).data("video-start"),
			vidOpts 	= "autoplay=1&loop=1&controls=0&showinfo=0&modestbranding=1",
			embedUrl;

		// Set video starting point, if any
		if (vidStart) { vidStart = "&start=" + vidStart; }
		if (!vidStart) { vidStart = ""; }

		// Construct youtube embed url
		embedUrl = vidBase + vidUrl + "?" + vidOpts + vidStart + "&playlist=" + vidUrl;

		// Inject iframe
		$(this).after('<iframe frameborder="0" volume="0" allowfullscreen src="' + embedUrl + '"></iframe>');
	});
}











// MOLECULE – image link list

$(".image-link-list").each(function(){
	$(this).find("ul").hide();

	// Remove trigger element if no links are supplied
	if (! $(this).find("li").length) { $(this).find(".image-link-list__trigger").remove(); }

	// don't set background image for the contained image style
	if (!$(this).is(".image-link-list--image-contained")) {
		var imgSrc =  $(this).find("img").attr("src");

		$(this).attr("style", "background-image: url(" + imgSrc + ")");
	}
});

if ($(".image-link-list li").length) {
	// link list stuff
	if (isTouchDevice) {
		$(".image-link-list").on("click", function(){
			var $container = $(this);

			if ($container.is(".is-active")) {
				hideImageLinkList($container);
			} else {
				showImageLinkList($container);
			}
		});
	} else {
		$(".image-link-list").on("mouseenter", function(){
			showImageLinkList($(this));
		});

		$(".image-link-list").on("mouseleave", function(){
			hideImageLinkList($(this));
		});
	}


	$(".image-link-list a").on("click", function(e){
		e.stopPropagation();
	});
}

// Show link list with animation
function showImageLinkList($container) {
	if ($container.find("li").length) {
		$container.addClass("is-active");
		$container.find("ul").show();
		TweenLite.set($container.find("li"), {y: 0, opacity: 1});
		TweenLite.to($container.find(".image-link-list__trigger"), 0.8, {opacity: 0, ease: Power3.easeOut});
		TweenLite.to($container.find("ul"), 0.8, {opacity: 1, ease: Power3.easeOut});
		TweenMax.staggerFrom($container.find("li"), 0.8, {y: 100, opacity: 0, ease: Power3.easeOut}, 0.07);
	}
}

// Hide link list with animation
function hideImageLinkList($container) {
	if ($container.find("li").length) {
		$container.removeClass("is-active");
		TweenLite.to($container.find("li"), 0.8, {y: 10, ease: Power3.easeOut});
		TweenLite.to($container.find("ul"), 0.8, {opacity: 0, ease: Power3.easeOut, onComplete: function(){
			$container.find("ul").hide();
		}});
	}
}











// MOLECULE – grid block
// Set background image from image element
$(".grid-block__image").each(function(){
	$(this).attr("style", "background-image: url(" + $(this).find("img").attr("src") +")");
});

// Add scroll reveal effect
$(".section-page .grid-block > li > div").scrollReveal({stagger: true, staggerDelay: 0.07});













// MOLECULE – promo image

if ($(".section-page").length) {
	$(".promo-image h2").scrollReveal({duration: 3, y: 100, trigger: ".promo-image"});
	$(".promo-image img").scrollReveal({trigger: ".promo-image"});
	$(".promo-image__text").scrollReveal();
}

// Add special animation if this element is the first of the page
if ($(".section-page").length || $(".launch-article-page").length) {
	var $el = $(".promo-image:first-child"),
		$heading = $el.find("h2"),
		$img = $el.find("img"),
		$txt = $el.find("p"),
		$facts = $el.siblings(".highlighted-facts");

		$el.addClass("is-animating");

		// The component can use both h1 and h2, depending on context
		if (!$heading.length) { $heading = $el.find("h1"); }

		// Animate heading
		TweenLite.set($heading, 	{ opacity: 0, y: 150 });
		TweenLite.to($heading, 1.5, { opacity: 1, ease: Linear.easeNone });
		TweenLite.to($heading, 2, 	{ y: 0, ease: Power3.easeOut });

		// Animate text
		TweenLite.set($txt,		{ opacity: 0, scale: 0.9 });
		TweenLite.to($txt, 2,	{ scale: 1, ease: Power3.easeOut });
		TweenLite.to($txt, 1.50,{ opacity: 1, ease: Linear.easeNone });

		// Animate image
		TweenLite.set($img, 	{opacity: 0, y: 70 });
		TweenLite.to($img, 1.5,	{ opacity: 1, ease: Linear.easeNone });
		TweenLite.to($img, 2,	{ y: 0, ease: Power3.easeOut, onComplete: function(){
			$el.removeClass("is-animating");
		} });

		// Animate facts
		if ($facts.length) {
			TweenLite.set($facts, 	{opacity: 0, y: -20 });
			TweenLite.to($facts, 1.5,	{ opacity: 1, ease: Linear.easeNone });
			TweenLite.to($facts, 2,	{ y: 0, ease: Power3.easeOut, onComplete: function(){
				$el.removeClass("is-animating");
			} });
		}
}













// MOLECULE – Media block

$(".media-block__image-toggler").on("click", function(e){
	e.preventDefault();
	e.stopPropagation();
	$(this).parents("[class*='__image']").toggleClass("is-expanded");
});


$(".section-page .media-block__image").scrollReveal();
$(".section-page .media-block__text").scrollReveal({delay: 0.3});














// MOLECULE – filters

// Init image filter
if ($(".filters__image-filter").length) {
	if (supportsMutationObservers) {
		// Make sure that init function runs after chosen is initialized
		var imageFilterObserver = new MutationObserver(function(){
			if ($(".filters__image-filter").next(".chosen-container").length) {
				initImageFilter();
				imageFilterObserver.disconnect();
			}
		});
		imageFilterObserver.observe($(".filters").get(0), { childList: true });
	} else {
		// Fallback DOM manipulation callback
		$(".filters").on("DOMSubtreeModified", function(){
			if ($(".filters__image-filter").next(".chosen-container").length) {
				initImageFilter();
				$(".filters").off("DOMSubtreeModified");
			}
		});
	}
}

function initImageFilter() {
	var $select = $(".filters__image-filter"),
		$chosen = $select.next(".chosen-container"),
		images = [];

	// Get and load images
	$select.find("option").each(function(index){
		// Don't do for any disabled options
		var img = new Image(),
			url = $(this).data("img-src");

		if (typeof url === "undefined") { url = ""; }

		img.src = url;
		images.push(img);
	});

	$select.on("chosen:showing_dropdown", function(){
		$chosen.find(".chosen-results").children("li").each(function(index){
			if (! $(this).is(".placeholder")) {
				$(this).prepend('<div class="image"><img src="' + images[index].src +'"/></div>');
			}
		});
	});
}











// ORGANISM - header
var mobileHeaderShown = false;

$page.on("click", function(){
	if ($body.is(".mobile-nav-is-open")) {
		hideMobileHeader();
	}
});

$header.on("click", function(e){
	e.stopPropagation();
});

// Stop click propagation on secondary nav list to avoid conflicts in login and language dialogs
$(".secondary-nav").on("click", function(e){
	e.stopPropagation();
});

// Mobile search form
$("#mobile-search-input").on("change", function(){
	if ($(this).val() !== "") {
		$(this).addClass("has-content");
	} else {
		$(this).removeClass("has-content");
	}
});


// Find tabbable elements
$header.show();
var $headerTabbable = $header.show().find("a, button, input, select").filter(function(){
	// Filter out any un-tabbable elements
	if ($(this).attr("tabindex") !== "-1") { return true; }
}).filter(":visible").filter(":last").add(".mobile-header-toggle");
$header.attr("style", "");

// Mobile open / close header
$(".mobile-header-toggle").on(touchOrClick, function(e){
	e.stopPropagation();
	e.preventDefault();

	if (mobileHeaderShown) {
		hideMobileHeader();
		mobileHeaderToggler.off();
		// remove keyboard focus trap
		$headerTabbable.last().off("blur", trapLast);
	} else {
		showMobileHeader();
		mobileHeaderToggler.on();
		// trap keyboard focus within header
		$headerTabbable.last().on("blur", {tabbable: $headerTabbable, container: $header}, trapLast);
	}
});

// Set up aria toggler object to handle aria state
var mobileHeaderToggler = new ariaToggler($(".mobile-header-toggle"), $header);


// Hide the global menu on small screens
function hideMobileHeader() {
	mobileHeaderShown = false;
	$body.removeClass("mobile-nav-is-open");

	var pageTop = parseInt($page.css("top"));
	$page.attr("style", "");
	$(window).scrollTop(-pageTop);

	TweenLite.to($header, 0.6, {x: "-100%", ease: Power3.easeOut});
	TweenLite.to([".page", ".mobile-header"], 0.6, {x: 0, ease: Power3.easeOut, onComplete: function(){
		$header.attr("style", "");
	}});
}

// Show the global menu on small screens
function showMobileHeader() {
	mobileHeaderShown = true;
	$body.addClass("mobile-nav-is-open");
	var headerWidth = $header.outerWidth();

	$header.show();
	TweenLite.to($header, 0.6, {x: "0%", ease: Power3.easeOut});
	TweenLite.to([".page", ".mobile-header"], 0.6, {x: headerWidth, ease: Power3.easeOut});

	$page.css({
		"position": "fixed",
		"top": - $(window).scrollTop()
	});

	hideScrollHint();
}

// Show header bar on small screens
function showMobileHeaderBar() {
	TweenLite.to(".mobile-header", 0.6, {y: "0%", ease: Power3.easeOut});
}

// Hide header bar on small screens
function hideMobileHeaderBar() {
	TweenLite.to(".mobile-header", 0.6, {y: "-100%", ease: Power3.easeOut});

}


// Init accordions for nav categories in the global menu that has sub navs
$(".main-nav a.has-sub-nav").each(function(index) {
	var $list = $(this).siblings("ul"),
		$listItem = $list.find("> li"),
		$trigger = $(this),
		id = "main-nav-content-" + index;

	// Get ID from content container, if any
	if ($list.attr("id") !== undefined) {
		id = $list.attr("id");
	} else {
		// Set generated ID on content
		$list.attr("id", id);
	}

	// Set accessibility stuff
	if ($list.is(":visible")) {
		$trigger.attr({"aria-expanded": "true", "aria-controls": id});
	} else {
		$trigger.attr({"aria-expanded": "false", "aria-controls": id});
	}


	$trigger.on("click", function(e){
		e.preventDefault();

		// Expand nav list and collapse siblings
		if (!$list.is(".is-expanded") && !$list.is(".is-animating")) {
			// Collapse sibling sub nav if open
			var $siblings = $("ul.is-expanded", $list.parents("ul").first()).not($list),
				$siblingTriggers = $("a.is-expanded", $list.parents("ul").first()).not($list);
			$siblings.removeClass("is-expanded");
			$siblingTriggers.removeClass("is-expanded").attr("aria-expanded", "false");
			collapseGlobalNavItem($siblings);

			// Expand clicked sub nav
			expandGlobalNavItem($list, $listItem);
			$list.addClass("is-expanded");
			$trigger.addClass("is-expanded").attr("aria-expanded", "true");


		// Collapse nav list and children nav lists
		} else if ($list.is(".is-expanded") && !$list.is(".is-animating")) {
			// Collapse nav list and children
			var $children = $list.find("ul.is-expanded");

			collapseGlobalNavItem($list);
			collapseGlobalNavItem($children);

			$list.removeClass("is-expanded");
			$children.removeClass("is-expanded");

			$trigger.removeClass("is-expanded").attr("aria-expanded", "false");
			$list.find("a.has-sub-nav").removeClass("is-expanded").attr("aria-expanded", "false");
		}
	});
});


// Reveal correct page in navigation meny on product pages
if ($(".product-footwear").length || $(".product-clothing").length) {
	// Get referrer URL and strip base URL
	var urlStub = document.referrer.replace(window.location.origin, "");

	if (urlStub.length) {
		// Find nav element with the matching URL
		var $navItem = $(".global-header .main-nav a[href='" + urlStub + "']");

		// Add classes to expand the correct nav containers
		$navItem.addClass("is-active");
		$navItem.closest(".main-nav__third-level").addClass("is-expanded");
		$navItem.closest(".main-nav__third-level").siblings("a").addClass("is-expanded");
		$navItem.closest(".main-nav__second-level").addClass("is-expanded");
		$navItem.closest(".main-nav__second-level").siblings("a").addClass("is-expanded");
	}
}


// Set the global header position to fixed when the bottom of the header aligns with the bottom of the screen
if (isDesktop && !isTouchDevice) {
	$(window).on("scroll",function(){
		var headerH = $header.outerHeight(),
			winH = $(window).innerHeight(),
			scrollY = $(window).scrollTop();

		if (scrollY < (headerH - winH)) {
			$header.css("top", -scrollY);
		} else {
			$header.css("top", -(headerH - winH));
		}
	});
}

// Expand global header nav list
function expandGlobalNavItem($list, $listItem) {
	// set initial states
	$list.addClass("is-animating");
	$list.attr("style", "");
	$list.css({display: "block", height: "auto"});
	TweenLite.set($listItem, {y: -20});

	// animate
	TweenMax.staggerTo($listItem, 0.6, {y: 0, ease: Power4.easeOut}, 0.035);
	TweenLite.from($list, 0.6, {height: 0, z: 0.01, ease: Power3.easeOut, onComplete: function(){
		$list.css("height", "auto");
		$list.removeClass("is-animating");
	}});
}

// Collapse global header nav list
function collapseGlobalNavItem($list) {
	$list.addClass("is-animating");
	TweenLite.set($list, {height: "auto", display: "block"});
	TweenLite.to($list, 0.6, {height: 0, paddingBottom: 0, ease: Power3.easeOut, onComplete: function(){
		$list.hide();
		$list.removeClass("is-animating");
	}});
}











// ORGANISM - Language select sidebar

if (isDesktop) {
	$("#language-selector-link").on("click", function(e){
		e.preventDefault();
	});

	// Init language select sidebar dialog
	var languageDialog = new dialog($languageSelector, $("#language-selector-link"), $(".language-selector__close"), showLanguageSelect, hideLanguageSelect);
	allDialogs.push(languageDialog);
}



// Language selector animate in
function showLanguageSelect() {
	$languageSelector.show();
	TweenLite.set($languageSelector, {opacity: 1, x: "-100%"});
	TweenLite.to($languageSelector, 0.6, {x: "0%", opacity: 1, ease: Power4.easeOut});
}

// Language selector animate out
function hideLanguageSelect() {
	TweenLite.to($languageSelector, 0.6, {opacity: 0, ease: Power3.easeIn});
	TweenLite.to($languageSelector, 0.6, {x: "-100%", ease: Power4.easeOut, onComplete: function(){
		$languageSelector.hide();
	}});
}










// ORGANISM - Log in / register form

$(".register-form-open").on("click", function(){
	showSecondaryForm($registerForm, $loginForm);
});

$(".register-form-close").on("click", function(e){
	e.preventDefault();
	hideSecondaryForm($registerForm, $loginForm);
});

$(".forgot-password-link").on("click", function(e){
	e.preventDefault();
	showSecondaryForm($pwForm, $loginForm);
});

$(".password-recovery-form-close").on("click", function(e){
	e.preventDefault();
	hideSecondaryForm($pwForm, $loginForm);
});

$loginDrawer.on("click", function(e){
	e.stopPropagation();
});

$("#login-register-link").on("click", function(e){
	if (isDesktop) { e.preventDefault(); }
});

// Set up login / register dialog
var loginDialog = new dialog($loginDrawer, $("#login-register-link"), $(".login-drawer__close"), showLoginDrawer, hideLoginDrawer);
allDialogs.push(loginDialog);

// Lock sub-form focus
$loginDrawer.find(".login-drawer__register-form, .login-drawer__password-recovery-form").each(function(){
	var $thisForm = $(this),
		$tabbable = $(this).find("a, button, input");

	$thisForm.attr("tabindex", -1);

	$tabbable.first().on("blur", function(){
		setTimeout(function() {
			if ($thisForm.has($(document.activeElement)).length <= 0 && !$body.is(".mouse-input")) {
				$tabbable.last().focus();
			}
		}, 20);
	});

	$tabbable.last().on("blur", function(){
		setTimeout(function() {
			if ($thisForm.has($(document.activeElement)).length <= 0 && !$body.is(".mouse-input")) {
				$tabbable.first().focus();
			}
		}, 20);
	});
});

// Show secondary content in login sidebar
function showSecondaryForm($form, $hideForm) {
	if (isDesktop) {
		$loginDrawer.css("overflow", "hidden").scrollTop(0);
		TweenLite.to($hideForm, 0.8, {x: "-120%", opacity: 0, ease: Expo.easeOut});
		TweenLite.to(".login-drawer__close", 0.3, {opacity: 0});
		TweenLite.set($form, {position: "absolute", opacity: 0, top: "0", x: "100%", display: "block"});
		TweenLite.to($form, 0.8, {x: "0%", opacity: 1, ease: Expo.easeOut, onComplete: function(){
			$loginDrawer.css("overflow", "auto");
			$hideForm.hide();
			$form.attr("style", "").show();
			$form.focus();
		}});
	}
}

// Hide secondary content in login sidebar
function hideSecondaryForm($form, $showForm) {
	if (isDesktop) {
		$loginDrawer.css("overflow", "hidden").scrollTop(0);
		$showForm.show();
		TweenLite.to($showForm, 0.8, {x: "0%", opacity: 1, ease: Expo.easeOut});
		TweenLite.to(".login-drawer__close", 0.3, {opacity: 1, delay: 0.3});
		TweenLite.set($form, {position: "absolute", top: "0"});
		TweenLite.to($form, 0.8, {x: "100%", opacity: 0, ease: Expo.easeOut, onComplete: function(){
			$form.attr("style", "").hide();
			$loginDrawer.focus().css("overflow", "auto");
		}});
	}
}

// Login form animate in
function showLoginDrawer() {
	$loginDrawer.show();
	TweenLite.set($loginDrawer, {opacity: 1, x: "-100%"});
	TweenLite.to($loginDrawer, 0.6, {x: "0%", opacity: 1, ease: Power4.easeOut});
}

// Login form animate out
function hideLoginDrawer() {
	TweenLite.to($loginDrawer, 0.6, {opacity: 0, ease: Power3.easeIn});
	TweenLite.to($loginDrawer, 0.6, {x: "-100%", ease: Power4.easeOut, onComplete: function(){
		$loginDrawer.hide();
	}});
}















// Scroll reveal effect article content
$(".article-page .article__content > div").scrollReveal({offset: 0.05});








// LAUNCH ARTICLE

// Scrollmagic stuff
var launchController,
	launchScenes = [],
	productFeaturesPositions = [];

// Init media slideshow scroll effects
function mediaSlideshowFX() {
	// Reset stuff
	productFeaturesPositions = [];

	// Destroy scrollmagic stuff if initialized before
	if (launchController) {
		launchController.removeScene(launchScenes).destroy(true);
		launchScenes = [];
		$(".media-slideshow__images").each(function(){
			if ($(this).parent().is("[data-scrollmagic-pin-spacer]")) {
				$(this).unwrap().attr("style", "");
			}
		});
	}

	// Only init effects if on desktop
	if (isDesktop) {
		launchController = new ScrollMagic.Controller({refreshInterval: 50});
		launchController.enabled(true);

		// Get slide positions and push to array, to use for scroll snapping
		$(".media-slideshow__content > li").each(function(index){
			productFeaturesPositions.push(parseInt($(this).offset().top));
		});


		// Sticky image container and fade in slideshow
		$(".media-slideshow").each(function(index){
			var $container = $(this),
				id = "#" + $container.attr("id"),
				$slides = $container.find(".media-slideshow__content > li"),
				$images = $container.find(".media-slideshow__images");

			$slides.each(function(index){
				var $li = $images.find("li").eq(index),
					$img = $images.find("img").eq(index);

				// Position images in container
				$img.css("top", $(window).innerHeight() / 2);

				// Reverse z-index of elements to allow for reveal effect and set background color
				$li.attr("style", "").css({
					"z-index": $slides.length - index,
					"background-color": getBgColor($img)
				});

				// Add scroll animation for all elements except the last one
				if (index < ($slides.length)-1) {
					// Get IDs to use with scrollmagic
					var id = "#" + $li.attr("id");
						triggerId = "#" + $container.find("> ul > li").eq(index).attr("id");

					launchScenes.push(new ScrollMagic.Scene({
							triggerHook: "onLeave",
							triggerElement: triggerId,
							duration: $(window).innerHeight()
						})
						.setTween(id, {height: 0, ease: Linear.easeNone})
						.addTo(launchController)
					);
				}
			});

			// Reset style on container
			$(id).attr("style", "");

			// Set sticky image container
			launchScenes.push(new ScrollMagic.Scene({
					triggerHook: "onLeave",
					triggerElement: id,
					duration: $container.height() - $(window).innerHeight()})
				.setPin(id + " .media-slideshow__images")
				.addTo(launchController)
			);

			// Fade in first slideshow on page on scroll
			if (index === 0) {
				// Add effect to first slideshow on page
				var firstOffset = $(window).innerHeight() - $container.offset().top + 150;

				launchScenes.push(new ScrollMagic.Scene({
						triggerHook: "onEnter",
						triggerElement: id,
						duration: 0,
						offset: firstOffset
					})
					.setTween(TweenLite.to(id, 0.6, {opacity: 1, ease:Linear.easeNone}))
					.addTo(launchController)
				);
			} else {
				// Show other slideshows on page
				$container.css("opacity", 1);
			}
		});


		// Text reveal effects
		$(".media-slideshow__content > li").each(function(){
			var id = "#" + $(this).attr("id"),
				li = id + " li",
				h2 = id + " h2",
				icon = id + " .media-block__icon",
				all = li + ", " + h2 + ", " + icon;

			// Reset styles
			$(all).attr("style", "");
			TweenLite.set(all, {opacity: 1, y: 0, scale: 1});

			// List item effect
			launchScenes.push(new ScrollMagic.Scene({
					triggerHook: "onEnter",
					triggerElement: id + " ul",
					duration: 0,
					offset: 200
				})
				.setTween(TweenMax.staggerFrom(li, 1, {
					y: 100,
					opacity: 0,
					ease: Power3.easeOut
				}, 0.1))
				.addTo(launchController)
			);

			// Heading effect
			launchScenes.push(new ScrollMagic.Scene({
					triggerHook: "onEnter",
					triggerElement: h2,
					duration: 0,
					offset: 200
				})
				.setTween(TweenMax.from(h2, 2, {
					scale: 0.93,
					opacity: 0,
					ease: Power2.easeOut
				}))
				.addTo(launchController)
			);

			// Icon effect
			launchScenes.push(new ScrollMagic.Scene({
					triggerHook: "onEnter",
					triggerElement: icon,
					duration: 0,
					offset: 100
				})
				.setTween(TweenMax.from(icon, 2, {
					y: 50,
					scale: 0,
					opacity: 0,
					delay: 0.3,
					ease: Elastic.easeOut.config(1, 0.8)
				}, 0.1))
				.addTo(launchController)
			);
		});

		// Trigger scroll to trigger scroll snap after resize
		$(window).trigger("scroll");
	} // end if (isDesktop)
}

// Init media slideshow
function mediaSlideshowInit() {
	// Add IDs for scrollmagic to text elements
	$(".media-slideshow__content > li").each(function(index){
		if (typeof $(this).attr("id") === "undefined" && isDesktop) {
			$(this).attr("id", "media-slideshow-text-" + index);
		}
	});

	$(".media-slideshow").each(function(index){
		// Create desktop image slides if not already present
		if(! $(this).find(".media-slideshow__images").length && isDesktop) {
			// Create image container for desktop
			$(this).append("<ul class='media-slideshow__images'></ul>");

			var $container = $(this),
				id = $container.attr("id"),
				$slides = $container.find(".media-slideshow__content > li"),
				$images = $container.find(".media-slideshow__images"),
				$img,
				containerIndex = index;

			// Set ID if missing
			if (typeof id === "undefined") {
				id = "media-slideshow-" + index;
				$container.attr("id", id);
			}

			$slides.each(function(index){
				// Add images to image container and set background colors
				var $img = $(this).find(".media-block__image img"),
					imgUrl = $img.attr("src");

				$images.append("<li><img src='" + imgUrl + "'/></li>");

				// Set IDs for scrollmagic. Exclude last element
				if (index < ($slides.length)-1) {
					var id = "media-slideshow-image-" + containerIndex + "-" + index,
						triggerId = id + "-trigger"; // ScrollMagi trigger

					$images.find("li").eq(index).attr("id", id);
					$container.find("> ul > li").eq(index).attr("id", triggerId);
				}
			});
		}
	}); // end $each
}

if ($("html").hasClass("flexbox") && $(".media-slideshow").length) {
	// Set up desktop slideshow
	mediaSlideshowInit();
	mediaSlideshowFX();

	// Scroll snap
	$(window).load(function(){
		productFeaturesPositions = [];
		$(".media-slideshow__content > li").each(function(index){
			productFeaturesPositions.push(parseInt($(this).offset().top));
		});
	});

	$(window).on("scroll", debounce(function(){
		if (isDesktop) {
			for (var i=0, len=$(".media-slideshow__content > li").length; i < len; i++) {
				var pos = productFeaturesPositions[i],
					distanceToPoint = Math.abs(pos - $(window).scrollTop());
				
				if ($("html").hasClass("flexbox")) {
					if (distanceToPoint < 0.5*$(window).innerHeight()) {
						TweenLite.to($(window), 0.3, {scrollTo: {y: pos}, ease: Power1.easeOut});
					}
				}
			}
		}
	}, 200));
}



// Apply background colors from images
if ($(".launch-article-page").length) {
	$(".media-slideshow__content > li").each(function(index){
		var $li = $(this),
			$img = $li.find(".media-block__image img"); 

		$img.load(function(){
			bgColor = getBgColor($img);

			// Apply style
			$(this).parent().css("background-color", bgColor);

			if ($(".media-slideshow__images").length && isDesktop) {
				$(".media-slideshow__images > li").eq(index).css("background-color", bgColor);
			}
		});
	});
}












// ORGANISM - Shopping cart
var mobileCartShown = false;

// Remove from cart button click handling
$cart.add(".shopping-cart--page").on("click", ".remove-from-cart", function(e){
	e.preventDefault();
	var $el = $(this).parents(".shopping-cart__product");

	removeCartItem($el);
});

$(".shopping-cart-trigger").on("click", function(e){
    e.stopPropagation();
});

$(".mobile-shopping-cart-toggle").on("click", function(e){
	e.stopPropagation();
});

// Set up mobile shopping cart dialog
var mobileCartDialog = new dialog($cart, $(".mobile-shopping-cart-toggle"), $(".close-shopping-cart"), showCart, hideCart);
allDialogs.push(mobileCartDialog);

// Set up shopping cart dialog
var cartDialog = new dialog($cart, $(".shopping-cart-trigger"), $(".close-shopping-cart"), showCart, hideCart);
allDialogs.push(cartDialog);

function toggleCart() {
	if (mobileCartShown) { hideCart(); }
	else 				{ showCart(); }
}

// Hide shopping cart sidebar animation
function hideCart() {
	mobileCartShown = false;
	$body.removeClass("shopping-cart-is-open");

	if (isDesktop) {
		TweenLite.to($cart, 0.6, {x: "100%", ease: Power4.easeOut});
		TweenLite.to($cart, 0.2, {opacity: 0, delay: 0.4, ease: Linear.easeNone, onComplete: function(){
			$cart.hide();
		}});
	} else {
		TweenLite.to($cart, 0.6, {x: "100%", ease: Power4.easeOut, onComplete: function(){
			$cart.hide();
		}});
		TweenLite.to([".page", ".mobile-header"], 0.6, {x: 0, ease: Power4.easeOut});
	}
}

// Show shopping cart sidebar animation
function showCart() {
	mobileCartShown = true;
	$body.addClass("shopping-cart-is-open");
	$cart.attr("style", "").show();

	if (isDesktop) {
    	TweenLite.to($cart, 0.6, {x: "0%", opacity: 1, ease: Power4.easeOut});
	} else {
		var cartWidth = $cart.outerWidth();

		TweenLite.to($cart, 0.6, {x: "0%", ease: Power4.easeOut});
		TweenLite.to([".page", ".mobile-header"], 0.6, {x: -cartWidth, ease: Power4.easeOut});
	}
}

// Animate out shopping cart item and remove from markup
function removeCartItem($el) {
	$el.css("border", "none").slideUp(function(){
		$el.remove();
		$(".close-shopping-cart").focus();
	});
}















// ORGANISM - Search

$search.on("click", function(e){
	e.stopPropagation();
});

$(".search-trigger").on("click", function(e){
	e.stopPropagation();
});

// Set up search sidebar dialog
var searchDialog = new dialog($search, $(".search-trigger"), $(".close-search"), showSearch, hideSearch);
allDialogs.push(searchDialog);

$(".mobile-search-toggle").on("click", function(){
	if ($body.is(".search-is-open")) {
		hideSearch();
	} else {
		showSearch();
	}
});

// Hide search sidebar animation
function hideSearch() {
	$body.removeClass("search-is-open");

	TweenLite.to($search, 0.6, {x: "100%", ease: Power4.easeOut});
	TweenLite.to($search, 0.2, {opacity: 0, delay: 0.4, ease: Linear.easeNone, onComplete: function(){
		$search.hide();
	}});
}

// Show search sidebar animation
function showSearch() {
	$body.addClass("search-is-open");
	$search.attr("style", "").show();

	if (isDesktop) {
		setTimeout(function() { $(".search .search__input-form input").focus(); }, 700);
		TweenLite.to(".search", 0.6, {x: "0%", ease: Power4.easeOut});
	}
}












// ORGANISM - My account tabs

// Order history
if ($(".order-history").length) {
	// Init accordion for each order history item
	$(".order-history__trigger").each(function(){
		new accordion($(this), $(this).closest("ul").siblings(".order-history__details"));
	});

	// Toggle text on order history item accordion trigger
	$(".order-history__trigger").on("click", function(e){
		e.stopPropagation();
		var textEl = $(this).find(".order-history__trigger-text"),
			text = textEl.text(),
			altText = textEl.data("expanded-text");

		textEl.text(altText).data("expanded-text", text);
	});

	// Make click on entire list item trigger click on accordion trigger
	$(".order-history__overview").on("click", function(e){
		if (window.matchMedia("(min-width: 768px)").matches) {
			$(this).find(".order-history__trigger").trigger("click");
		}
	});

	// Make enter key press on entire list item trigger click on accordion trigger
	$(".order-history__overview").on("keypress", function(e){
		if (window.matchMedia("(min-width: 768px)").matches && e.keyCode === 13) {
			$(this).find(".order-history__trigger").trigger("click");
		}
	});

	$(".order-history__mobile-trigger").on("click", function(e){
		$(this).closest("li").find(".order-history__trigger").trigger("click");
	});

	$(".order-history__overview a").on("click", function(e){
		e.stopPropagation();
	});
}


// Edit saved addresses

// Make tabbable
$(".customer-address-list__form, .customer-address-list__data").attr("tabindex", -1);

// Show / hide edit address form
$(".customer-address-list").on("click", ".edit-address", function(e){
	e.preventDefault();

	var $container = $(this).closest("li");
	toggleContent($container.find(".customer-address-list__data"), $container.find(".customer-address-list__form"), $container);
});

// Hide edit address form
$(".customer-address-list").on("click", ".cancel-edit-address", function(e){
	e.preventDefault();

	var $container = $(this).closest("li");
	toggleContent($container.find(".customer-address-list__form"), $container.find(".customer-address-list__data"), $container);

	// Reset text input fields
	$container.find(".customer-address-list__form").find("input[type='text']").each(function(index){
		$(this).val($(this).attr("value"));
	});

	// Reset dropdown lists
	$container.find(".customer-address-list__form").find("select").each(function(index){
		var selectedIndex;

		// Find initially selected option element
		$(this).find("option").each(function(index){
			if ($(this).attr("selected")) { selectedIndex = index + 1; }
		});

		$(this).val(selectedIndex);
	});
});

// check and set invisible checkbox state, used by backend
$(".customer-address-list__form").on("change", "input, select", function(){
	if (!$(this).is(".address-was-changed")) {
		$(this).parents(".customer-address-list__form").find(".address-was-changed").prop("checked", true);
	}
});

// Add checked state class to label in some cases where backend inserts a hidden checkbox between an input and its label
$("input[type='checkbox']").on("change", function(){
	if ($(this).next("input[type='hidden']")) {
		$(this).next("input[type='hidden']").next("label").toggleClass("is-checked", $(this).prop("checked"));
	}
});

// Show add address form
$(".customer-address-list__add-form-trigger").on("click", function(e){
	e.preventDefault();
	$(".customer-address-list__add-form").slideDown();
});













// ORGANISM - Front page grid

if ($(".frontpage-grid").length) { initFrontpageGrid(); }

// Init front page grid
function initFrontpageGrid() {
	// Show content on mouse enter / touchstart small screens
	$(".frontpage-grid").on(touchOrHoverIn, ".frontpage-grid__column", function(){
		if (!isDesktop) { showFrontpageGridContent($(this)); }
	});

	// Hide content on mouse leave / touchend small screens
	$(".frontpage-grid").on(touchOrHoverOut, ".frontpage-grid__column", function(){
		if (!isDesktop) { hideFrontpageGridContent($(this)); }
	});

	// Show / hide content on touch large screens
	$(".frontpage-grid__column").on("touchstart", function(){
		if (isDesktop) {
			if (!$(this).is(".is-active")) {
				showFrontpageGridContent($(this));
				hideFrontpageGridContent($(this).siblings());
			}
		}
	});

	// Show content on mouse enter large screens
	$(".frontpage-grid__column").on("mouseenter", function(){
		if (isDesktop && !isTouchDevice) {
			showFrontpageGridContent($(this));
		}
	});

	// Hide content on mouse leave large screens
	$(".frontpage-grid__column").on("mouseleave", function(){
		if (isDesktop && !isTouchDevice) {
			hideFrontpageGridContent($(this));
		}
	});


	// Show content on keyboard focus
	$(".frontpage-grid").find("a, button").on("focus", function(){
		var $container = $(this).parents(".frontpage-grid__content");

		$container.css("opacity", 1);
		// showFrontpageGridContent($container);
	});

	// Init grid column
	$(".frontpage-grid__column").each(function(){
		var $column = $(this);

		// Hide content on lost keyboard focus
		$(this).find(".frontpage-grid__content-wrapper").find("a, button").filter(":first, :last").on("blur", function(e){
			setTimeout(function() {
				var $toElement = $(document.activeElement),
					$toContainer = $toElement.parents(".frontpage-grid__column");

				if ($toContainer.length <= 0 || $toContainer[0] != $column[0]) {
					hideFrontpageGridContent($column);
				}
			}, 10);
		});

		// Add transparent overlay to avoid misclicks while animating
		$(this).prepend("<div class='frontpage-grid__content-overlay'></div>");


		// Get background-image for desktop
		var imgUrl = $(this).data("image");

		// Get background image if mobile
		if (!isDesktop) {
			var mobileUrl = $(this).data("mobile-image");
			if (mobileUrl) { imgUrl = mobileUrl; }
		}

		// Set background image
		$(this).attr("style", "background-image: url(" + imgUrl + ");");


		// Prevent links in the first cell to be clicked unintentionally on mobile screens
		if (!isDesktop) {
			$column.find(".frontpage-grid__cell").first().find("a").on("click touchstart", function(e){
				if ($column.scrollLeft() < 100) {
					e.stopPropagation();
					e.preventDefault();
				}
			});
		}
	}); // end of $each


	// Show column content on mouse enter or on touch
	function showFrontpageGridContent($container) {
		var $content = $container.find(".frontpage-grid__content"),
			$cell = $container.find(".frontpage-grid__cell"),
			$heading  = $container.find(".frontpage-grid__column__heading"),
			$overlay = $container.find(".frontpage-grid__content-overlay");

		if (isDesktop) {
			if (!$body.is(".shopping-cart-is-open") && !$body.is(".search-is-open")) {
				// Change scroll indicator text
				$container.find(".frontpage-grid__scroll-indicator").text("Scroll");
				$container.addClass("is-active");
				$content.scrollLock();
				$overlay.show();

				// Fade in content container
				TweenLite.to($content, 0.3, {opacity: 1, ease:Linear.easeNone, onComplete: function(){
					$overlay.hide();
				}});

				// Animate content cells
				TweenLite.set($cell, {opacity: 0, y: 50});
				TweenMax.staggerTo($cell, 0.8, {opacity: 1, y: 0, ease: Power3.easeOut}, 0.05);
			}
		}

		if (!isDesktop) {
			var scrollPos = $container.scrollLeft();

			// Move content and heading to the left, to indicate horizontal scroll
			if (scrollPos < 30) {
				TweenLite.to([$content, $heading], 0.6, {x: -60, ease: Elastic.easeOut.config(1, 0.75)});
			}
		}
	}

	function hideFrontpageGridContent($container, event) {
		var $content = $container.find(".frontpage-grid__content"),
			$overlay = $container.find(".frontpage-grid__content-overlay");

		if (isDesktop) {
			if (!$body.is(".shopping-cart-is-open") && !$body.is(".search-is-open")) {
				$content.scrollLock("off");
				$container.removeClass("is-active");

				// Fade out content container
				TweenLite.to($content, 0.2, {opacity: 0, ease: Linear.easeNone, onComplete: function(){
					$overlay.show();
				}});

				// Change scroll indicator text
				$container.find(".frontpage-grid__scroll-indicator").text("More");
			}
		}

		if (!isDesktop) {
			var scrollPos = $container.scrollLeft(),
				$heading  = $container.find(".frontpage-grid__column__heading"),
				x 		  = $content[0]._gsTransform.x; // transformation value

			if (scrollPos <= 30) {
				TweenLite.to([$content, $heading], 0.8, {x: 0, ease: Power3.easeOut});
				TweenLite.to($container, 0.8, {scrollTo: {x: 0}, ease: Power3.easeOut});
			} else {
				// Add scroll listener
				$container.on("scroll", debounce(delayedScrollReset));
			}
		}
	}

	function delayedScrollReset(event) {
		// Removes the transform created by showFrontpageGridContent() and adds the transform value to scrollLeft,
		// after momentum scrolling has stopped

		var $container = $(event.target).closest(".frontpage-grid__column"),
			$content   = $container.find(".frontpage-grid__content"),
			$heading   = $container.find(".frontpage-grid__column__heading"),
			scroll 	   = $container.scrollLeft(),
			delta 	   = 0;

		function loop(){
			setTimeout(function(){
				delta = Math.abs(scroll - $container.scrollLeft());
				scroll = $container.scrollLeft();

				if (delta > 0) {
					loop();
				} else {
					// Remove scroll listener
					$container.off("scroll");


					var x 		  = $content[0]._gsTransform.x, // transform value
						newScroll = $container.scrollLeft() - x;

					// Reset scroll and transform
					TweenLite.set([$content, $heading], {x: 0});

					if (newScroll <= 60) {
						$container.scrollLeft(0);
					} else {
						$container.scrollLeft(newScroll);
					}
				}
			},100);
		}

		loop();
	}
} // end initFrontpageGrid;









// ORGANISM: Product carousel
$(".product-carousel").slick({
	focusOnSelect: true,
	slidesToShow: 4,
	slidesToScroll: 4,
	speed: 800,
	easing: "easeOutQuart",
	useTransform: true,
	dots: true,
	infinite: false,
	responsive: [
		{
			breakpoint: 1500,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		{
			breakpoint: 1280,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 720,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
	]
});

// Override carousel settings in specific places
$(".article .product-carousel").slick("setOption", "slidesToShow", 3, true);
$(".http-error-page .product-carousel").slick("setOption", "slidesToShow", 3, true);
$(".order-receipt .product-carousel").slick("setOption", "slidesToShow", 3, true);
























// ORGANISM – Return form

// Cancel button
$(".returns-page .checkout-nav__prev").on("click", function(e){
	e.preventDefault();
	history.back();
});

$(".returns-label__print").on("click", function(e){
	e.preventDefault();

	// Add class for special print styling in some places
	$("body").addClass("print-page-button-pushed");

	window.print();
});

// Remove print styling class on user interaction
$("body").on("mousedown touchstart", function(){ $("body").removeClass("print-page-button-pushed"); });

// Toggle textarea visibility on select change
$(".returns-form__inputs").children("select").on("change", function(){
	var $textarea = $(this).siblings(".returns-form__text-input");

	if (String($(this).val()) == String($(this).data("custom-value-name"))) {
		$textarea.slideDown();
	} else {
		$textarea.slideUp();
	}
});

// Toggle textarea on page load
$(".returns-form__inputs").children("select").trigger("change");


// Enable / disable submit button based on checkbox state
$("#returns-form-submit-checkbox").on("change", function(){
	var checked = $(this).prop("checked");

	if (checked) { $(".checkout-nav__next").prop("disabled", false); }
	else 		 { $(".checkout-nav__next").prop("disabled", true); }
});

// Trigger change event on page load in case the clicked state was remembered
$("#returns-form-submit-checkbox").trigger("change");










// Product page scroll reveal effects
$(".product-footwear, .product-clothing")
	.find(".product-section > h2, .product-section > p, .product-carousel, .media-block__image, .product-section--video")
	.scrollReveal();

$(".product-footwear, .product-clothing").find(".media-block__icons > li").scrollReveal({stagger: true});

$(".product-footwear, .product-clothing").find(".media-block__text").scrollReveal({delay: 0.3});










// Init product grid scroll effects
if ($(".category .product-grid").length) {
	// Listen for dynamically inserted elements and add scroll FX
	if (supportsMutationObservers) {
		var PGObserver = new MutationObserver(function(mutations){
			mutations.forEach(function(mutation) {
				$.each(mutation.addedNodes, function(){
					productGridScrollFX($(this));
				});
			});
		}).observe($(".product-grid").get(0), { childList: true });
	} else {
		$(".category .product-grid").addClass("no-fx");
	}
}

// Product grid scroll effects
function productGridScrollFX($el) {
	if (!isTouchDevice) {
			var productId = "product-grid-fx-" + $el.index();
			$el.addClass(productId);
			productId = "." + productId;

			new ScrollMagic.Scene({triggerElement: productId, triggerHook: "onEnter", duration: 0, offset: 30})
				.setClassToggle(productId, "is-on-screen")
				.addTo(controller)
				.reverse(false);
	} else {
		$el.css("opacity", 1);
	}
}










// Utility function for drawing text with letter-spacing to canvas
var fillTextWithSpacing = function(context, text, x, y, spacing) {
    var wAll = context.measureText(text).width;

    do {
	    char = text.substr(0, 1);
	    text = text.substr(1);

	    context.fillText(char, x, y);

	    if (text === "") { wShorter = 0; }
	    else { wShorter = context.measureText(text).width; }

	    wChar = wAll - wShorter;
	    x += wChar + spacing;
	    wAll = wShorter;

    } while (text !== "");
};

// HTTP error draw text mask in canvas
if ($(".http-error__code").length) {
	var $canvas = $("#http-error-code"),
		canvas = $canvas[0],
		c = canvas.getContext("2d"),
		text = $canvas.find("h2").text();

	c.fillStyle = "#f7f7f7";
	c.fillRect(-100, -100, 1200, 700);

	c.globalCompositeOperation = "destination-out";
	c.font = "640px Impact";
	fillTextWithSpacing(c, text, 0, 530, -10);

	TweenLite.to($(".http-error__code"), 0.2, {opacity: 1, ease: Linear.easeNone});
}















// ATOM - select
var selectBoxObserver,
	shoppingCartObserver;

$(document).ready(function(){
	if (!isTouchDevice) {
		$(".product-sidebar select").chosen({
			disable_search: true,
			width: "100%"
		});

		$(".wishlist select").chosen({
			disable_search: true,
			width: "100%"
		});

		$(".search-filter-and-sort select").chosen({
			disable_search: true,
			width: "100%"
		});

		$(".language-selector .select").chosen({
			disable_search_threshold: 10,
			width: "100%"
		});

		$(".returns-form select").chosen({
			disable_search_threshold: 10,
			width: "100%"
		});

		$(".form .select").not("#billing-address-country, #shipping-address-country").chosen({
			width: "100%",
			disable_search_threshold: 10
		});

		$("#billing-address-country, #shipping-address-country").chosen({
			width: "100%"
		});

		$(".filters select").each(function() {
			var placeholder = $(this).attr("title");
			$(this).chosen({
				width: "100%",
				disable_search: true,
				placeholder_text_single: placeholder
			});
		});
	}

	// wrap all selects in a .select wrapper div unless already wrapped or chosen is initialized
	$("select").each(function(){
		if (!$(this).next().is(".chosen-container") && !$(this).parent().is(".select") && !$(this).parent().is(".select-with-label")) {
				$(this).wrap('<div class="select"></div>');
				$(this).closest(".select").addClass($(this).attr("class"));
			}
	});

	// Make click on label trigger click on select element (as label is overlaying the select)
	$("body").on("click", ".select-with-label label", function(){
		$(this).siblings(".chosen-container").find(".chosen-single").trigger("click");
	});

	// Select with label check value and toggle checkmark
	$("body").on("change", ".select-with-label select", function(){
		if ($(this).val()) {
			$(this).addClass("is-checked");
			$(this).siblings(".chosen-container").addClass("is-checked");
		} else {
			$(this).removeClass("is-checked");
			$(this).siblings(".chosen-container").removeClass("is-checked");
		}
	});

	$(".select-with-label select").trigger("change");


	// Update all chosen instances if new <option>s are inserted dynamically
	if (supportsMutationObservers) {
		selectBoxObserver = new MutationObserver(function(mutations){
			mutations.forEach(function(mutation) {
				$.each(mutation.addedNodes, function(){
					$(this).trigger("chosen:updated");
				});
			});
		});

		$(".chosen-container").prev("select").each(function(){
			selectBoxObserver.observe($(this).get(0), { childList: true });
		});
	} else {
		// Fallback DOM observer
		$(".chosen-container").prev("select").on("DOMSubtreeModified", function(){
			$(this).trigger("chosen:updated");
		});
	}

	// Shopping cart page select. Wait for angular to finish rendering the list of products before initializing chosen on these select elements.
	if (supportsMutationObservers && $(".shopping-cart--page").length) {
		// Add mutation observer for product list items
		shoppingCartObserver = new MutationObserver(function(mutations){
			mutations.forEach(function(mutation){
				$.each(mutation.addedNodes, function(){
					$(this).find("select").each(function(){
						// Check if Angular has finished rendering the markup
						var isInit = $(this).attr("id").search("{{") < 0;
						
						if (isInit && !isTouchDevice) {
							// Init chosen
							$(this).chosen({
								disable_search: true,
								width: "100%"
							});

							$(this).trigger("change");

							// Add to mutation observer in order to detect dynamically inserted options
							selectBoxObserver.observe($(this).get(0), {childList: true });
						}
					});
				});
			});
		});

		shoppingCartObserver.observe(
			$(".shopping-cart--page .shopping-cart__products").get(0),
			{ childList: true }
		);
	}
});





// Build pinterest sharing link, product page sidebar
$(".product-sidebar .social-links__item.pinterest").each(function(){
	var $link = $(".product-sidebar .social-links__item.pinterest"),
		options = [
			"url=" + encodeURIComponent(window.location.href),
			"&description=" + encodeURIComponent("SWIMS " + $(".product-header__heading").text())
		],
		queryString,
		optionsString = "";

	// Get image
	if ($(".social-media-image").length) {
		options.push(
			"&media=" + encodeURIComponent(
				window.location.origin + $(".social-media-image").data("img-src")
			)
		);
	} else if ($(".product-image-large img").length) {
		options.push(
			"&media=" + encodeURIComponent(
				window.location.origin + $(".product-image-large img").first().attr("src")
			)
		);
	}

	// Merge options to single string
	options.forEach(function(option){
		optionsString = optionsString + option;
	});

	queryString = "http://pinterest.com/pin/create/button/?" + optionsString;

	$link.attr("href", queryString).attr("target", "_blank");
});

// Build facebook sharing link, product page sidebar
$(".product-sidebar .social-links__item.facebook").each(function(){
	var $link = $(".product-sidebar .social-links__item.facebook"),
		options = [
			"app_id=575362132615093",
			"&link=" + encodeURIComponent(window.location.href),
			"&name=" + encodeURIComponent("SWIMS " + $(".product-header__heading").text()),
			"&description=" + encodeURIComponent($(".product-sidebar__description").text()),
			"&redirect_uri=" + encodeURIComponent("https://www.facebook.com")
		],
		queryString,
		optionsString = "",
		domain = window.location.origin,
		imgUrl;

	// Get image
	if ($(".social-media-image").length) {
		imgUrl = $(".social-media-image").data("img-src");
	} else if ($(".product-image-large img").length) {
		imgUrl = $(".product-image-large img").first().attr("src");
	}

	// Push image url component to options array
	if (typeof imgUrl !== "undefined") {
		options.push("&picture=" + encodeURIComponent(
			// Strip domain and first slash (if present) and reapply, for consistent URLs
				domain + "/" + imgUrl.replace(domain, "").replace(/^\//, "")
			)
		);
	}

	// Merge options to single string
	options.forEach(function(option){
		optionsString = optionsString + option;
	});

	queryString = "https://www.facebook.com/dialog/feed?" + optionsString;

	$link.attr("href", queryString).attr("target", "_blank");
});

})();

//}); // end of require