/* jshint -W099 */
/* global jQuery:false */

(function($, Oxx, commercestarterkit) {

	"use strict";

	commercestarterkit = window.commercestarterkit = {
        _language : "no",
		_$wishlist: undefined,
		_$wishlistCounter: undefined,
		_$cartCounter: undefined,

//********************************************************************************
//*PUBLIC FUNCTIONS***************************************************************
//********************************************************************************


		init: function () {
		    this._language = Oxx.AjaxUtils.language;
			this._$wishlist = $('.wishlist-counter');
			this._$wishlistCounter = this._$wishlist.find('.val');
			this._$mobileCartCounter = $('.mobile-header .cart-number-of-items');
			this._$cartCounter = $('.right-nav .cart-number-of-items');
			this._$smallCartCartCounter = $('.shopping-cart .cart-number-of-items');

			this.retrieveCartCounters();
		},

//********************************************************************************

		/**
		 * Update the wish list counter with provided value
		 * @param {Number} value
		 */
		updateWishlistCounter: function(value) {
			var text = parseInt(value, 10);
			if(text === 0) {
				this._$wishlist.hide();
			} else {
				this._$wishlist.show();
			}
			this._$wishlistCounter.text(text);
		},

//********************************************************************************

		/**
		 * Get the number of items in the wish list
		 * @returns {Number}
		 */
		getWishlistCounter: function() {
			return parseInt(this._$wishlistCounter.text(), 10);
		},

//********************************************************************************

		/**
		 * Update the cart counter with provided value
		 *
		 * @param {Number} value
		 */
		updateCartCounter: function (value) {
			var text = parseInt(value, 10);
			this._$mobileCartCounter.text(text);
			this._$cartCounter.text(text);
			this._$smallCartCartCounter.text(text);
			
		},

		animateCartCount: function() {
		    var lineHeight = this._$cartCounter.css('line-height');
		    this._$cartCounter.animate({ fontSize: '250%', lineHeight: '0px' }).animate({ fontSize: '100%', lineHeight: lineHeight });
		    $('.cart-message').addClass('visible');
		    setTimeout(function () {
		        $('.cart-message').removeClass('visible');
		    }, 1500);
		},

//********************************************************************************

		/**
		 * Get the number of items in the cart
		 * @returns {Number}
		 */
		getCartCounter: function() {
			return parseInt(this._$cartCounter.text(), 10);
		},

//********************************************************************************

		/**
		 * Update the counters on the wishlist and the cart links
		 */
		retrieveCartCounters: function() {
			if(this._$cartCounter.length > 0 || this._$wishlistCounter.length > 0) {
			    $.get('/api/cartapi/getCounters?_=' + new Date().getTime().toString(), undefined,
					$.proxy(this._onRetrieveCartCountersCallback, this), 'json');
			}
		},

//********************************************************************************
//*CALLBACK METHODS **************************************************************
//********************************************************************************

		/**
		 *
		 * @param data
		 * @private
		 */
		_onRetrieveCartCountersCallback: function(data) {
			if(data) {
				if(typeof(data.wishlist) !== "undefined") {
					this.updateWishlistCounter(data.wishlist);
				}
				if(typeof(data.cart) !== "undefined") {
					this.updateCartCounter(data.cart);
				}
			}
		}
	};

    // init commercestarterkit on DOM ready
	$($.proxy(commercestarterkit.init, commercestarterkit));


})(jQuery, window.Oxx, window.commercestarterkit);