-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2016 at 07:01 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_ampm`
--

-- --------------------------------------------------------

--
-- Table structure for table `main`
--

CREATE TABLE IF NOT EXISTS `main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `slogan` varchar(200) NOT NULL,
  `logo` text NOT NULL,
  `foto` text NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `line` varchar(50) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `google` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `google_analytics` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `main`
--

INSERT INTO `main` (`id`, `nama`, `slogan`, `logo`, `foto`, `telepon`, `line`, `twitter`, `facebook`, `google`, `email`, `description`, `keywords`, `google_analytics`) VALUES
(1, 'AMPM WeAreEqual', '-= LETS KNOW THE BEST OFFERINGS FROM US =-', 'images/bradleys-footwear-logo.png', 'images/main_foto.png', '6281322207414', 'ti/p/~officialbradleys', 'BradleysShoes', 'BradleysFootwear', '+BradleysfootwearOfficial', 'cs@bradleysfootwear.com', 'Bradleys Footwear, The best handmade assembling and materials. Bandung, Jawa Barat, Indonesia', 'bradleys footwear,bradleys official,official,bradleys,footwear,bradley,original,handmade,bandung,jawa barat,indonesia,sepatu,kulit', '<script>\r\n  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,''script'',''//www.google-analytics.com/analytics.js'',''ga'');\r\n\r\n  ga(''create'', ''UA-63627329-1'', ''auto'');\r\n  ga(''send'', ''pageview'');\r\n\r\n</script>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alamat`
--

CREATE TABLE IF NOT EXISTS `tbl_alamat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `kodepos` varchar(10) NOT NULL,
  `telepon` varchar(14) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_alamat`
--

INSERT INTO `tbl_alamat` (`id`, `user`, `nama`, `alamat`, `kota`, `provinsi`, `negara`, `kodepos`, `telepon`) VALUES
(12, 1, 'Ade Indra', 'Jl. Soekarno Hatta', '57', '12', 'INDONESIA', '41316', '081282180832'),
(18, 22, 'Doni Kusuma', 'Jl. Gajah Mada No. 67', '84', '11', 'INDONESIA', '41313', '08776546511'),
(11, 21, 'Ronald Kasali', 'Jl. Raya', '58', '12', 'INDONESIA', '41316', '085766554433'),
(22, 23, 'Ryan Sinata', 'Jl. Titimplik No. 65', '57', '12', 'INDONESIA', '41317', '081232362532');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bank`
--

CREATE TABLE IF NOT EXISTS `tbl_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nomor` varchar(50) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `cabang` varchar(100) NOT NULL,
  `swift` varchar(50) NOT NULL,
  `iban` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_bank`
--

INSERT INTO `tbl_bank` (`id`, `kode`, `nama`, `nomor`, `atas_nama`, `logo`, `cabang`, `swift`, `iban`) VALUES
(1, '014', 'BCA', '0080605404', 'GALIH AGUSTIAN', 'images/bank/bca.png', 'Bandung', '', ''),
(2, '011', 'BNI', '0482712995', 'GALIH AGUSTIAN', 'images/bank/bni.png', 'Bandung', '', ''),
(4, '013', 'MANDIRI', '9000032939176', 'GALIH AGUSTIAN', 'images/bank/mandiri.png', 'Bandung', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE IF NOT EXISTS `tbl_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `id_order` int(11) NOT NULL,
  `produk` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `size` varchar(5) NOT NULL,
  `berat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `ip`, `id_order`, `produk`, `user`, `size`, `berat`, `jumlah`, `harga`, `tanggal`) VALUES
(1, '127.0.0.1', 1003, 4, 23, 'M', 0, 2, 75000, '2016-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE IF NOT EXISTS `tbl_city` (
  `idcity` int(5) NOT NULL AUTO_INCREMENT,
  `idprovince` int(5) NOT NULL,
  `name_city` varchar(50) NOT NULL,
  `insertby_city` varchar(50) NOT NULL,
  `inserton_city` varchar(50) NOT NULL,
  `updateby_city` varchar(50) NOT NULL,
  `updateon_city` varchar(50) NOT NULL,
  PRIMARY KEY (`idcity`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`idcity`, `idprovince`, `name_city`, `insertby_city`, `inserton_city`, `updateby_city`, `updateon_city`) VALUES
(1, 1, 'Kabupaten Simeulue', '', '', '', ''),
(2, 1, 'Kabupaten Aceh Singkil', '', '', '', ''),
(3, 1, 'Kabupaten Aceh Selatan', '', '', '', ''),
(4, 1, 'Kabupaten Aceh Tenggara', '', '', '', ''),
(5, 1, 'Kabupaten Aceh Timur', '', '', '', ''),
(6, 1, 'Kabupaten Aceh Tengah', '', '', '', ''),
(7, 1, 'Kabupaten Aceh Barat', '', '', '', ''),
(8, 1, 'Kabupaten Aceh Besar', '', '', '', ''),
(9, 1, 'Kabupaten Pidie', '', '', '', ''),
(10, 1, 'Kabupaten Bireuen', '', '', '', ''),
(11, 1, 'Kabupaten Aceh Utara', '', '', '', ''),
(12, 1, 'Kabupaten Aceh Barat Daya', '', '', '', ''),
(13, 1, 'Kabupaten Gayo Lues', '', '', '', ''),
(14, 1, 'Kabupaten Aceh Tamiang', '', '', '', ''),
(15, 1, 'Kabupaten Nagan Raya', '', '', '', ''),
(16, 1, 'Kabupaten Aceh Jaya', '', '', '', ''),
(17, 1, 'Kabupaten Bener Meriah', '', '', '', ''),
(18, 1, 'Kabupaten Pidie Jaya', '', '', '', ''),
(19, 1, 'Kota Banda Aceh', '', '', '', ''),
(20, 1, 'Kota Sabang', '', '', '', ''),
(21, 1, 'Kota Langsa', '', '', '', ''),
(22, 1, 'Kota Lhokseumawe', '', '', '', ''),
(23, 1, 'Kota Subulussalam', '', '', '', ''),
(24, 2, 'Kabupaten Nias', '', '', '', ''),
(25, 2, 'Kabupaten Mandailing Natal', '', '', '', ''),
(26, 2, 'Kabupaten Tapanuli Selatan', '', '', '', ''),
(27, 2, 'Kabupaten Tapanuli Tengah', '', '', '', ''),
(28, 2, 'Kabupaten Tapanuli Utara', '', '', '', ''),
(29, 2, 'Kabupaten Toba Samosir', '', '', '', ''),
(30, 2, 'Kabupaten Labuhan Batu', '', '', '', ''),
(31, 2, 'Kabupaten Asahan', '', '', '', ''),
(32, 2, 'Kabupaten Simalungun', '', '', '', ''),
(33, 2, 'Kabupaten Dairi', '', '', '', ''),
(34, 2, 'Kabupaten Karo', '', '', '', ''),
(35, 2, 'Kabupaten Deli Serdang', '', '', '', ''),
(36, 2, 'Kabupaten Langkat', '', '', '', ''),
(37, 2, 'Kabupaten Nias Selatan', '', '', '', ''),
(38, 2, 'Kabupaten Humbang Hasundutan', '', '', '', ''),
(39, 2, 'Kabupaten Pakpak Bharat', '', '', '', ''),
(40, 2, 'Kabupaten Samosir', '', '', '', ''),
(41, 2, 'Kabupaten Serdang Bedagai', '', '', '', ''),
(42, 2, 'Kabupaten Batu Bara', '', '', '', ''),
(43, 2, 'Kabupaten Padang Lawas Utara', '', '', '', ''),
(44, 2, 'Kabupaten Padang Lawas', '', '', '', ''),
(45, 2, 'Kabupaten Labuhan Batu Selatan', '', '', '', ''),
(46, 2, 'Kabupaten Labuhan Batu Utara', '', '', '', ''),
(47, 2, 'Kabupaten Nias Utara', '', '', '', ''),
(48, 2, 'Kabupaten Nias Barat', '', '', '', ''),
(49, 2, 'Kota Sibolga', '', '', '', ''),
(50, 2, 'Kota Tanjung Balai', '', '', '', ''),
(51, 2, 'Kota Pematang Siantar', '', '', '', ''),
(52, 2, 'Kota Tebing Tinggi', '', '', '', ''),
(53, 2, 'Kota Medan', '', '', '', ''),
(54, 2, 'Kota Binjai', '', '', '', ''),
(55, 2, 'Kota Padangsidimpuan', '', '', '', ''),
(56, 2, 'Kota Gunungsitoli', '', '', '', ''),
(57, 12, 'Kota Bandung', '', '', '', ''),
(58, 12, 'Kabupaten Bandung', '', '', '', ''),
(59, 12, 'Kabupaten Bandung Barat', '', '', '', ''),
(60, 12, 'Kabupaten Bekasi', 'zoel', '2012-04-20 17:49:15', '', ''),
(61, 12, 'Kabupaten Bogor', 'zoel', '2012-04-20 17:49:15', '', ''),
(62, 12, 'Kabupaten Ciamis', 'zoel', '2012-04-20 17:49:15', '', ''),
(63, 12, 'Kabupaten Cianjur', 'zoel', '2012-04-20 17:49:15', '', ''),
(64, 12, 'Kabupaten Cirebon', 'zoel', '2012-04-20 17:49:15', '', ''),
(65, 12, 'Kabupaten Garut', 'zoel', '2012-04-20 17:49:15', '', ''),
(66, 12, 'Kabupaten Indramayu', 'zoel', '2012-04-20 17:49:15', '', ''),
(67, 12, 'Kabupaten Karawang', 'zoel', '2012-04-20 17:49:15', '', ''),
(68, 12, 'Kabupaten Kuningan', 'zoel', '2012-04-20 17:49:15', '', ''),
(69, 12, 'Kabupaten Majalengka', 'zoel', '2012-04-20 17:49:15', '', ''),
(70, 12, 'Kabupaten Purwakarta', 'zoel', '2012-04-20 17:49:15', '', ''),
(71, 12, 'Kabupaten Subang', 'zoel', '2012-04-20 17:49:15', '', ''),
(72, 12, 'Kabupaten Sukabumi', 'zoel', '2012-04-20 17:49:15', '', ''),
(73, 12, 'Kabupaten Sumedang', 'zoel', '2012-04-20 17:49:15', '', ''),
(74, 12, 'Kabupaten Tasikmalaya', 'zoel', '2012-04-20 17:49:15', '', ''),
(75, 12, 'Kota Banjar', 'zoel', '2012-04-20 17:49:15', '', ''),
(76, 12, 'Kota Bekasi', 'zoel', '2012-04-20 17:49:15', '', ''),
(77, 12, 'Kota Bogor', 'zoel', '2012-04-20 17:49:15', '', ''),
(78, 12, 'Kota Cimahi', 'zoel', '2012-04-20 17:49:15', '', ''),
(79, 12, 'Kota Cirebon', 'zoel', '2012-04-20 17:49:15', '', ''),
(80, 12, 'Kota Depok', 'zoel', '2012-04-20 17:49:15', '', ''),
(81, 12, 'Kota Sukabumi', 'zoel', '2012-04-20 17:49:15', '', ''),
(82, 12, 'Kota Tasikmalaya', 'zoel', '2012-04-20 17:49:15', '', ''),
(83, 11, 'Kota Administrasi Jakarta Barat', 'zoel', '2012-04-23 13:18:46', '', ''),
(84, 11, 'Kota Administrasi Jakarta Pusat', 'zoel', '2012-04-23 13:18:46', '', ''),
(85, 11, 'Kota Administrasi Jakarta Selatan', 'zoel', '2012-04-23 13:18:46', '', ''),
(86, 11, 'Kota Administrasi Jakarta Timur', 'zoel', '2012-04-23 13:18:46', '', ''),
(87, 11, 'Kota Administrasi Jakarta Utara', 'zoel', '2012-04-23 13:18:46', '', ''),
(88, 11, 'Kabupaten Kepulauan Seribu', 'zoel', '2012-04-23 13:18:46', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_foto_produk`
--

CREATE TABLE IF NOT EXISTS `tbl_foto_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_foto_produk`
--

INSERT INTO `tbl_foto_produk` (`id`, `id_produk`, `foto`) VALUES
(2, 2, 'assets/images/produk/hitam1.jpg'),
(3, 2, 'images/products/bradleys/bradleys-footwear-original-2-3.jpg'),
(4, 4, 'images/products/bradleys/bradleys-footwear-original-4-4.jpg'),
(5, 3, 'images/products/bradleys/bradleys-footwear-original-3-5.jpg'),
(6, 7, 'images/products/bradleys/bradleys-footwear-original-7-6.jpg'),
(7, 1, 'images/products/bradleys/bradleys-footwear-original-1-7.jpg'),
(8, 1, 'images/products/bradleys/bradleys-footwear-original-1-8.jpg'),
(9, 6, 'images/products/bradleys/bradleys-footwear-original-6-9.jpg'),
(10, 9, 'images/products/bradleys/bradleys-footwear-original-9-10.jpg'),
(11, 9, 'images/products/bradleys/bradleys-footwear-original-9-11.jpg'),
(12, 6, 'images/products/bradleys/bradleys-footwear-original-6-12.jpg'),
(13, 6, 'images/products/bradleys/bradleys-footwear-original-6-13.jpg'),
(14, 8, 'images/products/bradleys/bradleys-footwear-original-8-14.jpg'),
(15, 8, 'images/products/bradleys/bradleys-footwear-original-8-15.jpg'),
(16, 8, 'images/products/bradleys/bradleys-footwear-original-8-16.jpg'),
(17, 8, 'images/products/bradleys/bradleys-footwear-original-8-17.jpg'),
(18, 14, 'images/products/bradleys/bradleys-footwear-original-14-18.jpg'),
(19, 14, 'images/products/bradleys/bradleys-footwear-original-14-19.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `jml_bayar` int(11) NOT NULL,
  `ongkir` int(11) NOT NULL,
  `jml_berat` int(11) NOT NULL,
  `bank_transfer` varchar(100) NOT NULL,
  `bank_tujuan` varchar(100) NOT NULL,
  `alamat` int(11) NOT NULL,
  `kurir` varchar(50) NOT NULL,
  `resi_kiriman` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `agree` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `st_payment` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1005 ;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `user`, `ip`, `jml_bayar`, `ongkir`, `jml_berat`, `bank_transfer`, `bank_tujuan`, `alamat`, `kurir`, `resi_kiriman`, `tanggal`, `agree`, `status`, `st_payment`) VALUES
(1001, 1, '127.0.0.1', 150000, 0, 0, 'MANDIRI 1122334455 (Sony Dwi Kuncoro)', 'MANDIRI 9000032939176 (GALIH AGUSTIAN)', 12, '0', '0', '2016-12-06', 'agree', 'onprocess', 'PAID'),
(1002, 22, '127.0.0.1', 150000, 0, 0, 'BNI 046756232212 (DONI KUSUMA)', 'BNI 0482712995 (GALIH AGUSTIAN)', 18, 'JNE', 'ASDF 12345', '2016-12-08', 'agree', 'complete', 'PAID'),
(1003, 23, '127.0.0.1', 150000, 0, 0, 'MANDIRI 90000656632 (RYAN SINATA)', 'MANDIRI 9000032939176 (GALIH AGUSTIAN)', 22, '0', '0', '2016-12-08', 'agree', 'In Checking', 'UNPAID');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments`
--

CREATE TABLE IF NOT EXISTS `tbl_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `transfer_from` text NOT NULL,
  `transfer_to` text NOT NULL,
  `debit` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_payments`
--

INSERT INTO `tbl_payments` (`id`, `id_order`, `user`, `transfer_from`, `transfer_to`, `debit`, `credit`, `tanggal`, `status`) VALUES
(1, 1002, 22, 'BNI 046756232212 (DONI KUSUMA)', 'BNI 0482712995 (GALIH AGUSTIAN)', 150000, 0, '2016-12-08', 'Pending'),
(3, 1003, 23, 'MANDIRI 90000656632 (RYAN SINATA)', 'MANDIRI 9000032939176 (GALIH AGUSTIAN)', 150000, 0, '2016-12-08', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE IF NOT EXISTS `tbl_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `bahan` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `foto` text NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `nama`, `sex`, `kategori`, `type`, `warna`, `bahan`, `harga`, `berat`, `foto`, `keterangan`, `status`) VALUES
(1, 'Blue Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Blue', 'Bamboo', 75000, 1, 'bluerain.jpg', 'Best Product', 'aktif'),
(2, 'Blue Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Blue', 'Bamboo', 75000, 1, 'bluerain2.jpg', 'Best Product', 'aktif'),
(3, 'Blue Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Blue', 'Bamboo', 75000, 1, 'bluerain3.jpg', 'Best Product', 'aktif'),
(4, 'Blue Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Blue', 'Bamboo', 75000, 1, 'bluerain_signature.jpg', 'Best Product', 'aktif'),
(5, 'Brown Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Brown', 'Mamboo', 75000, 1, 'brownrain.jpg', 'Best Product', 'aktif'),
(6, 'Brown Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Brown', 'Mamboo', 75000, 1, 'brownrain2.jpg', 'Best Product', 'aktif'),
(7, 'Brown Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Brown', 'Mamboo', 75000, 1, 'brownrain3.jpg', 'Best Product', 'aktif'),
(8, 'Brown Rain', 'Men', 'Tshirt', 'Blank Tshirt', 'Brown', 'Mamboo', 75000, 1, 'brownrain_signature.jpg', 'Best Product', 'aktif'),
(9, 'Grey Titanium', 'Men', 'Tshirt', 'Blank Tshirt', 'Grey', 'Combed', 65000, 1, 'greytitanium.jpg', 'Best Product', 'aktif'),
(10, 'Grey Titanium', 'Men', 'Tshirt', 'Blank Tshirt', 'Grey', 'Combed', 65000, 1, 'greytitanium2.jpg', 'Best Product', 'aktif'),
(11, 'Grey Titanium', 'Men', 'Tshirt', 'Blank Tshirt', 'Grey', 'Combed', 65000, 1, 'greytitanium3.jpg', 'Best Product', 'aktif'),
(12, 'Grey Titanium', 'Men', 'Tshirt', 'Blank Tshirt', 'Grey', 'Combed', 65000, 1, 'greytitanium_signature.jpg', 'Best Product', 'aktif'),
(13, 'Red Brick', 'Men', 'Tshirt', 'Blank Tshirt', 'Red', 'Combed', 65000, 1, 'redbrick.jpg', 'Best Product', 'aktif'),
(14, 'Red Brick', 'Men', 'Tshirt', 'Blank Tshirt', 'Red', 'Combed', 65000, 1, 'redbrick2.jpg', 'Best Product', 'aktif'),
(15, 'Red Brick', 'Men', 'Tshirt', 'Blank Tshirt', 'Red', 'Combed', 65000, 1, 'redbrick3.jpg', 'Best Product', 'aktif'),
(16, 'Red Brick', 'Men', 'Tshirt', 'Blank Tshirt', 'Red', 'Combed', 65000, 1, 'redbrick_signature.jpg', 'Best Product', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_province`
--

CREATE TABLE IF NOT EXISTS `tbl_province` (
  `idprovince` int(5) NOT NULL AUTO_INCREMENT,
  `name_province` varchar(50) NOT NULL,
  `capital_province` varchar(50) NOT NULL,
  `insertby_province` varchar(50) NOT NULL,
  `inserton_province` varchar(50) NOT NULL,
  `updateby_province` varchar(50) NOT NULL,
  `updateon_province` varchar(50) NOT NULL,
  PRIMARY KEY (`idprovince`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tbl_province`
--

INSERT INTO `tbl_province` (`idprovince`, `name_province`, `capital_province`, `insertby_province`, `inserton_province`, `updateby_province`, `updateon_province`) VALUES
(1, 'Aceh', '', '', '', '', ''),
(2, 'Sumatera Utara', '', '', '', '', ''),
(3, 'Sumatera Barat', '', '', '', '', ''),
(4, 'Riau', '', '', '', '', ''),
(5, 'Jambi', '', '', '', '', ''),
(6, 'Sumatera Selatan', '', '', '', '', ''),
(7, 'Bengkulu', '', '', '', '', ''),
(8, 'Lampung', '', '', '', '', ''),
(9, 'Kepulauan Bangka Belitung', '', '', '', '', ''),
(10, 'Kepulauan Riau', '', '', '', '', ''),
(11, 'Dki Jakarta', '', '', '', '', ''),
(12, 'Jawa Barat', '', '', '', '', ''),
(13, 'Jawa Tengah', '', '', '', '', ''),
(14, 'Di Yogyakarta', '', '', '', '', ''),
(15, 'Jawa Timur', '', '', '', '', ''),
(16, 'Banten', '', '', '', '', ''),
(17, 'Bali', '', '', '', '', ''),
(18, 'Nusa Tenggara Barat', '', '', '', '', ''),
(19, 'Nusa Tenggara Timur', '', '', '', '', ''),
(20, 'Kalimantan Barat', '', '', '', '', ''),
(21, 'Kalimantan Tengah', '', '', '', '', ''),
(22, 'Kalimantan Selatan', '', '', '', '', ''),
(23, 'Kalimantan Timur', '', '', '', '', ''),
(24, 'Sulawesi Utara', '', '', '', '', ''),
(25, 'Sulawesi Tengah', '', '', '', '', ''),
(26, 'Sulawesi Selatan', '', '', '', '', ''),
(27, 'Sulawesi Tenggara', '', '', '', '', ''),
(28, 'Gorontalo', '', '', '', '', ''),
(29, 'Sulawesi Barat', '', '', '', '', ''),
(30, 'Maluku', '', '', '', '', ''),
(31, 'Maluku Utara', '', '', '', '', ''),
(32, 'Papua Barat', '', '', '', '', ''),
(33, 'Papua', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `email`, `password`, `telepon`, `nama`, `code`) VALUES
(1, 'andylife6@gmail.com', '6c736e08175c69d4f2d73523ab86103c', '081282180832', 'Gugun Gondrong', 249536),
(21, 'andy@gmail.com', '75bfc2f889bcef7b65f41f27046ee92a', '085766554433', 'Ronald Kasali', 217439),
(22, 'doni@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '08776565211', 'Doni Kusuma', 0),
(23, 'ryan1987@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0811656533', 'Ryan Nugraha', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_adm`
--

CREATE TABLE IF NOT EXISTS `user_adm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_adm`
--

INSERT INTO `user_adm` (`id`, `username`, `password`) VALUES
(1, 'bradleysofficial', 'TXdgGi7Bpc9GmCJv4iAtDG/arEaDXy/EKAUPZxCBeH4='),
(2, 'ihsanmchinks', '3huH5+zEcrHSsoO58SdAeXyzQwFpcAbQWwfrhkXCq6E=');
